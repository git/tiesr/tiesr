.build-conf:
	@echo Tool collection not found.
	@echo Please specify existing tool collection in project properties
	@exit 1

# Clean Targets
.clean-conf:
	${RM} -r build/ArmLinuxReleaseGnueabiWinHost
	${RM} ../../Dist/ArmLinuxReleaseGnueabiWinHost/lib/libTIesrEngineCore.so.1

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
