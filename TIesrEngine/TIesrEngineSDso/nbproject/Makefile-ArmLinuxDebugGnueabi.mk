#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
CCADMIN=CCadmin
RANLIB=ranlib
CC=arm-none-linux-gnueabi-gcc
CCC=arm-none-linux-gnueabi-g++
CXX=arm-none-linux-gnueabi-g++
FC=
AS=arm-none-linux-gnueabi-as

# Macros
CND_PLATFORM=arm-none-linux-gnueabi-Linux-x86
CND_CONF=ArmLinuxDebugGnueabi
CND_DISTDIR=dist

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=build/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/_ext/_DOTDOT/src/main-sdreco.o \
	${OBJECTDIR}/_ext/_DOTDOT/src/sdenro.o \
	${OBJECTDIR}/_ext/_DOTDOT/src/main-sdenro.o \
	${OBJECTDIR}/_ext/_DOTDOT/src/engine_sdenro_init.o \
	${OBJECTDIR}/_ext/_DOTDOT/src/sdreco.o \
	${OBJECTDIR}/_ext/_DOTDOT/src/sdbackgrd.o \
	${OBJECTDIR}/_ext/_DOTDOT/src/loadmfcc.o \
	${OBJECTDIR}/_ext/_DOTDOT/src/filemode.o \
	${OBJECTDIR}/_ext/_DOTDOT/src/engine_sdreco_init.o \
	${OBJECTDIR}/_ext/_DOTDOT/src/alinaux.o \
	${OBJECTDIR}/_ext/_DOTDOT/src/sdauxl.o \
	${OBJECTDIR}/_ext/_DOTDOT/src/sdlive.o \
	${OBJECTDIR}/_ext/_DOTDOT/src/sdvqpack.o

# C Compiler Flags
CFLAGS=-v

# CC Compiler Flags
CCFLAGS=-v
CXXFLAGS=-v

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-Wl,-rpath ${CND_BASEDIR}/../../Dist/${CND_CONF}/lib ../TIesrEngineCoreso/../../Dist/ArmLinuxDebugGnueabi/lib/libTIesrEngineCore.so.1

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	${MAKE}  -f nbproject/Makefile-ArmLinuxDebugGnueabi.mk ../../Dist/ArmLinuxDebugGnueabi/lib/libTIesrEngineSD.so.1

../../Dist/ArmLinuxDebugGnueabi/lib/libTIesrEngineSD.so.1: ../TIesrEngineCoreso/../../Dist/ArmLinuxDebugGnueabi/lib/libTIesrEngineCore.so.1

../../Dist/ArmLinuxDebugGnueabi/lib/libTIesrEngineSD.so.1: ${OBJECTFILES}
	${MKDIR} -p ../../Dist/ArmLinuxDebugGnueabi/lib
	${LINK.cc} -Wl,-znow,-zdefs -Wl,-h,libTIesrEngineSD.so.1 -Wl,--version-script=../resource/TIesrEngineSD.ver -shared -o ../../Dist/${CND_CONF}/lib/libTIesrEngineSD.so.1 -fPIC ${OBJECTFILES} ${LDLIBSOPTIONS} 

${OBJECTDIR}/_ext/_DOTDOT/src/main-sdreco.o: nbproject/Makefile-${CND_CONF}.mk ../src/main-sdreco.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/_DOTDOT/src
	${RM} $@.d
	$(COMPILE.cc) -g -Wall -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/_DOTDOT/src/main-sdreco.o ../src/main-sdreco.cpp

${OBJECTDIR}/_ext/_DOTDOT/src/sdenro.o: nbproject/Makefile-${CND_CONF}.mk ../src/sdenro.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/_DOTDOT/src
	${RM} $@.d
	$(COMPILE.cc) -g -Wall -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/_DOTDOT/src/sdenro.o ../src/sdenro.cpp

${OBJECTDIR}/_ext/_DOTDOT/src/main-sdenro.o: nbproject/Makefile-${CND_CONF}.mk ../src/main-sdenro.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/_DOTDOT/src
	${RM} $@.d
	$(COMPILE.cc) -g -Wall -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/_DOTDOT/src/main-sdenro.o ../src/main-sdenro.cpp

${OBJECTDIR}/_ext/_DOTDOT/src/engine_sdenro_init.o: nbproject/Makefile-${CND_CONF}.mk ../src/engine_sdenro_init.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/_DOTDOT/src
	${RM} $@.d
	$(COMPILE.cc) -g -Wall -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/_DOTDOT/src/engine_sdenro_init.o ../src/engine_sdenro_init.cpp

${OBJECTDIR}/_ext/_DOTDOT/src/sdreco.o: nbproject/Makefile-${CND_CONF}.mk ../src/sdreco.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/_DOTDOT/src
	${RM} $@.d
	$(COMPILE.cc) -g -Wall -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/_DOTDOT/src/sdreco.o ../src/sdreco.cpp

${OBJECTDIR}/_ext/_DOTDOT/src/sdbackgrd.o: nbproject/Makefile-${CND_CONF}.mk ../src/sdbackgrd.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/_DOTDOT/src
	${RM} $@.d
	$(COMPILE.cc) -g -Wall -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/_DOTDOT/src/sdbackgrd.o ../src/sdbackgrd.cpp

${OBJECTDIR}/_ext/_DOTDOT/src/loadmfcc.o: nbproject/Makefile-${CND_CONF}.mk ../src/loadmfcc.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/_DOTDOT/src
	${RM} $@.d
	$(COMPILE.cc) -g -Wall -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/_DOTDOT/src/loadmfcc.o ../src/loadmfcc.cpp

${OBJECTDIR}/_ext/_DOTDOT/src/filemode.o: nbproject/Makefile-${CND_CONF}.mk ../src/filemode.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/_DOTDOT/src
	${RM} $@.d
	$(COMPILE.cc) -g -Wall -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/_DOTDOT/src/filemode.o ../src/filemode.cpp

${OBJECTDIR}/_ext/_DOTDOT/src/engine_sdreco_init.o: nbproject/Makefile-${CND_CONF}.mk ../src/engine_sdreco_init.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/_DOTDOT/src
	${RM} $@.d
	$(COMPILE.cc) -g -Wall -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/_DOTDOT/src/engine_sdreco_init.o ../src/engine_sdreco_init.cpp

${OBJECTDIR}/_ext/_DOTDOT/src/alinaux.o: nbproject/Makefile-${CND_CONF}.mk ../src/alinaux.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/_DOTDOT/src
	${RM} $@.d
	$(COMPILE.cc) -g -Wall -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/_DOTDOT/src/alinaux.o ../src/alinaux.cpp

${OBJECTDIR}/_ext/_DOTDOT/src/sdauxl.o: nbproject/Makefile-${CND_CONF}.mk ../src/sdauxl.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/_DOTDOT/src
	${RM} $@.d
	$(COMPILE.cc) -g -Wall -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/_DOTDOT/src/sdauxl.o ../src/sdauxl.cpp

${OBJECTDIR}/_ext/_DOTDOT/src/sdlive.o: nbproject/Makefile-${CND_CONF}.mk ../src/sdlive.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/_DOTDOT/src
	${RM} $@.d
	$(COMPILE.cc) -g -Wall -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/_DOTDOT/src/sdlive.o ../src/sdlive.cpp

${OBJECTDIR}/_ext/_DOTDOT/src/sdvqpack.o: nbproject/Makefile-${CND_CONF}.mk ../src/sdvqpack.cpp 
	${MKDIR} -p ${OBJECTDIR}/_ext/_DOTDOT/src
	${RM} $@.d
	$(COMPILE.cc) -g -Wall -fPIC  -MMD -MP -MF $@.d -o ${OBJECTDIR}/_ext/_DOTDOT/src/sdvqpack.o ../src/sdvqpack.cpp

# Subprojects
.build-subprojects:
	cd ../TIesrEngineCoreso && ${MAKE}  -f Makefile CONF=ArmLinuxDebugGnueabi

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/ArmLinuxDebugGnueabi
	${RM} ../../Dist/ArmLinuxDebugGnueabi/lib/libTIesrEngineSD.so.1

# Subprojects
.clean-subprojects:
	cd ../TIesrEngineCoreso && ${MAKE}  -f Makefile CONF=ArmLinuxDebugGnueabi clean

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
