#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# WindowsDebugMinGW configuration
CND_PLATFORM_WindowsDebugMinGW=Cygwin-Linux-x86
CND_ARTIFACT_DIR_WindowsDebugMinGW=../../Dist/WindowsDebugMinGW
CND_ARTIFACT_NAME_WindowsDebugMinGW=libTIesrEngineSI.dll
CND_ARTIFACT_PATH_WindowsDebugMinGW=../../Dist/WindowsDebugMinGW/libTIesrEngineSI.dll
CND_PACKAGE_DIR_WindowsDebugMinGW=dist/WindowsDebugMinGW/Cygwin-Linux-x86/package
CND_PACKAGE_NAME_WindowsDebugMinGW=libTIesrEngineSIso.so.tar
CND_PACKAGE_PATH_WindowsDebugMinGW=dist/WindowsDebugMinGW/Cygwin-Linux-x86/package/libTIesrEngineSIso.so.tar
# WindowsReleaseMinGW configuration
CND_PLATFORM_WindowsReleaseMinGW=Cygwin-Linux-x86
CND_ARTIFACT_DIR_WindowsReleaseMinGW=../../Dist/WindowsReleaseMinGW
CND_ARTIFACT_NAME_WindowsReleaseMinGW=libTIesrEngineSI.dll
CND_ARTIFACT_PATH_WindowsReleaseMinGW=../../Dist/WindowsReleaseMinGW/libTIesrEngineSI.dll
CND_PACKAGE_DIR_WindowsReleaseMinGW=dist/WindowsReleaseMinGW/Cygwin-Linux-x86/package
CND_PACKAGE_NAME_WindowsReleaseMinGW=libTIesrEngineSIso.so.tar
CND_PACKAGE_PATH_WindowsReleaseMinGW=dist/WindowsReleaseMinGW/Cygwin-Linux-x86/package/libTIesrEngineSIso.so.tar
# LinuxDebugGnu configuration
CND_PLATFORM_LinuxDebugGnu=GNU-Linux-x86
CND_ARTIFACT_DIR_LinuxDebugGnu=../../Dist/LinuxDebugGnu/lib
CND_ARTIFACT_NAME_LinuxDebugGnu=libTIesrEngineSI.so.1
CND_ARTIFACT_PATH_LinuxDebugGnu=../../Dist/LinuxDebugGnu/lib/libTIesrEngineSI.so.1
CND_PACKAGE_DIR_LinuxDebugGnu=dist/LinuxDebugGnu/GNU-Linux-x86/package
CND_PACKAGE_NAME_LinuxDebugGnu=libTIesrEngineSIso.so.tar
CND_PACKAGE_PATH_LinuxDebugGnu=dist/LinuxDebugGnu/GNU-Linux-x86/package/libTIesrEngineSIso.so.tar
# LinuxReleaseGnu configuration
CND_PLATFORM_LinuxReleaseGnu=GNU_current-Linux-x86
CND_ARTIFACT_DIR_LinuxReleaseGnu=../../Dist/LinuxReleaseGnu/lib
CND_ARTIFACT_NAME_LinuxReleaseGnu=libTIesrEngineSI.so.1
CND_ARTIFACT_PATH_LinuxReleaseGnu=../../Dist/LinuxReleaseGnu/lib/libTIesrEngineSI.so.1
CND_PACKAGE_DIR_LinuxReleaseGnu=dist/LinuxReleaseGnu/GNU_current-Linux-x86/package
CND_PACKAGE_NAME_LinuxReleaseGnu=libTIesrEngineSIso.so.tar
CND_PACKAGE_PATH_LinuxReleaseGnu=dist/LinuxReleaseGnu/GNU_current-Linux-x86/package/libTIesrEngineSIso.so.tar
# ArmLinuxDebugGnueabi configuration
CND_PLATFORM_ArmLinuxDebugGnueabi=arm-none-linux-gnueabi-Linux-x86
CND_ARTIFACT_DIR_ArmLinuxDebugGnueabi=../../Dist/ArmLinuxDebugGnueabi/lib
CND_ARTIFACT_NAME_ArmLinuxDebugGnueabi=libTIesrEngineSI.so.1
CND_ARTIFACT_PATH_ArmLinuxDebugGnueabi=../../Dist/ArmLinuxDebugGnueabi/lib/libTIesrEngineSI.so.1
CND_PACKAGE_DIR_ArmLinuxDebugGnueabi=dist/ArmLinuxDebugGnueabi/arm-none-linux-gnueabi-Linux-x86/package
CND_PACKAGE_NAME_ArmLinuxDebugGnueabi=libTIesrEngineSIso.so.tar
CND_PACKAGE_PATH_ArmLinuxDebugGnueabi=dist/ArmLinuxDebugGnueabi/arm-none-linux-gnueabi-Linux-x86/package/libTIesrEngineSIso.so.tar
# ArmLinuxReleaseGnueabi configuration
CND_PLATFORM_ArmLinuxReleaseGnueabi=arm-none-linux-gnueabi-Linux-x86
CND_ARTIFACT_DIR_ArmLinuxReleaseGnueabi=../../Dist/ArmLinuxReleaseGnueabi/lib
CND_ARTIFACT_NAME_ArmLinuxReleaseGnueabi=libTIesrEngineSI.so.1
CND_ARTIFACT_PATH_ArmLinuxReleaseGnueabi=../../Dist/ArmLinuxReleaseGnueabi/lib/libTIesrEngineSI.so.1
CND_PACKAGE_DIR_ArmLinuxReleaseGnueabi=dist/ArmLinuxReleaseGnueabi/arm-none-linux-gnueabi-Linux-x86/package
CND_PACKAGE_NAME_ArmLinuxReleaseGnueabi=libTIesrEngineSIso.so.tar
CND_PACKAGE_PATH_ArmLinuxReleaseGnueabi=dist/ArmLinuxReleaseGnueabi/arm-none-linux-gnueabi-Linux-x86/package/libTIesrEngineSIso.so.tar
# ArmLinuxDebugGnueabiWinHost configuration
CND_PLATFORM_ArmLinuxDebugGnueabiWinHost=
CND_ARTIFACT_DIR_ArmLinuxDebugGnueabiWinHost=../../Dist/ArmLinuxDebugGnueabiWinHost/lib
CND_ARTIFACT_NAME_ArmLinuxDebugGnueabiWinHost=libTIesrEngineSI.so.1
CND_ARTIFACT_PATH_ArmLinuxDebugGnueabiWinHost=../../Dist/ArmLinuxDebugGnueabiWinHost/lib/libTIesrEngineSI.so.1
CND_PACKAGE_DIR_ArmLinuxDebugGnueabiWinHost=dist/ArmLinuxDebugGnueabiWinHost//package
CND_PACKAGE_NAME_ArmLinuxDebugGnueabiWinHost=libTIesrEngineSIso.so.tar
CND_PACKAGE_PATH_ArmLinuxDebugGnueabiWinHost=dist/ArmLinuxDebugGnueabiWinHost//package/libTIesrEngineSIso.so.tar
# ArmLinuxReleaseGnueabiWinHost configuration
CND_PLATFORM_ArmLinuxReleaseGnueabiWinHost=
CND_ARTIFACT_DIR_ArmLinuxReleaseGnueabiWinHost=../../Dist/ArmLinuxReleaseGnueabiWinHost/lib
CND_ARTIFACT_NAME_ArmLinuxReleaseGnueabiWinHost=libTIesrEngineSI.so.1
CND_ARTIFACT_PATH_ArmLinuxReleaseGnueabiWinHost=../../Dist/ArmLinuxReleaseGnueabiWinHost/lib/libTIesrEngineSI.so.1
CND_PACKAGE_DIR_ArmLinuxReleaseGnueabiWinHost=dist/ArmLinuxReleaseGnueabiWinHost//package
CND_PACKAGE_NAME_ArmLinuxReleaseGnueabiWinHost=libTIesrEngineSIso.so.tar
CND_PACKAGE_PATH_ArmLinuxReleaseGnueabiWinHost=dist/ArmLinuxReleaseGnueabiWinHost//package/libTIesrEngineSIso.so.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
