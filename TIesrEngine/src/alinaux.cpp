/*====================================================================
 * alinaux.cpp
 *
 * This source defines several functions used by the TIesrSD enrollment
 * API.
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *

 ====================================================================*/


#include "tiesr_config.h"

#include "tiesrcommonmacros.h"
#include "gmhmm_type.h"


/*--------------------------------*/
/* Defines local to this api */

#define SWAP(x,y,temp) {temp=x; x=y; y=temp;}


/*--------------------------------*/
/*
** return true if converged,  print out iteration information if enabled.
*/
short test_convergence(short new_like, short last_like, ushort iter)
{
  short  delta; 

  delta = new_like - last_like;
  PRT_ERR(printf("%2d frame average log likelihood = %8.4f",iter,new_like/64.));
  if (iter > 1) {
    PRT_ERR(printf(" improvement = %8.4f\n",delta/64.)); 
    if ( delta < 0) {
      PRT_ERR(printf(">>> log L decreased: OLD L = %d, NEW L = %d\n", last_like, new_like));
    }
  }
  else PRT_ERR(printf("\n"));
  return (delta < 1);  /* shreshood */ 
}


/* Should not be defined for live mode operation  */
#ifdef REC
extern short *mem_mfcc;
extern short *mem_d_mfcc;


/*--------------------------------*/
/*
** this could be avoided by usinge DIM-dim mfcc (currently DIM/2)
*/
void copy_feature(short feature[], ushort crt_vec, short n_mfcc)
{
  ushort d;

  for (d = 0; d < n_mfcc; d++) { /* copy static and dynamic */
    feature[d] = mem_mfcc[ crt_vec + d];
    feature[d + n_mfcc] = mem_d_mfcc [crt_vec + d];
  }
}

#endif

/*

void prt_lab_j(char str[], ushort nbr, ushort start, gmhmm_type *gv)
{
  ushort i, j;
  for (j=0; j<nbr; j++) {
    i = j + start;
    printf("%s start = %d, %2d %3d %3d len=%2d %.2d\n",str, start, j, gv->stt[i], gv->stp[i], gv->stp[i]-gv->stt[i], gv->hmm_code[i]);
  }
}
*/


/*--------------------------------*/
/*
** lab list is reversed by decoder
*/
void reverse_list(ushort low, ushort high, ushort start, gmhmm_type *gv)
{
  ushort cl = start + low, ch = start + high - 1, temp;
  while (cl < ch) {
    SWAP(gv->stp[cl],gv->stp[ch],temp);
    SWAP(gv->stt[cl],gv->stt[ch],temp);
    SWAP(gv->hmm_code[cl],gv->hmm_code[ch],temp);
    cl++;
    ch--;
  }
}
