/*=================================================================
 * engine_sdenro_init.cpp
 *
 * This source defines the API that is exposed to the user of the
 * TIesr SD recognizer for enrollment.
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *

 ====================================================================*/


#include "tiesr_config.h"

#include "gmhmm_type_common_user.h"
#include "uttdet_user.h"
#include "volume_user.h"

#include "tiesr_engine_api_sdenro.h"
#include "gmhmm_sd_api.h"



TIESR_ENGINE_SDENRO_API 
void TIesrEngineSDENROOpen (TIesrEngineSDENROType *sdenro)
{
  sdenro->Open   = TIesrSDEnroInit;
  sdenro->Close  = TIesrSDEnroRelease;
  sdenro->Init   = TIesrSDMemInit;
  sdenro->Enroll = TIesrSDEnro;
  
#ifndef USE_AUDIO
  sdenro->Load   = TIesrSDEnrLoad;
#endif
  sdenro->GetScore = TIesrSDEnrGetSco;
  sdenro->GetRecordData = TIesrSDEnroData;

  sdenro->RecordOpen = TIesrSDRecordOpen;
  sdenro->Record = TIesrSDRecord;
  sdenro->RecordClose = TIesrSDRecordClose;
  sdenro->GetFrameCount = GetFrameCount;
  sdenro->SpeechEnded = SpeechEnded;
  sdenro->SpeechDetected = SpeechDetected;

  sdenro->GetVolumeStatus = GetVolumeStatus; 

#ifdef USE_AUDIO
  sdenro->SetSADPrams = SetTIesrSAD;
  sdenro->SetVolumeRange = SetTIesrVolRange; 
  sdenro->GetSADPrams = GetTIesrSAD;
  sdenro->GetVolumeRange = GetTIesrVolRange; 
#endif

  return ;
} 

TIESR_ENGINE_SDENRO_API void TIesrEngineSDENROClose(TIesrEngineSDENROType *)
{
  return;
}

