/*==============================================================

 *
 * filemode.cpp
 *
 * This source defines functions used by the TIesr SD API to process
 * SD enrollment files.
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *

 ======================================================================*/

#include "tiesr_config.h"

#include "tiesrcommonmacros.h"

#include "pack_user.h"
#include "mfcc_f_user.h"
#include "search_user.h"
#include "filemode_user.h"



/*
** this version is only valid for BIT8FEAT. 
*/

/*----------------------------------------------------------------*/
/*
** calculate regression coeffi (delta-mfcc), and pack into 8 bit representation
*/
static void get_feature_vector(short idx, ushort *feat, short *mfcc_feature, gmhmm_type *gv)
{
  ushort i;
  
  /* buffer primed, type 1 */
  compute_regression(idx, 1,  mfcc_feature + gv->n_mfcc,gv->n_mfcc, gv->muScaleP2, gv->mfcc_buf);

  for (i = 0; i < gv->n_mfcc; i++) mfcc_feature[i] = gv->mfcc_buf[idx][i];

  vector_packing(mfcc_feature, feat, gv->scale_feat, gv->n_mfcc);
} 

/*
** mfcc -> mfcc+dmfcc, inplace (of mfcc array)
*/

void get_regression_sequence(gmhmm_type *gvv, ushort start_frame, ushort total_frm_cnt)
{
  gmhmm_type *gv = (gmhmm_type *)gvv;
  short mfcc_feature[ MAX_DIM ];   
  short idx = 0, type; /* referenced? */
  short i, j;
  ushort mfcc_idx;
  short idx2, idx3;                  /* to prime buffer, HTK compatible */
  short  *mfcc = (short*) gv->mem_feature + start_frame * gv->n_mfcc;

  /* feat points to the same location as mfcc, in-place operation! */
  ushort *feat = (ushort *) mfcc;
  ushort frm_count = 0;
  short frm_cnt_frontend; 

  /* compute regression and search */

  for (mfcc_idx = 0, frm_cnt_frontend = 0; frm_cnt_frontend < total_frm_cnt;
       mfcc_idx += gv->n_mfcc, frm_cnt_frontend++) {

    /* put into circular buffer, for regression computation */
    idx = circ_idx( frm_cnt_frontend );
    for (i = 0; i < gv->n_mfcc; i++) {
      gv->mfcc_buf[ idx ][i] = mfcc[ mfcc_idx + i];
    }
    
    if ( frm_cnt_frontend == 0 ) {                /* prime the buffer, HTK compatible */
      for (i = 1; i < MFCC_BUF_SZ; i++) {
	for (j = 0; j < gv->n_mfcc; j++) {
	  gv->mfcc_buf[i][j] = gv->mfcc_buf[0][j];
	}
      }
    }      

    if ( frm_cnt_frontend >= REG_SPAN ) {

      type = frm_cnt_frontend < (REG_SPAN * 2) ? 0 : 1;    /* beg or regular ? */
      idx = circ_idx( idx - REG_SPAN );

      get_feature_vector(idx,feat, mfcc_feature,gv);
      frm_count++;
      feat += gv->n_mfcc;
    }
  }
  
  /* end type regression frames */
  for (j = 0;  frm_count < total_frm_cnt && j < REG_SPAN; j++) {

    idx  = circ_idx( idx + 1 );
    idx2 = circ_idx( idx + 1 );
    idx3 = circ_idx( idx + 2 );
    for (i = 0; i <  gv->n_mfcc; i++)                  /* prime the buffer */
      gv->mfcc_buf[idx3][i] =  gv->mfcc_buf[idx2][i];

    get_feature_vector(idx,feat, mfcc_feature, gv);
    frm_count++;
    feat +=  gv->n_mfcc;
  }
}

/*--------------------------------*/
/* This function does not seem to be used at this time
void Get_regression_sequence(void *gvv, ushort start_frame)
{
  gmhmm_type *gv = (gmhmm_type *)gvv;
  get_regression_sequence(gvv, start_frame, gv->frm_cnt);
}
*/


/*--------------------------------*/
/*
** recognizing one utterance, without SAD.
** mem_mfcc contains mfcc+dmfcc (in 8+8 bits)
*/
TIesrEngineStatusType process_a_file_1( gmhmm_type *gv, ushort frm_cnt, ushort start_frame)
{
  short status;
  short mfcc_feature[ MAX_DIM ];   
  ushort *feat = (ushort *)gv->mem_feature + start_frame * gv->n_mfcc; 
  ushort fm;



  for (fm = 0; fm < frm_cnt; fm++) {
    vector_unpacking(feat, mfcc_feature, gv->scale_feat,  gv->n_mfcc);
    /* search entry */
    status = search_a_frame(mfcc_feature, 1, fm, gv);
    if( status != eTIesrEngineSuccess )
    {
      return (TIesrEngineStatusType)status;
    }
    feat += gv->n_mfcc;
  }

  return eTIesrEngineSuccess;
}

/*--------------------------------*/
/* This function does not seem to be used at this time 
TIesrEngineStatusType process_a_file(void *gvv, ushort start_frame)
{
  gmhmm_type *gv = (gmhmm_type *) gvv;
  return (process_a_file_1(gv, gv->frm_cnt, start_frame));
}
*/

/*
** filler function 
*/

/*--------------------------------*/
/* This function does not seem to be used at this time 
TIesrEngineStatusType CallSearchEngine(short *, void *gvv)
{
  return eTIesrEngineSuccess;
}
*/
