/*===============================================================
 *
 * filemode_user.h
 *
 * This header exposes the interface used by the TIesr SD API that
 * processes model data.
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 ==================================================================*/

#ifndef _FILEMODE_USER_H
#define _FILEMODE_USER_H

#include "tiesr_config.h"

#include "tiesrcommonmacros.h"
#include "status.h"
#include "gmhmm_type.h"


void get_regression_sequence(gmhmm_type *gvv, ushort start_frame,
			     ushort total_frm_cnt);

TIesrEngineStatusType process_a_file_1( gmhmm_type *gv, ushort frm_cnt, 
					ushort start_frame);

#endif /* _FILEMODE_USER_H */
