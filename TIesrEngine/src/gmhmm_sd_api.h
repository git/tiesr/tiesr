/*===============================================================
 *
 * gmhmm_sd_api.h
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 ==================================================================*/

#ifndef _GMHMM_SD_API_H
#define _GMHMM_SD_API_H

// #include "use_sd_dll.h" 

/*
** TIesr SDSR ENGINE API
*/

#include "tiesr_config.h"

#include "tiesrcommonmacros.h"
#include "status.h"
#include "gmhmm_type.h"

#include "sd_enro_type.h"
#include "sd_reco_type.h"
#include "sdparams.h"


/*
** SDSR API functions
*/

/*--------------------------------*/
/* Common functions to both Enrollment and Recognition */


/*
** record utterance with SAD
*/
/* GMHMM_SD_API */ 
void  TIesrSDRecordOpen_1(gmhmm_type *);


/* GMHMM_SD_API */ 
void  TIesrSDRecordOpen(gmhmm_type *gvv );


/* GMHMM_SD_API */ 
TIesrEngineStatusType TIesrSDRecord(gmhmm_type *gvv, short sig[],
				    short calc_noise_estimate   /* 0: no noise estimation (for enroll), 1: with noise estimation (for reco)*/
);


/* GMHMM_SD_API */ 
TIesrEngineStatusType TIesrSDRecordClose(gmhmm_type *gvv);



/*--------------------------------*/
/*
**  Recognition API
*/

/*
** initialization of SD recognizer
*/
/* GMHMM_SD_API */ void TIesrSDRecInit(sd_reco_type *sdvv);

/*
** initialization of the SDSR, to be called at beginning of recognition
*/
/* GMHMM_SD_API */ 
TIesrEngineStatusType TIesrSDRecOpen(sd_reco_type *sdvv,
				     int total_mem_size,
				     int max_frames );
 
 
/*
** extract gmhmm_type from sdreco_type
*/
/* GMHMM_SD_API */ 
gmhmm_type* TIesrSDRecoData(sd_reco_type *sdvv);


/*
** set pruning threshold
*/
/* Now in gmhmm_type_common_user.h as Get/SetTIesrPrune 
void SetTIesrSDPrune(void *gv, short value);
void GetTIesrSDPrune(void *gv, short *value);
*/


/*
** restore 
*/
/* GMHMM_SD_API */ 
void TIesrSDRecClose(sd_reco_type *sdvv);


/*
** create background models
*/
/* GMHMM_SD_API */ 
TIesrEngineStatusType TIesrSDBgd(unsigned short nbr_words, char *directory, char *name_list[], sd_reco_type *sdvv);


/*
** confidence score
*/
/* GMHMM_SD_API */ short TIesrSDConf(short spotting, sd_reco_type *);


#ifndef USE_AUDIO
/*
** load test utterance from file, with no utterance detection.
*/
/* GMHMM_SD_API */ 
unsigned short  TIesrSDRecLoad(char fname[], sd_reco_type *);
#endif

/*
** scoring one models, given utterance
*/
/* GMHMM_SD_API */ 
TIesrEngineStatusType TIesrSDSco(char *dir, char item[], long *sco, 
				 sd_reco_type *sdvv);



/*--------------------------------*/
/*
**  Enrollment API:
*/

/*
** To be done before enrolling every item or before loading a reco item
** But this is only used during enrollment processing.
*/
/* GMHMM_SD_API */
void TIesrSDMemInit(sd_enro_type *sdee);


/*
** load initial model
*/
/* GMHMM_SD_API */ 
TIesrEngineStatusType TIesrSDEnroInit(char *path, sd_enro_type * sdee, 
				      int total_mem_size, ushort max_frm );


/*
** free memory for initial model
*/
/* GMHMM_SD_API */ 
TIesrEngineStatusType TIesrSDEnroRelease(sd_enro_type *sdee);


/*
** score (log likelihood) in Q6:
*/
/* GMHMM_SD_API */ 
short TIesrSDEnrGetSco(sd_enro_type *);


/*
** extract gmhmm_type from sdenro_type
*/
/* GMHMM_SD_API */ 
gmhmm_type* TIesrSDEnroData(sd_enro_type *);


/*
** enrol one item 
*/
/* GMHMM_SD_API */ 
TIesrEngineStatusType TIesrSDEnro(char *path, sd_enro_type *sdee);


#ifndef USE_AUDIO
/*
** load test utterance from file, with no utterance detection.
*/
/* GMHMM_SD_API */ 
TIesrEngineStatusType TIesrSDEnrLoad(char fname[], sd_enro_type *sdee);
#endif



#endif 
