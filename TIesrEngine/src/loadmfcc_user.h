/*=======================================================================
 
 *
 * loadmfcc_user.h
 *
 * This header exposes the interface used in TIesrSD for mfcc processing.
 *
 * Copyright (C) 2001-2010 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
  
 ====================================================================*/

#ifndef _LOADMFCC_USER_H
#define _LOADMFCC_USER_H

#include "tiesr_config.h"
#include "tiesrcommonmacros.h"

#include "status.h"
#include "gmhmm_type.h"

TIesrEngineStatusType load_mfcc_1(gmhmm_type *gvv, short mfcc[], 
				  ushort max_frms, char fname[]);

TIesrEngineStatusType Load_mfcc_(gmhmm_type *gvv, char fname[]);

#endif /* _LOADMFCC_USER_H */
