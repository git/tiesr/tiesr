/*================================================================
 
 *
 * main-sdenro.cpp
 *
 * This source defines the main enrollment functions used by the
 * TIesr SD enrollment API.
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *

 ======================================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <limits.h>


#include "tiesr_config.h"

#include "status.h"
#include "tiesrcommonmacros.h"
#include "gmhmm_type.h"
#include "mfcc_f_user.h"
#include "volume_user.h"
#include "uttdet_user.h"
#include "load_user.h"


#include "sd_enro_type.h"
#include "gmhmm_sd_api.h"
#include "sdenro_user.h"
#include "loadmfcc_user.h"
#include "sdenro.h"
/* #include "sdhead.h" */



/*--------------------------------*/
/* Local defines */

#define MFCC_FRAME_SIZE(enro) (NBR_ENR_UTR  * enro->gv->max_frame_nbr)



/*--------------------------------*/
/*
** to be done before enrolling every item
*/
/* GMHMM_SD_API */ 
void TIesrSDMemInit(sd_enro_type *sdee)
{
  sd_enro_type *enro = (sd_enro_type *) sdee; 
  enro->gv->mem_start_frame = 0;
  //  enro->gv->mem_start_index = 0;
  enro->z_base_mu = NULL; 
  enro->gv->nbr_loaded_utter = 0;
}  


/*--------------------------------*/
/*
** load initial model
*/
/* GMHMM_SD_API */ 
TIesrEngineStatusType TIesrSDEnroInit(char *path, sd_enro_type *sdData, 
				      int total_mem_size, ushort max_frm) 
{
  TIesrEngineStatusType status;
  sd_enro_type *sd_enro;
  int curt_mem;       /* size of used memory, in short */
  
  sd_enro = (sd_enro_type *) sdData;
  sd_enro->stt_mem = 0;    /* index of first available memory space */
  sd_enro->ttl_mem = total_mem_size;

  
  curt_mem = (sizeof(sd_enro_type) + 1) >> 1;
  sd_enro->stt_mem += curt_mem;
  display_size("sd_enro_type",curt_mem, sd_enro->stt_mem, total_mem_size);
  if (sd_enro->stt_mem > total_mem_size)  return eTIesrEngineMemorySizeFail;

  sd_enro->gv = (gmhmm_type *)((short *)sdData + sd_enro->stt_mem);
  curt_mem = (sizeof(gmhmm_type) + 1) >> 1;
  sd_enro->stt_mem += curt_mem;
  display_size("gmhmm_type memory",curt_mem, sd_enro->stt_mem, total_mem_size);
  if (sd_enro->stt_mem > total_mem_size)  return eTIesrEngineMemorySizeFail;


  /* Updated: Load models for finding speech segments. Moved here in order to get
   model parameters to set sym2pos arrays */
  status = load_models(path, sd_enro->gv, FALSE, NULL, NULL); /* should be moved into file loop */
  if (status != eTIesrEngineSuccess) return eTIesrEngineModelFileFail; /* model file openning fail */


  sd_enro->search_mem = (short *)sdData + sd_enro->stt_mem;  /* starting memory for search */
  curt_mem = ALL_MEM_SIZE;
  sd_enro->stt_mem += curt_mem;
  display_size("search memory",curt_mem, sd_enro->stt_mem, total_mem_size);
  if (sd_enro->stt_mem > total_mem_size)  return  eTIesrEngineSearchMemoryOut;

  /* Updated: Allocate for sym2pos map arrays */
  sd_enro->gv->sym2pos_map.syms = (unsigned short *)( (short *)sdData + sd_enro->stt_mem);
  curt_mem = sd_enro->gv->trans->n_sym * sd_enro->gv->trans->n_set + 1;
  sd_enro->stt_mem += curt_mem;
  if( sd_enro->stt_mem >= total_mem_size )
     return eTIesrEngineSym2PosMapSize;

  sd_enro->gv->sym2pos_map.sym2pos = ( (short *)sdData + sd_enro->stt_mem );
  sd_enro->stt_mem += curt_mem;
  if( sd_enro->stt_mem >= total_mem_size )
     return eTIesrEngineSym2PosMapSize;

  sd_enro->gv->sym2pos_map.max_nbr_syms = curt_mem;


  sd_enro->gv->max_frame_nbr = max_frm;

  sd_enro->gv->mem_feature = (ushort *) sdData + sd_enro->stt_mem;  /* starting memory for search */
  curt_mem = MFCC_FRAME_SIZE(sd_enro) * SD_N_MFCC;
  sd_enro->stt_mem += curt_mem;
  display_size("utterance memory",curt_mem, sd_enro->stt_mem, total_mem_size);
  if (sd_enro->stt_mem > total_mem_size)  return eTIesrEngineFeatureFrameOut;


  if (sd_enro->gv->n_mfcc != SD_N_MFCC)  return eTIesrEngineFeatureDimensionFail; /* size mismatch */
  sd_enro->gv->nbr_dim = sd_enro->gv->n_mfcc * 2;
  sd_enro->gv->scale_feat = (short *)new_scale_mu;

  dim_p2_init(sd_enro->gv->n_mfcc, sd_enro->gv); 
  sd_enro->gv->prune = (-10)*(1<<6);    /* search pruning */
  sd_enro->gv->tranwgt = (3)*(1<<6);
 
  sd_enro->gv->nbr_loaded_utter = 0;
  sd_enro->gv->mem_start_frame = 0;

  sd_enro->gv->low_vol_limit  = LOW_VOL_LIMIT;
  sd_enro->gv->high_vol_limit = HIGH_VOL_LIMIT;

  /* Set default utterance detection parameters.  The API user may
     subsequently change them. */
  set_default_uttdet( sd_enro->gv );


  return eTIesrEngineSuccess;
}

/*--------------------------------*/
/* GMHMM_SD_API */ 
TIesrEngineStatusType TIesrSDEnroRelease(sd_enro_type *sdee)
{  
  sd_enro_type *enro = (sd_enro_type *) sdee; 
  free_save_models(enro); 
  return eTIesrEngineSuccess;
}


/*--------------------------------*/
/*
** score (log likelihood) in Q6:
*/
/* GMHMM_SD_API */ 
short TIesrSDEnrGetSco(sd_enro_type *sdee)
{
  sd_enro_type *enro = (sd_enro_type *) sdee; 
  return enro->enro_sco;
}


/*--------------------------------*/
/*
** enroll one item 
*/
/* GMHMM_SD_API */
TIesrEngineStatusType TIesrSDEnro(char *path, sd_enro_type *sdee)
{
  TIesrEngineStatusType  status;
  sd_enro_type *enro = (sd_enro_type *) sdee;
  gmhmm_type *gv = enro->gv;

  status = (TIesrEngineStatusType)
    enroll_one_name(gv->n_mfcc,gv->total_frames,NBR_ENR_UTR, silence_code,bws_code, 
		    path, gv->nbr_dim, &(enro->enro_sco), gv, enro);
  return status;  
}


/*--------------------------------*/
/*
** load test utterance from file, with no utterance detection.
** memory released after the call.
*/
/* GMHMM_SD_API */  
TIesrEngineStatusType TIesrSDEnrLoad(char fname[], sd_enro_type *sdee)
{
  TIesrEngineStatusType status;
  sd_enro_type *enro = (sd_enro_type *) sdee;
  gmhmm_type *gv = enro->gv;
  
  if (enro->gv->nbr_loaded_utter == NBR_ENR_UTR) return eTIesrEngineEnrollLoadFail;
  else { 
    status = load_mfcc_1(gv, (short *)gv->mem_feature + enro->gv->mem_start_frame * SD_N_MFCC, 
			 MFCC_FRAME_SIZE(enro) - enro->gv->mem_start_frame, fname);
    if (status != eTIesrEngineSuccess) return status;
    else {
      gv->total_frames[enro->gv->nbr_loaded_utter] = gv->frm_cnt;
      enro->gv->mem_start_frame += gv->frm_cnt;
      enro->gv->nbr_loaded_utter++;
      return eTIesrEngineSuccess;
    }
  }
}


/*--------------------------------*/
/* GMHMM_SD_API */ 
gmhmm_type* TIesrSDEnroData(sd_enro_type *sdee)
{
   sd_enro_type *enro = (sd_enro_type *) sdee;
   return enro->gv;
}
