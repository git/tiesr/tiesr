/*==============================================================

 *
 * main-sdreco.cpp
 *
 * This source defines the main recognition functions used by the
 * TIesr SD recognition API.
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *

 ====================================================================*/

#include <stdio.h>

#include "tiesr_config.h"
#include "status.h"
#include "tiesrcommonmacros.h"

#include "gmhmm_type.h"
#include "mfcc_f_user.h"
#include "search_user.h"
#include "volume_user.h"
#include "uttdet_user.h"

/* #include "sdhead.h" */
#include "sdreco.h"
#include "sd_reco_type.h"
#include "sdreco_user.h"
#include "loadmfcc_user.h"
#include "sdbackgrd_user.h"
#include "gmhmm_sd_api.h"


/* Some other local constant allocations */
#include "sdvar_header.h"


/*--------------------------------*/
/* Local defines and allocations */
/* 
** scale for name dialing reco
*/
const short zero_scale_mu[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,}; /* 16 dim */


static ushort stt[SD_MAX_NBR_SEGS];      /* start frame  */


/*--------------------------------*/
/*
** initialization of SD recognizer (per utterance)
*/
/* GMHMM_SD_API */ void TIesrSDRecInit(sd_reco_type *sdvv)
{
  sd_reco_type *sdv = (sd_reco_type *) sdvv;
  
  sdv->memo.best_length = sdv->gv->frm_cnt;
  sdv->memo.best_sco = -2147483647; /* -2^31-1 */  // BAD_SCR;
}


/*--------------------------------*/
/*
** scoring one models, given utterance
*/
/* GMHMM_SD_API */ 
TIesrEngineStatusType TIesrSDSco(char *dir, char item[], long *the_sco, sd_reco_type *sdvv )
{
  // !!! ushort answer[ 1 ], nbr_ans; /* zero answer (word) for name dialing */

  sd_reco_type *sdv = (sd_reco_type *) sdvv;
  gmhmm_type *gv = sdv->gv;
  short status;
  short bt;

  /* Use HMM-based search, not word-based search. Keep track of and restore
  search type on exit. */
  bt = gv->word_backtrace;
  gv->word_backtrace = HMMBT;


  load_1_net_and_gtm( dir, item, TOTAL_MODEL_SIZE, sdv->clusters, sdv->nbr_class, gv, sdv );
  pmc_all_hmms( gv );
  status = rec_process_a_file( gv );
  *the_sco = gv->best_sym_scr;

  gv->nbr_seg = 0;

  //      if ( status == eTIesrEngineSuccess ) search_a_frame( 0, total_frames - 1);
  if( status == eTIesrEngineSuccess )
  {
    if( gv->best_sym != USHRT_MAX ) /* backtrace and print result */
    {
      status = back_trace_beam( gv->best_sym, gv->frm_cnt - 1, NULL, stt, NULL,
              &( gv->nbr_seg ), &( gv->hmm_dlt ), gv );

      if( *the_sco > sdv->memo.best_sco )
      {
        sdv->memo.best_sco = *the_sco;
        sdv->memo.best_length = stt[0] - stt[gv->nbr_seg - 2]; /* speech duration */
      }
    }

    else
    {
      status = eTIesrEngineAlignmentFail;
    }
  }

  gv->word_backtrace = bt;
  
  if( gv->nbr_seg > SD_MAX_NBR_SEGS ) status = eTIesrEngineSegmentMemoryOut;
  return (TIesrEngineStatusType) status;
}


/*--------------------------------*/
/*
** confidence score
*/
/* GMHMM_SD_API */ 
short TIesrSDConf(short spotting, sd_reco_type *sdvv)
{
    sd_reco_type *sdv = (sd_reco_type *) sdvv;
    gmhmm_type *gv = sdv->gv;
    long bgd_sco;
    short status;
/* begin background */
    make_bgd_net_and_gtm(sdv->clusters, sdv->nbr_class, TOTAL_MODEL_SIZE, gv, sdv);
    pmc_all_hmms( gv );
    /* I fully  tested this */
#ifdef REC 
    printf("speech stt = %d stp = %d\n", best_stt, best_stp);

    if (best_stt <= 1) {
      b_H = -30 * 64; /* too short, decoder gives too low score */
      printf("***");
    }
    
    else b_H = rec_process_a_file(best_stt,mem_mfcc);

    printf("bgd H score = %f %ld D=%d\n",  b_H/64./best_stt, b_H , best_stt);    

    b_M = rec_process_a_file(best_stp - best_stt, mem_mfcc +  SD_N_MFCC*best_stt);
    printf("bgd M score = %f %ld D=%d\n",  b_M/64./(best_stp - best_stt), b_M,(best_stp - best_stt) );    

    
    if (total_frames - best_stp <= 1) {
      b_T = -30 * 64;
      printf("***");
    }
    else b_T = rec_process_a_file(total_frames - best_stp, mfcc +  SD_N_MFCC * best_stp);
    printf("bgd T score = %f %ld D=%d\n",  b_T/64./(total_frames - best_stp), b_T, (total_frames - best_stp));    

    
    bgd_sco =  (sco[0].sco * total_frames - b_H - b_T - b_M)/(best_stp - best_stt);
    printf("new diff score = %f\n",  bgd_sco /64.);
#endif
    /* conventional background score  */
    status  = rec_process_a_file(gv);
    bgd_sco = gv->best_sym_scr;
    if (spotting) /* division is just for scaling and display */
      return (short)((sdv->memo.best_sco - bgd_sco ) / (sdv->memo.best_length)); /* in Q6 */
    else 
      return (short)((sdv->memo.best_sco - bgd_sco ) / gv->frm_cnt);       /* in Q6 */
}


/*--------------------------------*/
/*
** initialization of the SDSR, to be called at beginning of recognition
*/
/* GMHMM_SD_API */ 
TIesrEngineStatusType TIesrSDRecOpen(sd_reco_type *sdvv, int total_mem_size, int max_frames)
{
  short *search_space, i; 
  sd_reco_type *sdv;
  int curt_mem;    /* size of used memory, in short */
  int stt_mem = 0; /* index of first available memory space */
  
  sdv = (sd_reco_type *) sdvv;
  curt_mem = (sizeof(sd_reco_type) + 1) >> 1;
  stt_mem += curt_mem;
  display_size("sd_reco_type",curt_mem, stt_mem, total_mem_size);
  //  if (stt_mem > total_mem_size)  return eTIesrEngineHLRGlobalMemory;
  
  sdv->gv = (gmhmm_type *)((short *)sdvv + stt_mem);
  curt_mem = (sizeof(gmhmm_type) + 1) >> 1;
  stt_mem += curt_mem;
  display_size("gmhmm_type memory",curt_mem, stt_mem, total_mem_size);
  //  if (stt_mem > total_mem_size)  return eTIesrEngineHLRGlobalMemory;


/* Updated: Allocate for sym2pos map arrays */
  sdv->gv->sym2pos_map.syms = (unsigned short *)( (short *)sdvv + stt_mem);
  curt_mem = MAX_RECO_SYMS;
  stt_mem += curt_mem;
  if( stt_mem >= total_mem_size )
     return eTIesrEngineSym2PosMapSize;

  sdv->gv->sym2pos_map.sym2pos = ( (short *)sdvv + stt_mem );
  stt_mem += curt_mem;
  if( stt_mem >= total_mem_size )
     return eTIesrEngineSym2PosMapSize;

  sdv->gv->sym2pos_map.max_nbr_syms = curt_mem;

  /* Updated: allocate space for Gaussian cache, since models are not
   read in during initialization */
  sdv->gv->gauss_scr = (short*)sdvv + stt_mem;
  curt_mem = MAX_RECO_GAUSS;
  stt_mem += curt_mem;
  if( stt_mem >= total_mem_size )
     return eTIesrEngineSearchMemorySize;


  /* reuse search memory 
  sdv->mem_blc = (long *)sdvv + stt_mem;
  stt_mem += curt_mem;
  display_size("mu accumulation",curt_mem, stt_mem, total_mem_size);
  */

  sdv->mem_base = (ushort *)sdvv + stt_mem;  /* starting memory location after clusters */
  curt_mem = TOTAL_MODEL_SIZE;
  stt_mem += curt_mem;
  display_size("model size",curt_mem, stt_mem, total_mem_size);
  if (stt_mem > total_mem_size)  return  eTIesrEngineModelSize;

  sdv->gv->mem_feature = (ushort*) sdvv + stt_mem;   /* storing the whole utterance in RAM */
  curt_mem = (max_frames * SD_N_MFCC);
  stt_mem += curt_mem;
  display_size("utterance RAM",curt_mem, stt_mem, total_mem_size);
  if (stt_mem > total_mem_size)  return   eTIesrEngineFeatureMemoryOut;
  sdv->gv->max_frame_nbr = max_frames; 

  search_space = (short *) sdvv + stt_mem; /* search memory */
  if (TOTAL_SEARCH_SIZE < sizeof(long)*(MUSIZE + SDDIM) / sizeof(short)) return  eTIesrEngineMuSpaceMemoryOut;
  curt_mem = TOTAL_SEARCH_SIZE;
  stt_mem += curt_mem;
  display_size("search space",curt_mem, stt_mem, total_mem_size);

  if (stt_mem > total_mem_size) return eTIesrEngineSearchMemoryOut;

  sdv->mem_blc =  (long *)search_space ;
  set_search_space(search_space, BEAM_Z, SYMB_Z, STATE_Z, TIME_Z, sdv->gv);

  //  const_init(); 
  dim_p2_init(SD_N_MFCC,sdv->gv); 
  sdv->gv->nbr_dim = SD_N_MFCC * 2; /* !!! compared it to sdauxl.cpp */
  sdv->gv->prune = -8 * (1<<6);  /* -8 */
  sdv->gv->tranwgt = (3)*(1<<6);   


  sdv->org_scale_mu = sdv->gv->scale_mu;
  sdv->org_scale_var = sdv->gv->scale_var;
 

  sdv->_scale_var = Z_scale_var;
  sdv->var_1 = Z_var_1;
  sdv->unique_pr = Z_unique_pr;

  sdv->gv->scale_mu = (short *)zero_scale_mu;
  sdv->gv->scale_var = (short*)sdv->_scale_var;

  sdv->gv->last_sig = 0;
  //  sdv->total_mem_size = total_mem_size;

  sdv->gv->low_vol_limit  = LOW_VOL_LIMIT;
  sdv->gv->high_vol_limit = HIGH_VOL_LIMIT;

  sdv->gv->n_mfcc = SD_N_MFCC;
 
  sdv->gv->nbr_loaded_utter = 0;
  sdv->gv->mem_start_frame = 0;

  /* Set default utterance detection parameters on initialization of 
     the gmhmm_type structure.  The user of the API may subsequently 
     change these */
  set_default_uttdet( sdv->gv );

 /*  printf("MEMORY SIZE FOR SD RECO (SEARCH %d + MODEL %d) = %d (words)\n",
  **	 TOTAL_SEARCH_SIZE,TOTAL_MODEL_SIZE, MAX_MEM);
  **  will print this:  MEMORY SIZE FOR SD RECO (SEARCH 2200 + MODEL 2000) = 4200 (words)
  */
  for (i = 0; i < sdv->gv->n_filter; i++)  sdv->gv->log_H[i] = 0; 
  return eTIesrEngineSuccess;
}


/*--------------------------------*/
/* GMHMM_SD_API */ 
TIesrEngineStatusType TIesrSDBgd(ushort nbr_words, char *directory, char *name_list[], sd_reco_type *sdvv)
{
  sd_reco_type *sdv = (sd_reco_type *) sdvv;
  gmhmm_type *gv = sdv->gv;

/*
** backup the variable for enro
*/
  short *org_scale_mu, *org_scale_var;

  org_scale_mu = gv->scale_mu;
  org_scale_var = gv->scale_var;
 
  gv->scale_mu = (short *) zero_scale_mu;
  gv->scale_var = (short *)sdv->_scale_var;
  sdv->nbr_class = TIesrsdbgd(nbr_words, directory, name_list, gv, sdv);

  gv->scale_mu  = org_scale_mu;
  gv->scale_var = org_scale_var;

  if( sdv->nbr_class == 0 ) return eTIesrEngineBackgoundCreationFail;         
  return eTIesrEngineSuccess;      
}


/*--------------------------------*/
/*
** restore scales
*/
/* GMHMM_SD_API */ 
void TIesrSDRecClose(sd_reco_type *sdvv)
{
  sd_reco_type *sdv = (sd_reco_type *) sdvv;
  gmhmm_type *gv = sdv->gv;

  gv->scale_mu  = sdv->org_scale_mu;
  gv->scale_var = sdv->org_scale_var;
}


/*--------------------------------*/
/*
** load test utterance from file, with no utterance detection.
*/
/* GMHMM_SD_API */ 
ushort TIesrSDRecLoad(char fname[], sd_reco_type *sdvv)
{
  sd_reco_type *sdv = (sd_reco_type *) sdvv;
  gmhmm_type *gv = sdv->gv;

  //  gv->mem_feature = (ushort*) ram_mfcc;
  //  gv->max_frame_nbr = max_frame; 

  /* Load_mfcc_((void *)gv, fname); */
  Load_mfcc_(gv, fname);
  return gv->frm_cnt;
}


/*--------------------------------*/
/* GMHMM_SD_API */ 
gmhmm_type* TIesrSDRecoData(sd_reco_type *sdee)
{
   sd_reco_type *reco = (sd_reco_type *) sdee;
   return reco->gv;
}
