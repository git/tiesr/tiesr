/*=================================================================
 *
 * sd_enro_type
 *
 * This header defines a structure used during TIesr SD enrollment.
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 ==================================================================*/

#ifndef _SD_ENRO_TYPE_H
#define _SD_ENRO_TYPE_H

/*
** structure for SD enrollment. Algo+data API. 
*/


#include "tiesr_config.h"
#include "gmhmm_type.h"


typedef struct sd_enro_type
{

      //short nbr_loaded_utter; /* number of utterance loaded to memory */
      short *mem_mfcc; 
      ushort nbr_frames; /* redundant ? */
  
      short *org_scale_mu;    /* backup the variable for enro */
      short *org_scale_var;

      const short *_scale_var;
      const short *var_1;
      const short *unique_pr;

      ushort *mem_base;       /* location for (grammar + hmm) */
      ushort nbr_class;       /* number of clusters of the background model */

      /* save initial models: */
      short *z_base_mu;
      short *z_base_gconst;
      short *z_base_var;
      unsigned short *z_base_hmms;
      short *z_base_tran;
      unsigned short *z_base_pdf;
      short *z_base_mixture;
      short *z_obs_scr;
      short *z_base_net;
      short *z_base_mu_orig;
      unsigned short  z_n_mu;
      unsigned short  z_n_pdf;

      short enro_sco;         /* score after enrollment */
      ushort stt_mem;         /* index of first available memory space */
      int    ttl_mem;         /* total memory size given by the application */
      ushort max_frm;         /* maximum number of frames PER utterance */

/*
** search memory + storage for segment information: 
*/

      short *search_mem;

      //  ushort total_mem_size;  /* total memory (in short) given by ther application */
  
      //  ushort total_mem_base[ MAX_MEM ]; /* MEMORY FOR RECOGNITION: */
/*
** this memory could be part of mem_base: only needed during clustering:
*/
      long *mem_blc ; /* shared w/ search space 
			 [MUSIZE + SDDIM] accumulation of mu,  the last SDDIM is for sum[] */
  
      gmhmm_type *gv;         /* recognizer structure.   storing the whole utterance in RAM, in 8 bits */
} sd_enro_type;


#endif /* _SD_ENRO_TYPE_H */
