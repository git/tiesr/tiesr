/*============================================================
 * sdauxl.cpp
 *
 * Functions used throughout TIesrSD enrollment and recognition.
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *

 ===================================================================*/

/*
** This file contains functions shared by reco and enrol
*/


#include "tiesr_config.h"
#include "sdhead.h"
#include "sdauxl_user.h"


/* Only if OUTPUT_NET is defined, which should not be the case for live processing */
/*extern void output_network(short *base_net); */ 


/*--------------------------------*/
/* Defines used within this code only  */

/* network size as function of symbols: 
** n-1 emission HMM n+2 sym type (2: exit states)
*/
#define TOTAL_NET_SIZE(n_sym) (sizeof(TransType)/sizeof(short) /* size of transtype */ \
                               + (n_sym - 1) /* minus space occupied by symlist[0] */ \
                               + (n_sym + 2) * sizeof(SymType)/sizeof(short)) /* space for symbtype */

#define NBR_FIELDS 2  /* used in make_net */


#define NBR_VAR 2 /* number of variances for the name */
#define MAX_NBR_STATES (MAX_NBR_EM_STATES + 1)
#define LOG2Q6 44 /* log 2 in Q 6 */

/* This does not seem to be used anywhere at this time.
#define hmm_bgd(N) ((N)-2)
*/

//  remove the following comment when you need to see memory usage!
//#define SHOW_MEMORY 


/*--------------------------------*/
/* return 0 if fail, 1 if pass */
short cap_in_check(char *mesg, ushort value, ushort max_mem)
{
#ifdef SHOW_MEMORY
  printf("%3d words (MAX = %3d) %s\n", value, max_mem, mesg); 
#endif
  if (value > max_mem ){
#ifdef SHOW_MEMORY
    fprintf(stderr,"maximum size capacity exceeded at \"%s\"\n", mesg);
    exit(1);
#endif
    return 0;
  }
  else return 1;
}


/* OUTPUT_NET should not be defined for live mode. */

#ifdef OUTPUT_NET
/*--------------------------------*/
void prt_spot_one_sym(SymType *p_sym, ushort s)
{
  ushort n;
  void prt_hmm(HmmType *hmm, char str[]);
  HmmType *hmm;
  extern short     *base_hmms;
  
  printf("symbl %2d: hmm code = %2d number of next = %2d [", 
	   s, p_sym->hmm_code, p_sym->n_next);
  for (n=0; n < p_sym->n_next; n++) printf("%2d ", p_sym->next[n]);
  printf("]\n");
  printf("HMM:\n");
  hmm = GET_HMM(base_hmms, p_sym->hmm_code, 0);
  prt_hmm(hmm,"hmm");
}


/*--------------------------------*/
/*
** print, in ASCII form, a network. Need only for debug.
*/
void prt_spot_network(short *base_net)
{
  ushort s;
  TransType *trans;
  
  trans = (TransType *) base_net;
  printf("Entry state:\n");
  prt_spot_one_sym((SymType *) (base_net + trans->start), 0);
  printf("Exit state:\n");
  prt_spot_one_sym((SymType *) (base_net + trans->stop), 0);
  printf("Other states:\n");  
  for (s = 0; s < trans->n_sym; s++) 
    prt_spot_one_sym( (SymType *)(base_net + trans->symlist[s]), s);
}

#endif /* OUTPUT_NET */


/*--------------------------------*/
/*
** make a network for name decoding.
** n is the number of HMMs including 1 SIL (there are two)
** silence uses the last code
*/
TIesrEngineStatusType make_net(gmhmm_type *gv, ushort n, ushort *mem_count, short *mem_base, ushort max_mem)
{
  int cnt /* in number of shorts */;
  ushort *p_cnt, s, total_words;
  SymType *p_sym, *p_sym0; /* the full network (including entry&Exit states) */

  gv->base_net = mem_base + *mem_count;
  gv->trans = (TransType *) gv->base_net;

  gv->trans->n_sym = n + 1; /* plus 1, for h#  */

  total_words = TOTAL_NET_SIZE(gv->trans->n_sym);
  *mem_count += total_words;
  /*
  ** print total memory usage: 
  */
  if (!cap_in_check("total network memory", *mem_count, max_mem)) return eTIesrEngineNetworkMemorySize;
  

  gv->trans->n_hmm = n;     /* including one h# */
  gv->trans->n_word = 0;    /* no word information needed for name dialing */
  gv->trans->n_set = 1;
  gv->trans->n_hmm_set =  gv->trans->n_hmm / gv->trans->n_set;

  /* start and atop offset: */
  cnt = 5 +  /* the 5 previous numbers */
        2 +  /* offset pointer to start and stop */
        gv->trans->n_sym; /* -> 10 */
  gv->trans->start = cnt;
  /* entry & exit symbols: */

  cnt += NBR_FIELDS + 1; /* start.n_next */ /* -> 13 */
  gv->trans->stop  = cnt;    /* stop offset  */
  cnt += NBR_FIELDS + 1; /* stop.n_next  */  /* -> 16 */

  for (p_cnt = &(gv->trans->symlist[0]), s = 0; s < gv->trans->n_sym; s++, p_cnt++) {
    *p_cnt = cnt;  /* n_sym offsets */
    cnt += NBR_FIELDS + 1; /* trans.symlist[ i ].n_next; */
  }
  
  /* entry & exit symbols: */
  p_sym0 = p_sym = (SymType *) p_cnt;
  /* output_sym( &trans.start, fp); */
  p_sym->hmm_code = 0; /* non-emitting */
  p_sym->n_next = 1;
  p_sym->next[0] = 0;
  p_sym++;
  /* output_sym( &trans.stop, fp); */
  p_sym->hmm_code = 0; /* non-emitting */
  p_sym->n_next = 1;
  p_sym->next[0] = gv->trans->n_sym - 1;
  p_sym++;

  /* HMM code for entry and exit silences: */
  for (s=1; s<gv->trans->n_sym - 1; s++) /* for symbols excl. sil */ 
     p_sym[s].hmm_code = s - 1;
  /* same hmm as the first one */
  p_sym[0].hmm_code = p_sym[gv->trans->n_sym - 1].hmm_code = gv->trans->n_hmm - 1;

  /* other settings */
  for (s=0; s<gv->trans->n_sym; s++) { 
    p_sym[s].n_next = 1; /* always one next symbol */
    p_sym[s].next[0] = s + 1;
  }
  p_sym[gv->trans->n_sym - 1].n_next = 0;
#ifdef OUTPUT_NET
  //  output_network(base_net); /* write network in vcg format */
  prt_spot_network(gv->base_net);
#endif
  return eTIesrEngineSuccess;
}

/*--------------------------------*/
/*
** generate initial and transition probabilities for an HMM, log applied
** N is nbr of states (including exit) 
*/
void enr_hmm_trans(HmmType *hmm, ushort N, short slf, short nxt, short skp, short *base_tran)
{ 
  short *pi, i, j;
  /* set initial probability */

  pi = GET_PI(hmm,  base_tran); /* PI */
  for (i=1; i<N-1; i++) pi[i] = BAD_SCR;
  pi[0] = 0;

  for (i=0; i<N-1; i++) {
    for (j=0; j<N; j++) GET_AIJ(hmm,i,j,base_tran) = BAD_SCR;
    GET_AIJ(hmm,i,i,base_tran) = slf;  /* diagonal */
  }
  for (i=0; i<N-1; i++) GET_AIJ(hmm,i,i+1,base_tran) = nxt;
  for (i=0; i<N-2; i++) GET_AIJ(hmm,i,i+2,base_tran) = skp;
  i = N-2;
  
  if (i>=0) { 
    GET_AIJ(hmm,i,i,base_tran)   = TR_SELF_LST; /* make the last row summing to 1 */
    GET_AIJ(hmm,i,i+1,base_tran) = TR_NEXT_LST;
  }
  /*   ptr_tr(hmm, pi, N); */
}



/*--------------------------------*/
/*
** build speaker-dependent (e.g. name dialing) models from the only information:
** 1. Number of HMMs (the last one is silence) in the name N
** 2. Number of states in each hmm  Mi 
** 3. mean vectors of each state
** 4. variance (single) of the name
**
** For recognition/enrollement. Use one block of memory for all structures.
*/

/*
** updated for transition matrix sharing (transition matrix of the smae state number) 
** updated for multiple-gaussian mixture HMM.
** update: removed silence-specific transition prob matrix (sil shares with other)
*/


/*
** construct GMT models for name dialing with names embedded in unwanted speech
** 1. With number of hmm = N, N-1 is silence, N-2 is garbage speech
** 2. garbage speech (N-2) is an hmm with nbr_mix_g gaussians, other hmms with single gaussian
** 3. nbr_mix == 0 => no background model will be created (everything is single Gaussian mix)
*/
TIesrEngineStatusType new_spot_reco_gtm(ushort nbr_hmm, ushort state_nbr[], ushort *mem_base, ushort *mem_count, ushort max_mem, short nbr_mix, gmhmm_type *gv)
{
  ushort i, cum_sz, nbr_emit_states = 0, trans_size = 0, ttl_states = 0, emit_nbr;
  ushort p = *mem_count, pdf_count, mix_count, mean_count;
  HmmType *hmm;
  ushort state;
  short k, *p_mix, *p_compn, delta_size;
  short used_state_nbr[MAX_NBR_STATES];  /* emit state */
  short log_nbr_mix, the_nbr_mix;

  short nbr_dim = 
#ifdef BIT8MEAN 
  SD_N_MFCC; 
#else
  SDDIM;  
#endif
  
  delta_size = nbr_mix;  /* silence will have more mixture for background */

  /* count number of states, including silence (the last one) */

  for (i = 0; i < MAX_NBR_STATES; i++) used_state_nbr[ i ] = 0;
  for (i = 0; i < nbr_hmm; i++) { 
    emit_nbr = state_nbr[i] - 1;
    nbr_emit_states += emit_nbr;
    ttl_states += (emit_nbr + 1);
    used_state_nbr[ emit_nbr ] -= 1; /* set to -1 when present */
  }

  for (i = 0; i < MAX_NBR_STATES; i++) {
    if (used_state_nbr[ i ] < 0) { /* has been used */
      trans_size += (i + 1) * (i + 1);
    }
  }

  /* calc. total memory size, and allocate memory: */

  /* mean vectors: */
  gv->base_mu  = (short *) mem_base + p;    

  p += nbr_dim * nbr_emit_states; /* one state <-> one gaussian mean, except for the background */
  if (nbr_mix != 0) p += nbr_dim * delta_size;   /* correction for mixture models */  
  
  gv->base_var = (short *) mem_base + p;    /* Two variance vectors */
#ifdef BIT8VAR
  p+= SD_N_MFCC * NBR_VAR; 
#else
  p+= SDDIM * NBR_VAR;
#endif
  gv-> base_gconst = (short *) mem_base + p; p+= NBR_VAR;     /* Two gconst                      */

  gv->base_tran = (short *) mem_base + p;   p+= trans_size;  /* transition prob size            */

  gv->base_mixture = (short *) mem_base+p;  p += SIZEOFAMIX * nbr_emit_states;   /* mixtures */
  if (nbr_mix != 0) p += delta_size * COMPONENT_SIZE;   /*  more components */
  
  gv->base_pdf =(unsigned short *)  mem_base + p;    p+= nbr_emit_states;      /* state pdf              */
  gv->base_hmms= (unsigned short *) mem_base + p;    p+=  nbr_hmm + ttl_states; /* HMM index and states   */
  
  if (!cap_in_check("gtm memory size", p, max_mem)) return eTIesrEngineModelMemorySize;
  
  gv->obs_scr = (short *) mem_base + p;     p+= nbr_emit_states;      /* observation score, one per pdf */
  if (!cap_in_check("observation memory", p,max_mem)) return eTIesrEngineFeatureMemorySize;
  
  cum_sz = nbr_hmm; /* first block for state  information */
  trans_size = 0;
  pdf_count = 0;
  mix_count = 0;
  mean_count = 0;
  for (i=0; i<nbr_hmm; i++) {
    gv->base_hmms[i] = cum_sz;
    cum_sz += state_nbr[i]; /* includes obs index array and transition matrix index */
    hmm = GET_HMM( gv->base_hmms, i, 0);
    /* transition probabilities */
    if (used_state_nbr[state_nbr[i]-1] < 0) {
	used_state_nbr[state_nbr[i]-1] = trans_size;
	trans_size += state_nbr[i] * state_nbr[i];
	hmm->tran = used_state_nbr[state_nbr[i]-1];
	NBR_STATES(hmm, gv->base_tran) = state_nbr[i];
	enr_hmm_trans(hmm, state_nbr[i], TR_SELF, TR_NEXT, TR_SKIP, gv->base_tran);  
    }
    hmm->tran = used_state_nbr[state_nbr[i]-1];
    /* mixing probabilities */
    FOR_EMS_STATES(state,hmm,gv->base_tran) {
      GET_BJ_IDX(hmm,state) = pdf_count;
      gv->base_pdf[pdf_count] = mix_count;
      p_mix = GET_MIX(gv,pdf_count);
      the_nbr_mix = 1;
      if ((nbr_mix != 0) && (i == nbr_hmm - 1)) the_nbr_mix += nbr_mix;  /* silence */
      log_nbr_mix = 0;
      k = the_nbr_mix; /* assuming pow of 2 */
      while (k != 1) {  k >>= 1;  log_nbr_mix++; }

      MIX_SIZE(p_mix) = the_nbr_mix; /* multiple gaussian mixture model */
      FOR_EACH_MIXING(k,p_compn,p_mix) {
	//	if ((i == nbr_hmm - 1) && (k == the_nbr_mix - 1)) 
	if ((i == nbr_hmm - 1) && (the_nbr_mix != 1))
	  MIXTURE_WEIGHT(p_compn) = -1 * LOG2Q6 ; /* 1/2 */
	else MIXTURE_WEIGHT(p_compn) = - log_nbr_mix * LOG2Q6;
	INDEX_FOR_MEAN(p_compn) = mean_count;
	INDEX_FOR_VARC(p_compn) = 0; /* single variance */
	mean_count += 1;
	mix_count  +=  COMPONENT_SIZE;
      }
      mix_count  += 1; 
      pdf_count  += 1;
    }
  }
  gv->n_pdf = pdf_count;
  gv->n_mu  = mean_count;
  *mem_count = p; /* pass the value to the caller */
  return eTIesrEngineSuccess;
}
