/*======================================================================

 *
 * sdauxl_user.h
 *
 * This header defines the functions exposed by the sdauxl api,
 * which is a collection of functions used by TIesr SD enrollment
 * and recognition.
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *

 ==================================================================*/

#ifndef _SDAUXL_USER_H
#define _SDAUXL_USER_H

#include "status.h"
#include "tiesrcommonmacros.h"
#include "search_user.h"
#include "gmhmm_type.h"

short cap_in_check(char *mesg, ushort value, ushort max_mem);

void enr_hmm_trans(HmmType *hmm, ushort N, short slf, short nxt, 
		   short skp, short *base_tran);

TIesrEngineStatusType make_net(gmhmm_type *gv, ushort n,
			       ushort *mem_count, short *mem_base, 
			       ushort max_mem);

TIesrEngineStatusType new_spot_reco_gtm(ushort nbr_hmm, ushort state_nbr[], 
					ushort *mem_base, ushort *mem_count, 
					ushort max_mem, short nbr_mix, 
					gmhmm_type *gv);

#endif
