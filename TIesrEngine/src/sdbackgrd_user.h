/*----------------------------------------------------------------
  sdbackgrd_user.h

  This header exposes the single function TIesrsdbgd, which handles
  background calculations for TIesr SD recognition.
  4/27/05 LN
  ----------------------------------------------------------------*/
#ifndef _SDBACKGRD_USER_H
#define _SDBACKGRD_USER_H

#include "tiesr_config.h"
#include "tiesrcommonmacros.h"
#include "gmhmm_type.h"
#include "sd_reco_type.h"

ushort TIesrsdbgd(ushort nbr_words, char *directory, char *name_list[], gmhmm_type *gv, sd_reco_type *sdv);

#endif /* _SDBACKGRD_USER_H */
