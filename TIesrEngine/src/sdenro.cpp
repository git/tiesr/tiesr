/*=================================================================

 *
 * sdenro.cpp
 *
 * This source is the main processing source for TIesr SD enrollment.
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *

 =====================================================================*/

/*
** Name dialing enrollment. 
*/

#include <stdio.h>
#include <stdlib.h>

#include "tiesr_config.h"
#include "tiesrcommonmacros.h"
#include "status.h"

#include "gmhmm_type.h"
#include "sd_enro_type.h"
#include "search_user.h"
#include "pack_user.h"
#include "sdhead.h"
#include "filemode_user.h"
#include "sdparams.h"
#include "sdenro.h"
#include "alinaux_user.h"
#include "sdauxl_user.h"
#include "filemode_user.h"
#include "sdvar_header.h"
#include "sdvqpack_user.h"

#include "dist_user.h"

/*--------------------------------*/
/* Local defines */
#define LONG_BAD_SCR ((long)-2147483647)  /* 2^31-1 */

#define IS_SILENCE '-'
#define IS_OTHER   ' '

#define MAX_ITR 2
 /* limit segment size: this may create more "div by zero" (above) */
#define MAX_SEG_SIZE 12


/* linear time warping of d to N: */
#define get_frame_index(j,d,N) ((N != 0)?((d-1)*j/N): 0)

#define M2(x) ((x) & 1) /* modulo 2 */


/*--------------------------------*/

/* memory pool for the enrollment: phonetic back tracking */
#ifndef BIT8FEAT
// short mem_d_mfcc[UTTER_SIZE ];   /* storing delta mfcc */
#endif


/*--------------------------------*/
static void seg_memory_assign(ushort *mem, gmhmm_type *gv, ushort asize)
{
  ushort p = 0;
  gv->hmm_code = mem + p;  p += asize; 
  gv->stt      = mem + p;  p += asize; 
  gv->stp      = mem + p;  p += asize; 
}


/*--------------------------------*/
/*
** save original models
*/
static void save_old_model(gmhmm_type *gv, sd_enro_type *enro)
{
  enro->z_base_mu = gv->base_mu;
  enro->z_base_gconst = gv->base_gconst;
  enro->z_base_var = gv->base_var;
  enro->z_base_hmms = gv->base_hmms;
  enro->z_base_tran = gv->base_tran;
  enro->z_n_mu = gv->n_mu;
  enro->z_base_pdf = gv->base_pdf;
  enro->z_base_mixture = gv->base_mixture;
  enro->z_n_pdf = gv->n_pdf;
  enro->z_obs_scr = gv->obs_scr;
  enro->z_base_net = gv->base_net;
  enro->z_base_mu_orig =  gv->base_mu_orig;
}


/*--------------------------------*/
void free_save_models(sd_enro_type *enro)
{
  if (enro->z_base_mu) { 
      free( enro->z_base_net );
      free( enro->z_base_hmms );
      free( enro->z_base_mu );
      free( enro->z_base_mu_orig );
      free( enro->z_base_var );
      free( enro->z_base_tran );

      /* Now use Gaussian cache rather than pdf cache in TIesrEngineCore */
      /* free( enro->z_obs_scr ); */

      free( enro->z_base_pdf );
      free( enro->z_base_mixture );
      free( enro->z_base_gconst );
  }
      /*
#ifdef COMPACT
      free( z_scale_mu );
      free( z_scale_var);
#endif
      */
}


/*--------------------------------*/
/*
** For original models, update silence's mean vectors
** Assuming SIL and BWS are not shared, and SIL is not shared by other PDFs
*/
static void update_silence(ushort sil_code, short mean[], short n_mfcc, gmhmm_type *gv)
{
  ushort k, j, hmm_dlt; /* hmm code */
  short *p_mix, i, *mu, *pdf;
  HmmType *hmm;
  for ( hmm_dlt = 0; hmm_dlt < gv->trans->n_hmm; hmm_dlt += gv->trans->n_hmm_set) {
    hmm = GET_HMM(gv->base_hmms, sil_code, hmm_dlt);
    FOR_EMS_STATES(j,hmm, gv->base_tran) {  /* loop for each HMM emission state */
      pdf = GET_BJ(gv,hmm,j);
      FOR_EACH_MIXING(k,p_mix,pdf) {
#ifdef BIT8MEAN
	mu = GET_MU(gv->base_mu, p_mix, n_mfcc);
	for (i=0; i<gv->n_mfcc; i++) mu[i] = nbr_coding(mean[i], gv->scale_feat[i], 0) ;
		   // + (mu[i] & 0xff); dynamic, doesn't help 
#else
	mu = GET_MU(gv->base_mu, p_mix, (n_mfcc * 2));
      	for (i=0; i<n_mfcc; i++) mu[i] = mean[i];
#endif
	break;
      }
    }
  }
}

/*--------------------------------*/
static void mean_ini(  long cum[], ushort dim )
{
  ushort i;
  for (i = 0; i < dim; i++) cum[i] = 0;
}


/*--------------------------------*/
void mean_cum(short mfcc[], ushort nbr, long cum[], ushort dim,  gmhmm_type *gv) 
{
  ushort i,j;
  
#ifdef BIT8FEAT
  short *pv = mfcc, tmp[MAX_DIM];
                                 
    for (i = 0; i < nbr; i++) {  
      vector_unpacking((ushort*)pv, tmp, gv->scale_feat, SD_N_MFCC);
      for (j = 0; j < dim; j++) { 
	cum[ j ] += tmp[ j ];   
      }
      pv += dim;
    }           
#else
  for (i = 0; i < nbr; i++) {
    for (j = 0; j < dim; j++) {
      cum[ j ] += mfcc[ i * dim +  j ];
    }
  }
#endif
}
  

/*--------------------------------*/
static void mean_div(short mean[], long cum[], ushort nbr, ushort dim) 
{
  ushort i;
  /* compute mean */
  for (i = 0; i < dim; i++) {
    mean[ i ] = (short) (cum[i] / nbr);
  }
}


/*--------------------------------*/
static void gauss_prm_cum(short mfcc[], ushort nbr, short mean[], long cum[], gmhmm_type *gv) 
{
  mean_ini( cum, SD_N_MFCC );
  mean_cum(mfcc, nbr, cum, SD_N_MFCC, gv);
  mean_div(mean, cum, nbr, SD_N_MFCC);
}


/*--------------------------------*/
/*
** works for offline tests. Has no effect if audio device is used.
** to be shared with SI API code
*/
static void __GetSearchInfo(void *gvv, ushort frm_cnt)
{
  PRT_ERR(gmhmm_type *gv = (gmhmm_type *)gvv);
  PRT_ERR(printf("DR: peak count, beam = %d, sym = %d, state = %d, time = %d\n", 
	 gv->evalstat.beam.high_pos,
	 NSYM * MAX(gv->evalstat.sym[0].high_pos,gv->evalstat.sym[1].high_pos),
	 gv->evalstat.state.high_pos, gv->evalstat.time.high_pos));  
  PRT_ERR(printf("Search size = %d words, score = %f (%d frames)\n", SEARCH_SPACE(gv->evalstat),
	 (float) gv->best_sym_scr /  frm_cnt / 64,  frm_cnt));
}


/*--------------------------------*/
static TIesrEngineStatusType sd_enro_process_a_file(void *gvv, ushort total_frm, ushort start_frame, ushort start_seg)
{
  TIesrEngineStatusType status;
  gmhmm_type *gv = (gmhmm_type *) gvv;
  short bt;

  /* Use HMM-based search, not word-based search. Keep track of and restore
  search type on exit. */
  bt = gv->word_backtrace;
  gv->word_backtrace = HMMBT;

  status = process_a_file_1(gv, total_frm, start_frame);
  /* empty frame */
  //  frm_cnt = total_frm - 1;

  if ( status == eTIesrEngineSuccess ) search_a_frame(NULL, 0, total_frm - 1, gv ); 

  if ((total_frm - 1) != 0 &&  status == eTIesrEngineSuccess && gv->best_sym != USHRT_MAX ) {
    status = back_trace_beam(gv->best_sym, total_frm - 1,  
			     gv->hmm_code + start_seg,  gv->stt + start_seg,  gv->stp + start_seg, 
			       &(gv->nbr_seg), &(gv->hmm_dlt), gv);
    __GetSearchInfo(gv, total_frm );
  }
  else {
    if (status == eTIesrEngineSuccess) status = eTIesrEngineAlignmentFail;
    PRT_ERR(fprintf(stderr,"\nalignment not found, error code = %d\n", status)); 
  }
  PRT_ERR(printf("R:\n"););

  /* Restore search and backtrace type */
  gv->word_backtrace = bt;

  return status;
}


/*--------------------------------*/
/*
** compute mean vector from mfcc array
*/
static void get_mean_vector(short mfcc[], short mean[], ushort stt, ushort stp,long cum[], gmhmm_type *gv)
{
  gauss_prm_cum(mfcc + stt * SD_N_MFCC, stp - stt, mean, cum, gv);  /* stp not included */
}


#ifdef DEBUG_WITH_PHONE_STR
/*--------------------------------*/
/*
** only for debug, not useful DSP
*/
char phone_list[ MAX_WORD ][ MAX_STR ];

void prt_lab(char str[], ushort nbr)
{
  ushort i = nbr_seg;
  for (i=0; i<nbr_seg; i++) {
    printf("%s %2d %3d %3d %s\n",str, i, stt[i], stp[i],  phone_list[hmm_code[i]]);
  }
}
#endif


#ifdef BIT8MEAN
/*--------------------------------*/
/*
** add model mean
*/
static void add_model_mean(short mean_vec[], gmhmm_type *gv)
{
  ushort i, j;
  short *p_mu;
  short tmp[ SDDIM ];
  
  FOR_EACH_MEAN(i, p_mu, SD_N_MFCC, gv->base_mu, gv->n_mu) {
    vector_unpacking((ushort *)p_mu, tmp, gv->scale_mu,  SD_N_MFCC);
    for (j = 0; j < SD_N_MFCC; j++) tmp[ j ] += mean_vec[ j ];
    vector_packing(tmp, (ushort *) p_mu, gv->scale_feat, SD_N_MFCC);
  }
}
#endif


/*--------------------------------*/
/*
** mean adjustment of original model, return status of alignment
*/
static short adjust_mean(short mfcc[],  ushort nbr_frames, ushort silCode, ushort bwsCode,
		  gmhmm_type *gv, long cum[])
{
#ifndef BIT8MEAN
  short i, *p_mu, n_dim = SD_N_MFCC*2;
#endif
  short j, status;
  short glb_mean[SD_N_MFCC];
  short seg_mean[SD_N_MFCC];
  short sil_mean[SD_N_MFCC];
  
  /* get file mean over utr: */
  get_mean_vector(mfcc,glb_mean, 0, nbr_frames, cum, gv); /* whole utterance mean */
  /* add mean */
#ifdef BIT8MEAN 
  add_model_mean(glb_mean, gv);
  /* mean vectors has been updated using new scale */
  for (j = 0; j < SDDIM; j++) gv->scale_mu[ j ] = gv->scale_feat /* new_scale_mu */ [ j ];
#else
//   FOR_EACH_MEAN(i, p_mu, n_dim) {
   FOR_EACH_MEAN(i, p_mu, n_dim, gv->base_mu, gv->n_mu) {
    for (j = 0; j < SD_N_MFCC; j++) p_mu[ j ] += glb_mean[ j ];
  }
#endif

  /* utterance specific updating of silence: */
  get_mean_vector(mfcc, sil_mean, 0, PMCFRM, cum,gv );
  update_silence(silCode, sil_mean, SD_N_MFCC, gv);
  status = sd_enro_process_a_file(gv,nbr_frames,0,0); /* first transcription, to find h# ... h#: */
  if (status != eTIesrEngineSuccess) return status;
  
#ifdef AS_NEEDED
  /* get silence-only mean: */
  mean_ini( cum, N_MFCC );
  mean_cum(mfcc + stt[nbr_seg - 1] * N_MFCC, stp[nbr_seg-1]- stt[nbr_seg-1], cum, N_MFCC, gv); /* first silence */
  mean_cum(mfcc + stt[0] * N_MFCC, stp[0] - stt[0], cum, N_MFCC, gv); /* last silence */
  j = stp[nbr_seg-1] - stt[nbr_seg-1] + stp[0] - stt[0];
  mean_div(seg_mean, cum, j, N_MFCC); /* silence mean */

  update_silence(bwsCode, seg_mean); 
#endif

  /* printf("stt = %d stop = %d\n",  stp[nbr_seg-1], stt[0]);  speech portion */


  /* Since the aligned models are listed in reverse order from the backtrace,
     gv->stp[gv->nbr_seg-1] is the ending frame of the first model in time (the beginning silence),
     and gv->stt[0] is the starting frame of the last model in time (the ending silence). */ 
  get_mean_vector(mfcc, seg_mean,  gv->stp[gv->nbr_seg-1], gv->stt[0], cum, gv); /* speech mean */

  for (j = 0; j < SD_N_MFCC; j++) seg_mean[j] -= glb_mean[j];  /* rm glb_mean mean before adding */
  /* add mean */
#ifdef BIT8MEAN 
  add_model_mean(seg_mean, gv);
#else
//  FOR_EACH_MEAN(i, p_mu, n_dim) {
  FOR_EACH_MEAN(i, p_mu, n_dim, gv->base_mu, gv->n_mu) {
    for (j = 0; j < SD_N_MFCC; j++) p_mu[ j ] += seg_mean[ j ];
  }
#endif
  update_silence(silCode, sil_mean, SD_N_MFCC, gv);
  status = sd_enro_process_a_file(gv,nbr_frames,0,0); /* transcribe using mean-adjusted hmms */
  return status;
}


#ifdef DEBUG_WITH_PHONE_STR
/*--------------------------------*/
void load_phone_list(void)
{
  FILE  *fp;
  short i;

  fp = fopen("/home/ygong/evaluation/rss/grm/D4710.single.lst", "r");

  for (i = 0; i < 39; i++) {
    fgets(phone_list[ i ], MAX_STR, fp);
    phone_list[ i ][ strlen( phone_list[ i ] ) - 1 ] = '\0';
  }

  fclose( fp );
}
#endif


/*--------------------------------*/
/*
** initial model from mac files 
** silence must be the last model (consistent with make_net)
*/
static void model_size(ushort silCode, ushort bws, ushort nbr_hmm, ushort state[], gmhmm_type *gv)
{
 ushort sil_n, len, N, h; /* the zero-th is silence */
 HmmType *hmm;
 
 for (h = 1; h <= nbr_hmm; h++) { /* skip the first segment, which is a silence */
    len = gv->stp[h] - gv->stt[h];
    if (gv->hmm_code[h] == silCode) { /* must use system-wide silence */
      hmm = GET_HMM(gv->base_hmms, silCode, gv->hmm_dlt);
      sil_n = NBR_STATES(hmm,gv->base_tran) - 1;
      len = sil_n; /* one emission state */
    }
    else if (gv->hmm_code[h] == bws) { 
      len = MIN(len,4);  /* tried 1, looks fine, between-word silence has special treatment */
    }

      //      len = len >>1; 
      //      len = MAX(len,1);
      /* else any other labels */
    len = MIN(len, MAX_NBR_EM_STATES); 
    
    if (len > 4) len = len & 0xfffe; /* use only even nbr of states */
    N = len + 1;
    state[h-1] = N;
  }
}


/*--------------------------------*/
/*
** copy silence mean to a one gaussian/mixture silence model
*/
static void enr_sil_init(ushort n_sil, ushort o_sil, short n_dim, gmhmm_type *gv, sd_enro_type *enro)
{
  ushort state,k, d, size_vec;
#ifdef BIT8VAR  
  ushort n_mfcc = n_dim>>1;
#endif
  short *o_bj, *o_pmx;
  short *pv = gv->base_var +  /* precision values */
#ifdef BIT8VAR  
    n_mfcc; 
#else
    n_dim;
#endif
  short *n_bj, *n_pmx, *po, *pn;
  HmmType *o_hmm = GET_HMM(enro->z_base_hmms, o_sil, gv->hmm_dlt);
  HmmType *n_hmm = GET_HMM(gv->base_hmms, n_sil, 0);

#ifndef BIT8VAR
  for (d = 0; d < n_dim; d++) pv[d] = 0x7fff;
#endif

/*  for_ems_states(state,o_hmm,enro->z_base_tran) {
 */
  FOR_EMS_STATES(state,o_hmm,enro->z_base_tran) {
    n_bj = GET_BJ(gv,n_hmm,state);
    o_bj = enro->z_base_mixture + enro->z_base_pdf[ GET_BJ_IDX(o_hmm,state) ];
    o_pmx = GET_MIXING_COMPONENT(o_bj);
    FOR_EACH_MIXING(k, n_pmx, n_bj) {
      INDEX_FOR_VARC(n_pmx) = 1; 
      /* copy mean vector: */
#ifdef BIT8MEAN
      size_vec = n_dim>>1;
#else
      size_vec = n_dim;
#endif
      pn = GET_MU(gv->base_mu, n_pmx, size_vec);
      po = enro->z_base_mu + o_pmx[1] * size_vec;
      for (d = 0; d < size_vec; d++) {
	pn[d] = po[d];
	/* find largest variance == smallerst precision */
#ifndef BIT8VAR
	pv[d] = MIN(pv[d], (enro->z_base_var + o_pmx[2] * n_dim)[d]);
#endif
      }
#ifdef BIT8VAR
      for (d = 0; d < n_mfcc; d++)  pv[d] = Z_var_1[d];
      //      for (d = 0; d < n_mfcc; d++)  pv[d] = (z_base_var + o_pmx[2] * n_mfcc)[d];
#endif
      break; /* only deal w/ the first mixing */
    }
  }
}


/*--------------------------------*/
/*
** build one phoneme hmm by initialising mean vectors with
** segment frames, variance shared among all trained HMMs
** updated for label list reversing
*/
static void enr_one_model(ushort sil, short mfcc_idx,
#ifndef BIT8FEAT
		   short mfcc[], short dmfcc[],
#endif
		   HmmType *hmm, ushort h, long cum1[], long cum2[], ushort T, char code,
		   short n_mfcc, gmhmm_type *gv, sd_enro_type *enro)
{
  ushort j,  mix, d, idx, n, m;
  short *pdf_j, *pm, p, *pmu, n_dim = n_mfcc * 2;
#ifndef BIT8VAR
  long prod;
#endif

// Needed even if not BIT8MEAN in order to convert features from byte to short.
//#ifdef BIT8MEAN
  short tmp[ SDDIM ];
//#endif  

  if (code != IS_SILENCE) { /* silence is the last -- required by make_net */
    n = NBR_STATES(hmm, gv->base_tran) - 2; /* number of emit state -1 */
    FOR_EMS_STATES(j,hmm, gv->base_tran) {
      pdf_j = GET_BJ(gv,hmm,j);
      idx = mfcc_idx + get_frame_index(j,T,n) * n_mfcc;
      FOR_EACH_MIXING(mix,pm,pdf_j) {
#ifdef BIT8MEAN
	pmu = GET_MU(gv->base_mu, pm, n_mfcc);
	for (d = 0; d < gv->n_mfcc; d++) pmu[d] = (gv->mem_feature + idx)[d];
	//	vector_unpacking(mem_feature + idx, tmp, scale, n_mfcc); 

	//	  idx = get_frame_index(j,T,n) * n_mfcc + d;
	//	  tmp[d       ] =  mfcc[idx];
	//	  tmp[d+n_mfcc] = dmfcc[idx];
	//	}
	//	vector_packing(tmp, (ushort *) pmu, new_scale_mu, SD_N_MFCC);
#else

	/* pointer to location to load mean vector for the new model */
	pmu = GET_MU(gv->base_mu, pm, n_dim);

	/* Unpack the frame byte feature vector, and load it into the new model mean */
	vector_unpacking(gv->mem_feature + idx, tmp, gv->scale_feat, n_mfcc); 
	for (d = 0; d < n_mfcc; d++) {
	  pmu[d] =  tmp[d];
	  pmu[d+n_mfcc] = tmp[d+n_mfcc];
	}	

	//for (d = 0; d < n_mfcc; d++) {
	//idx = get_frame_index(j,T,n) * n_mfcc + d;
	//pmu[d       ] =  mfcc[idx];
	//pmu[d+n_mfcc] = dmfcc[idx];
	//}

#endif
      }
    }
    for ( m = 0, j = 0; j < T; j++, m += n_mfcc) { /* accumulate silence mean and variance */
      vector_unpacking(gv->mem_feature + mfcc_idx + m,tmp, gv->scale_feat, n_mfcc);
      for (d = 0; d < n_mfcc; d++) {
	/* static */
	p = tmp[d];
	cum1[ d ] += p; /* Q11 */
#ifndef BIT8VAR
	prod = p * p;   /* Q22 */
	cum2[ d ] += prod>>11; /* Q 11 */
#endif
	/* dynamic */
	p = (tmp+n_mfcc)[d];
	cum1[n_mfcc + d ] += p; /* Q11 */
#ifndef BIT8VAR
	prod = p * p;   /* Q22 */
	cum2[n_mfcc + d ] += prod>>11; /* Q 11 */ 
#endif
      }
    }
  }
  else enr_sil_init(h, sil, n_dim, gv, enro);  /* copy the old silence */
}


/* x^-0.5 polynomial for x between .5 and 1.0 Q11 */
static const short COEFINVSQRT[6] = { 0xf4b3, 0x3386, 0x9e36, 0x63f2, 0xc3e1, 0x19bf };

   /* 2^-0.5, Q15 */
static const short SQRTMP5 = 0x5a82;

/*----------------------------------------------------------------
  InvSquareRoot

  Find x^(-1/2) of a fixed-point number.  The user provides
  the number and a pointer to the Q point.  The routine returns the
  result, and in qpt the Q point of the result.

  ----------------------------------------------------------------*/
static short InvSquareRoot( short aValue, short *aQPoint )
{
   short ex, exd2;
   short msqrt;
   long temp;
   short qtmp;
   short s_temp;
   short coef;

   if( aValue <= (short)0 )
   {
      *aQPoint = 0;
      return (short)0;
   }

   /* initialize exponent part of aValue */
   ex = 15 - *aQPoint;

   /* find fractional and exponent part of aValue */
   while( aValue < (short)0x4000 )
   {
      aValue <<= 1;
      ex--;
   }


   /* Q11 inv square root expansion of aValue in series */
   msqrt = COEFINVSQRT[0];

   for( coef = 1; coef <= 5; coef++ )
   {
      /* Q11*Q15 */
      temp = msqrt*aValue;
      temp += ((long)COEFINVSQRT[coef] << 15 );
      temp += 1<<14;
      msqrt = (short)( temp >> 15);
   }

   /* 2^(-ex/2) */
   ex = -ex;
   exd2 = ( ex < 0 ) ?  -( -(ex) >> 1 ) : ( ex >> 1 );
   qtmp = 15 -  exd2  - 1;

   if( ex & 0x1 )
   {
      s_temp = SQRTMP5;

      if( ex < 0 )
         qtmp++;
   }
   else
   {
      s_temp = 0x4000;
   }

   /* Q(qtmp)*Q11 */
   temp = s_temp*msqrt;
   qtmp += 11;
   while( temp >= 0x8000 || qtmp > 15 )
   {
      temp >>= 1;
      qtmp--;
   }
   *aQPoint = qtmp;
   return( (short)temp );
}










/*--------------------------------*/
/*
** - estimate an unique variance for all hmms  
** - return nbr of non-silence symbols
*/
/* Not needed if BIT8VAR */
/* Now replaced by a function in TIesrMath - InvSquareRoot
//#ifndef BIT8VAR
//extern short fix_msqrt( short x, short *qpt ); /* SQRT function */
//#endif

/* This does not appear to be used now
extern void prt_vec(char *str, short *v, unsigned short n);
*/

/*--------------------------------*/
static void calc_variance(long cum1[], long cum2[], ushort T, short n_dim, gmhmm_type *gv)
{
  ushort i;
  short *pv = gv->base_var; /* precision */
#ifdef BIT8VAR
 
#else  
  short qp;
  long m2, meanx, meanx2;

  for (i=0; i<n_dim; i++) {
    meanx  = cum1[i] / T; /* this is the mean vector */
    meanx2 = cum2[i] / T; /* mean of x2 */
    m2 = meanx * meanx; /* Q 22 */
    m2 >>= 11;          /* Q 11 */
    pv[i] = meanx2 - m2;    /* Q 11 */
    /* unscaled: */
    /* printf("%7.3f", (float)(pv[i]) * mu_scale(i)* mu_scale(i) / (1<<11)); */
    qp = 11;
    pv[i] = InvSquareRoot(pv[i], &qp);    /* huristics for the variance */
    if (qp > 9) pv[i] >>= (qp - 9); /* the final res in Q9 */
    else  pv[i] <<= (9 - qp);
  }
#endif

  /* calc gaussian constant */
#ifdef BIT8VAR
  gv->base_gconst[0] = GCONSTSP; /* speech  */
  gv->base_gconst[1] = GCONSTSL; /* silence */
  free(gv->scale_var);  /* will not use this any more */
  gv->scale_var = (short *)Z_scale_var;

  for (i=0; i < SD_N_MFCC; i++) pv[i] = Z_unique_pr[i];
#else
  /* Silence */
  gv->base_gconst[1] = gauss_det_const(pv + n_dim, 2, gv->muScaleP2, n_dim);

  /* scale speech inverse variance to take into account sqrt */

  /* You always have to scale inverse variance 
     by 2*muScaleP2.  If you scale variance by muScaleP2, then the
     squared mean terms and the variance terms have different scaling
     factors, and the likelihood score for each dimension
     will be weighted by the inverse of the scaling factor. */
  for (i=0; i<n_dim; i++)
  { 
    long pvx =  pv[i] <<  2*gv->muScaleP2[i];
    if (pvx > 32767) {
       // printf("pv overflow = %ld\n", pvx);
       // exit(0);

       // For live-mode, can't print and exit, so limit variance.
       pvx = 32767;
    }

    pv[i] = (short)pvx;
  }

  /* Speech variance, using a heuristic of sqrt of variance as the variance */
  gv->base_gconst[0] = gauss_det_const( pv, 2, gv->muScaleP2, n_dim );

#endif
}


/*--------------------------------*/
/*
** construct and initialize hmm models with frames in a phone
** segment, with global (to the utterance) variance 
** return the nbr of syms (excl. NDSIL)
** MUST use a single silence for all command models.
** duplicate sil of the old gtm.
*/
static void mac_to_hmm(ushort state_nbr[], ushort sil,  ushort nbr_hmm, 
		ushort tflen, short skip_, short next_, gmhmm_type *gv, sd_enro_type *enro)
{
  short N, h, h1; /* the zero-th is silence */
  char cc;
  HmmType *hmm;
  long cum1[SDDIM];
  ushort T, total_T = 0;  
#ifndef BIT8VAR
  long cum2[SDDIM];
  mean_ini(cum2, SDDIM); /* cum for sigma2 */
#else
  long *cum2 = 0; /* will not be accessed */
#endif

  mean_ini(cum1, SDDIM); /* cum for sigma */
  for (h = 0, h1 = 1; h < nbr_hmm; h++, h1++) {
    N = state_nbr[h];
    T = gv->stp[h1] - gv->stt[h1];
    //    printf("N = %d T = %d\n", N, T);
    
    if (h == nbr_hmm - 1) cc = IS_SILENCE;
    else {   total_T += T;  cc= IS_OTHER; }
#ifdef DEBUG_WITH_PHONE_STR
    printf("%c code = %.2d length = %2d [%3d %3d) %s\n", cc, h, N, stt[h1], stp[h1], phone_list[hmm_code[h1]]);
#else
    ENRO_PRT(printf("%c code = %.2d length = %2d [%3d %3d) %3d\n", cc, h, N, gv->stt[h1], gv->stp[h1], gv->hmm_code[h1]);)
#endif
    hmm = GET_HMM(gv->base_hmms, h, 0);
    /* initialize mean, transition, and silence variance: */
    enr_one_model(sil,  gv->stt[h1] * SD_N_MFCC, 
   		      hmm, h, cum1, cum2, T, cc, SD_N_MFCC, gv, enro);
  }
  /* now calculate the variance for the reste of hmms: */
  calc_variance(cum1, cum2, total_T, SDDIM, gv); /* sigma2 is accumulated over non-silence */
}


ENRO_PRT(extern void prt_lab_j(char str[], ushort nbr, ushort start);) 

/* Now in alinaux_user.h
extern void reverse_list(ushort low, ushort high, ushort start, gmhmm_type *);
*/
  
/*--------------------------------*/
/*
** alignment of test utr with new models
** return total number of segments.
*/
static ushort forced_align(ushort nbr_utr, ushort nbr_frames[], gmhmm_type *gv)
{
  ushort i,t, t_seg, status;
  ushort start_frame = 0, seg_ct = 0;
  
  for (i=0; i<nbr_utr; i++) {
    status = sd_enro_process_a_file(gv,nbr_frames[i], start_frame, seg_ct);
    if (status != eTIesrEngineSuccess ) return 0;
    reverse_list(0,gv->nbr_seg, seg_ct, gv);
    ENRO_PRT(prt_lab_j("D", gv->nbr_seg, seg_ct, gv); )

    for (t=0, t_seg = seg_ct; t<gv->nbr_seg; t++, t_seg++) {
      gv->stt[t_seg] += start_frame;
      gv->stp[t_seg] += start_frame;
    }
    start_frame += nbr_frames[i];
    seg_ct += gv->nbr_seg;
  }
  /* final list: */
  /* prt_lab_j("seg", seg_ct,0);  */
  return seg_ct;
}

/*--------------------------------*/
/*
 * back-tracking for cumulative mean vectors;
*/
static void back_track_viterbi_1(ushort TokenLen, short best_state, ushort curt_pos, HmmType *hmm,
			  ushort *BestPrevState  /* max_len * MAX_NBR_EM_STATES */,short n_mfcc,
			  gmhmm_type *gv, long cum[], ushort cum_ct[])
{
  short t, n_dim = n_mfcc * 2;
  ushort j, pm, *p;
  short mfcc_feature[ SDDIM ]; 
  pm = curt_pos + (TokenLen - 1) * n_mfcc;   
  
  for (t = TokenLen - 1, p = BestPrevState + t * MAX_NBR_EM_STATES; t >= 0;
       t--, p -= MAX_NBR_EM_STATES)  {
    vector_unpacking(gv->mem_feature + pm, mfcc_feature, gv->scale_feat, n_mfcc);  
    for (j = 0; j < n_dim; j++) cum[ best_state * n_dim + j ] += mfcc_feature[ j ];
    cum_ct[best_state] += 1;
    pm -= n_mfcc;
    best_state = p[best_state];
  }
}


/* Now part of search_user 
extern short gauss_obs_score_f(short *feature, int pdf_idx, gmhmm_type *);
*/


/*--------------------------------*/
/* 
 * Viterbi segmentation: 
 * use the current HMM to find the best state-time alignement of the given sequence 
 * return max prob and best state at last time index
 */
static void pdf_of_all_codes(HmmType *hmm, ushort crt_vec, short pdf_scr[], gmhmm_type *gv)
{
  ushort i, idx;
  short mfcc_feature[ SDDIM ];   
  
  vector_unpacking(gv->mem_feature + crt_vec, mfcc_feature, gv->scale_feat, SD_N_MFCC);
  FOR_EMS_STATES(i,hmm,gv->base_tran) { /* calculated more than once if shared. sh'd iter on whole pdf set */
    idx = GET_BJ_IDX(hmm,i);
    pdf_scr[ idx ] = gauss_obs_score_f(mfcc_feature, idx, gv);
  }
}


/*--------------------------------*/
/*
** Viterbi alignment, update completed:
** - long column -> short column
*/
static long Viterbi_path(HmmType *hmm, ushort seg_head, ushort TokenLen, short *obs_pdf, short *best_i, 
		  ushort *BestPrevState, gmhmm_type *gv)
{ 
   ushort j,i,t;
   long max_prob_i, curt_prob;
   short trans;
   ushort exit_state, *p;
   short  column[2 * MAX_NBR_EM_STATES], *pc, *pcj;  /* viterbi score */
   short  best_cur_scr = BAD_SCR; /* best score for current frame evaluation */
   short  best_prev_scr;          /* best score from previous frame evaluation */
   long   cum_best_prev_scr = 0;
   
   *best_i = -1 /* undefined state */;
   p = BestPrevState;
   /* initializing entry time = 0: */
   pdf_of_all_codes(hmm,seg_head,obs_pdf, gv);
   FOR_EMS_STATES(j,hmm,gv->base_tran ) {
     column[j] = (HAS_TRANSITION(GET_PI(hmm,gv->base_tran)[j]))? 
       GET_PI(hmm,gv->base_tran)[j] + obs_pdf[GET_BJ_IDX(hmm,j)]: BAD_SCR;
     p[j] = *best_i;
     if (best_cur_scr < column[j]) best_cur_scr = column[j];
   }
   /* recursion: time [1] -> time[TokenLen-1] */
   p += MAX_NBR_EM_STATES;
   for (t = 1; t < TokenLen; t++) {
     best_prev_scr = best_cur_scr;
     cum_best_prev_scr +=  best_prev_scr;
     best_cur_scr = BAD_SCR;
     if (M2(t)) { pc = column;  pcj= column +  MAX_NBR_EM_STATES; }
     else       { pcj= column;  pc = column +  MAX_NBR_EM_STATES; }
     seg_head += SD_N_MFCC;
     pdf_of_all_codes(hmm,seg_head,obs_pdf, gv);
     FOR_EMS_STATES(j,hmm, gv->base_tran) {
       max_prob_i = BAD_SCR;
       FOR_EMS_STATES(i,hmm, gv->base_tran) {
	 trans = GET_AIJ(hmm,i,j, gv->base_tran);
	 if (HAS_TRANSITION(trans)) {
	   curt_prob = trans + pc[i];
	   if (curt_prob > max_prob_i) { max_prob_i = curt_prob; *best_i = i;  }	
	 }
       }
       curt_prob = max_prob_i +  obs_pdf[ GET_BJ_IDX(hmm,j) ];
       curt_prob -= best_prev_scr;
       if (curt_prob < BAD_SCR) pcj[j] = BAD_SCR;
       else pcj[j] = curt_prob ;
       /* keep the best sco: */
       if (best_cur_scr < pcj[j]) best_cur_scr = pcj[j];
       p[j]= *best_i;
     }
     p += MAX_NBR_EM_STATES;
   }
   /* time[TokenLen-1]--> exit state(numStates) (no emission pdf) */
   max_prob_i = BAD_SCR;
   pc = column + M2(TokenLen-1) * MAX_NBR_EM_STATES;
   exit_state = NBR_STATES(hmm, gv->base_tran) - 1;
   FOR_EMS_STATES(i,hmm,gv->base_tran ) {
     if ( HAS_TRANSITION(GET_AIJ(hmm,i, exit_state, gv->base_tran))) {
       curt_prob = (long)GET_AIJ(hmm,i, exit_state, gv->base_tran) + (long)pc[i];
       if (curt_prob >= max_prob_i) { *best_i = i; max_prob_i = curt_prob;  }
     }
   }
   /* *best_i now gives last internal state along best state sequence */
   /* this log prob takes into account of transition into exit state */
   return max_prob_i + cum_best_prev_scr ; 
} 


/*--------------------------------*/
/*
** compute new hmm state mean by averaging, transition probabilities are not touched.
*/
static void state_update_prm(HmmType *hmm, short n_dim, gmhmm_type *gv, long cum[], ushort cum_ct[])
{
  ushort mix, i, d, cum_c;
  short *bj, *pmix;
  
  cum_c = 0;
  FOR_EMS_STATES(i,hmm, gv->base_tran) { /* loop for each HMM emission state */
    bj = GET_BJ(gv,hmm,i);
    FOR_EACH_MIXING(mix,pmix,bj) {
      if (cum_ct[i]) 
#ifdef BIT8MEAN
	{ short tmp [ SDDIM ], *pmu =  GET_MU(gv->base_mu, pmix, (n_dim>>1));
	  for (d = 0; d < n_dim; d++) tmp[d] = (short)(cum[cum_c + d] / cum_ct[i]);
	  vector_packing(tmp, (ushort *) pmu, gv->scale_feat, SD_N_MFCC);
	}
#else
	for (d = 0; d < n_dim; d++) GET_MU(gv->base_mu, pmix, n_dim)[d] = (short)(cum[cum_c + d] / cum_ct[i]);
#endif
      else { /* it's fine to have this: */
	ENRO_PRT(printf("found div by 0\n");)
      }
    }
    cum_c += n_dim;
  }
}

/* MSX_SEG_SIZE limits segment size: this may create more "div by zero" (above) */


/*--------------------------------*/
/*
** retrain one-gaussian/state  HMMs, with excluded symbol (silence, not trained)
** iterative improvement of HMM parameters via Viterbi alignment
** warning: the mixture number must be one (for name dialing)
** Can't reuse gtm/calvit.c because of different variables in the loop.
** use the current HMM to state-align all training segments.
*/
TIesrEngineStatusType
static train_vite_excl(ushort ix_code, ushort nbr_hmm, ushort nbr_seg, ushort *mem_base, ushort mem_count,
		     short n_dim, short *sco, gmhmm_type *gv, long cum[])
{
  ushort h_code, seg, state, pm, *BestPrevState, iter,
         total_T = 0, hmm_T, crt_vec, token_order, seg_len;
  short best_i, converged, cum_c, new_likelihood, last_likelihood = BAD_SCR;
  long  total_log_prob = 0, hmm_sum, max_prob;
  HmmType *hmm;
  ushort cum_ct[MAX_NBR_EM_STATES];       /* mean vector counter */


  
  for (iter = 1,  converged = 0; iter <= MAX_ITR && !converged; iter++) {
    total_log_prob = 0;
    total_T = 0;
    for (h_code = 0; h_code < nbr_hmm; h_code++) { /* for each hmm model */
      if (ix_code != h_code) {
	hmm_sum = 0;
	hmm_T = 0;
	hmm = GET_HMM(gv->base_hmms, h_code, 0);
	cum_c = 0;
	FOR_EMS_STATES(state,hmm, gv->base_tran) {
	  mean_ini(cum + cum_c, SDDIM);  /* clear cumulator */
	  cum_ct[state] = 0;
	  cum_c += SDDIM;
	}
	token_order = 0;
	for (seg = 0; seg < nbr_seg; seg++) { /* for each seg */
	  
	  if (gv->hmm_code[seg] == h_code) { /* found the segment */
	    /* start and frame and number of frames in the segment */
	    seg_len = gv->stp[seg] - gv->stt[seg];
	    if (seg_len > MAX_SEG_SIZE) seg_len = MAX_SEG_SIZE;
	    
	    crt_vec = gv->stt[seg] * SD_N_MFCC; /* mfcc & dmfcc */
	    BestPrevState = mem_base + mem_count; /* this one must be the last memory alloc*/
	    pm = mem_count + seg_len * MAX_NBR_EM_STATES; 
	    /* if memory over flown, just skip, keeping old model paramters */
	    if (!cap_in_check("back-tracking", pm, ALL_MEM_SIZE)) return eTIesrEngineBackTrackMemorySize;
	    max_prob = Viterbi_path(hmm,crt_vec,seg_len,gv->obs_scr, &best_i, BestPrevState, gv);
	    if (max_prob > LONG_BAD_SCR) {
	      hmm_sum += max_prob; 
	      hmm_T   += seg_len; 
	      back_track_viterbi_1(seg_len,best_i,crt_vec, hmm, BestPrevState,SD_N_MFCC, gv, cum, cum_ct);
	    }
	    else { /* the data of this segment will not be put in pool */
	      ENRO_PRT(printf("No Viterbi alignment path found: ");)
	    }
	    token_order++;
	  }
	}
	total_log_prob += hmm_sum;
	total_T += hmm_T;
	state_update_prm(hmm, n_dim, gv, cum, cum_ct); /* update hmm prms */
	/* printf("%7.4f\n", hmm_sum/64./hmm_T); */ /* likelihood for the hmm */
      }
    }
    new_likelihood = (short) (total_log_prob/ total_T); /* for all segments */
    converged = (new_likelihood - last_likelihood) < 1;  /* shreshood */ 
    ENRO_PRT(converged = test_convergence(new_likelihood,last_likelihood,iter);)
    last_likelihood = new_likelihood;    
  }
  ENRO_PRT(printf("frame log likelihood = %11.7f (%d)\n", new_likelihood/64., total_T); /* Q6 */)
  *sco = new_likelihood; 
  return eTIesrEngineSuccess ;
}



/* N bits will be shifted out */
/*
#define ROUND_AT_N(tmp,N) { tmp += 1 << ((N)-1); tmp >>= (N);}
*/


/*--------------------------------*/
/* 
** Save state nbrs, mean vector, and variance. Assuming the last hmm is silence
*/
static short save_model_prms(char o_dir[], ushort nbr_hmm, gmhmm_type *gv)
{
  FILE *pf;
  ushort d, h, varn, nbr = nbr_hmm - 1; /* silence not in */
  ushort nbr_em_states = 0; /* total nbr of emit states */
  HmmType *hmm;
  char len_name[MAX_STR];
  short *pmu;
#ifdef  VQMODEL
  ushort coded[ VECSIZE ];
#endif

#ifdef BIT8MEAN
  short scale_mu0[ SDDIM];  // scale_mu set to zero 
  for (d = 0; d < SDDIM; d++)  scale_mu0[d] = 0; 
#endif
  
  sprintf(len_name,"%s%s", o_dir,SDEXT);
  pf = fopen(len_name,"wb");
  if (!pf) {
    ENRO_PRT(fprintf(stderr,"open failed: %s\n", len_name);)
    return FALSE;
  } 
  fwrite(&nbr,sizeof(ushort),1,pf);
  
  for (h = 0; h < nbr; h++) {
    hmm = GET_HMM(gv->base_hmms, h, 0);
    varn = NBR_STATES( hmm, gv->base_tran );
    fwrite(&varn,sizeof(ushort),1,pf);
    nbr_em_states += varn - 1;
  }
  /* mean */

  pmu = gv->base_mu; /* pmu points to the static part of the mean vectors */
  for (h = 0; h < nbr_em_states; h++) {
#ifdef BIT8MEAN
    short tmp[ SDDIM ];
    vector_unpacking((ushort *)pmu, tmp, gv->scale_feat, SD_N_MFCC);
    vector_packing(tmp, (ushort *)pmu, scale_mu0,  SD_N_MFCC);
#ifdef VQMODEL /* output VQ-ed models: */
    pack_to_vq(pmu, coded);    
    fwrite(coded,sizeof(short), VECSIZE, pf); 
#else /* output packed models: */
    fwrite(pmu,sizeof(short), SD_N_MFCC, pf); 
#endif
    pmu += SD_N_MFCC; 
#else
    for (d = 0; d < SD_N_MFCC; d++) {
      /* ENCODE (packing) MEAN */
      char tmp1 = pmu[d] >> 8;
      char tmp2 = pmu[d + SD_N_MFCC] >> 8;
      ushort tmp_sh = (tmp1 << 8) + tmp2;
      fwrite(&tmp_sh,sizeof(short), 1, pf); 
    }
    pmu += SDDIM; /* skip the delta part, as it has been processed */
#endif
  }
  
  /* variance */
  //  fwrite(base_var, sizeof(short), SDDIM,pf); /* only one variance is needed */
  fclose(pf);
  return TRUE;
}


#ifdef BIT8FEAT
//extern void get_regression_sequence(ushort total_frm, ushort start_frame,  short n_mfcc);
#endif


/*--------------------------------*/
/*
** enrollement of one command, return the score in Q6
*/
short enroll_one_name(short n_mfcc, ushort total_frames[], ushort nbr_enr_utr, 
		      ushort silence, ushort bws, char out_dir[],short nbr_dim, short *sco,
		      gmhmm_type *gv, sd_enro_type *enro)
{
  ushort total_seg, nbr_hmm, nbr_sym, mem_count = 0; 
  short the_status;
  short t;
  short *search_space; /* search memory */
  ushort *state_nbr; /* number of states for each hmm */
  long cum[MAX_NBR_EM_STATES * SDDIM];    /* mean vector accumulator */

  /*
  ** printf("memory size for enrollment (search %d + segment %d) = %d (words)\n", 
  **	 TOTAL_SEARCH_SIZE, TOTAL_SEG_SIZE, ALL_MEM_SIZE);
  ** Will print: "memory size for enrollment (search 4270 + segment 240) = 4510 (words)"
  */

  // printf("Refinement search memory = %d (words)\n", TOTAL_SEARCH_SIZE_R);

  seg_memory_assign((ushort*) enro->search_mem, gv, NBR_ENR_UTR * SD_MAX_NBR_SEGS);

/*
** capacity specification
*/

  search_space = enro->search_mem + TOTAL_SEG_SIZE; /* search memory start */
  mem_count += TOTAL_SEG_SIZE;
  if (!cap_in_check("segments", mem_count, ALL_MEM_SIZE)) return eTIesrEngineSegmentSizeCheckFail;
  set_search_space(search_space, BEAM_Z_, SYMB_Z_, STATE_Z_, TIME_Z_, gv);
  PRT_ERR(printf("Total search memory: %d (words)\n",SEARCH_SIZE(BEAM_Z_, SYMB_Z_, STATE_Z_, TIME_Z_)));
#ifdef BIT8FEAT
  /* 
  ** calculate delta mfcc
  */ 
  get_regression_sequence(gv, 0,total_frames[0]); 
  get_regression_sequence(gv, total_frames[0], total_frames[1]);
  
#endif


/*
** 1. SEGMENTATION: 
** add bias (specific to the first enrl-utterance) to all hmm mean: 
** use only the first utterance: 
*/
  the_status = adjust_mean((short *)gv->mem_feature,
#ifndef BIT8FEAT
		       mem_mfcc,mem_d_mfcc,
#endif
		       total_frames[0], silence, bws, gv, cum);
  if (the_status != eTIesrEngineSuccess) /* fail to align */  return the_status;
    
/*
** 2. CONSTRUCT HMM TOPOLOGY AND INITIALIZE THE MEAN VECTORS:
*/
  reverse_list(0,gv->nbr_seg,0, gv);

  t = MAX(gv->stp[0] - 3, 1); // include more leading  silence 
  gv->stp[0] = gv->stt[1] = t;

#ifdef DEBUG_WITH_PHONE_STR
  prt_lab("C", nbr_seg);
#endif

  if (gv->nbr_seg > SD_MAX_NBR_SEGS) {
    PRT_ERR(fprintf(stderr,"utterance contains too many segments\n"));
    return (MAX_NBR_SEGS);
  }
    nbr_hmm =  gv->nbr_seg - 1; /* two NDSIL at the ends: /h# a b c h#/, we need only (last) one  */
    state_nbr = (ushort *) enro->search_mem + mem_count; 
    mem_count += SD_MAX_NBR_SEGS - 1; /* only one silence model */
    if (!cap_in_check("state memory", mem_count, ALL_MEM_SIZE)) return  eTIesrEngineStateMemoryOut;
    model_size(silence, bws, nbr_hmm, state_nbr, gv);

    // printf("original model n_pdf = %d\n", n_pdf);

    save_old_model( gv, enro ); /* copy pointors of original model */

    /* this will reassign grammar and gtm: */

    the_status = new_spot_reco_gtm(nbr_hmm,  state_nbr, (ushort*)enro->search_mem, &mem_count, ALL_MEM_SIZE, 0, gv); 
    if (the_status != eTIesrEngineSuccess) return the_status;

    ENRO_PRT(printf("new model n_pdf = %d\n", n_pdf);)
#ifdef BIT8FEAT
    mac_to_hmm(state_nbr, silence, nbr_hmm,  total_frames[0], 0,0, gv, enro);
#else
    mac_to_hmm(state_nbr, silence, nbr_hmm, mem_mfcc, mem_d_mfcc, total_frames[0], 0,0);
#endif
    /* From here, no need for original models anymore */
    nbr_sym = nbr_hmm;
/* 
** 3. SUPERVISED ALIGNMENT AND MODEL REFINEMENT 
*/
    make_net(gv, nbr_sym, &mem_count, (short *)enro->search_mem, ALL_MEM_SIZE);
    
    /*
    ** refinement search need small memory size
    */
    search_space = enro->search_mem + mem_count; /* search memory start */
    mem_count += TOTAL_SEARCH_SIZE_R;
    
    if (!cap_in_check("refinement search memory", mem_count, ALL_MEM_SIZE)) return eTIesrEngineSearchMemoryOut;  
    set_search_space(search_space, BEAM_R, SYMB_R, STATE_R, TIME_R, gv);
    PRT_ERR(printf("Total search memory: %d (words)\n",SEARCH_SIZE(BEAM_R, SYMB_R, STATE_R, TIME_R)));
    
    /* viterbi alignment: */
    total_seg = forced_align(NBR_ENR_UTR, total_frames, gv);

    if (total_seg == 0) /* fail to align */ return  eTIesrEngineAlignmentFail;
    
    /* reestimation of prms: */
    the_status = train_vite_excl(nbr_hmm - 1, nbr_hmm, total_seg, (ushort *)enro->search_mem, mem_count, nbr_dim, sco, gv, cum);
    if (the_status != eTIesrEngineSuccess) return the_status;
    else if (*sco == BAD_SCR)   return eTIesrEngineAlignmentFail;
/*
** 4. save the model parms: 
*/
    if (!save_model_prms(out_dir, nbr_hmm, gv)) return eTIesrEngineSaveFail;
    else return eTIesrEngineSuccess;
}
