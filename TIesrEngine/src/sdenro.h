/*=====================================================================

 *
 * sdenro.h
 *
 * This header exposes some constants and macros needed during TIesr SD
 * enrollment processing.
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 
 =====================================================================*/

#ifndef _SDENRO_H
#define _SDENRO_H

#include "tiesr_config.h"
#include "sdparams.h"
#include "gmhmm_type.h"
#include "sdhead.h"


/*--------------------------------*/
/* Defines */
/*


** print message during SD enro, for diagnosis
*/
#define ENRO_PRT(x) 


#if defined BIT8MEAN || defined BIT8FEAT
/* 
** The original mu was CMN processed, and in enrollment we use non CMN.
** So this scale (obtained from WAVES digits non-CMN) is needed.
*/
const short new_scale_mu [] = {
 0,    1,    2,    1 ,   1,    1,    1,    1,
 1,    2,    2 ,   2 ,   2 ,   2 ,   3 ,   3 
};

#ifdef BIT8FEAT
//short *scale_feat = new_scale_mu;
#endif
#endif


#define silence_code 11
#define bws_code 5


/*
** assign memory for alignment results
*/
#define TOTAL_SEG_SIZE (3 * NBR_ENR_UTR * SD_MAX_NBR_SEGS)


/* initial search space */
//#define BEAM_Z_   320
#define BEAM_Z_   1000
#define SYMB_Z_   190
#define STATE_Z_  810
#define TIME_Z_   60
#define TOTAL_SEARCH_SIZE   SEARCH_SIZE(BEAM_Z_, SYMB_Z_, STATE_Z_, TIME_Z_)


/* refinement search space */
// refinedment: 74.0000   14.0000   71.0000   33.0000
#define BEAM_R   110 // 90 /* increased after global constant variance for enrollment */
#define SYMB_R   30
#define STATE_R  100
#define TIME_R   40
#define TOTAL_SEARCH_SIZE_R  SEARCH_SIZE(BEAM_R, SYMB_R, STATE_R, TIME_R)


#define ALL_MEM_SIZE (TOTAL_SEARCH_SIZE + TOTAL_SEG_SIZE)


#endif /* _SDENRO_H */
