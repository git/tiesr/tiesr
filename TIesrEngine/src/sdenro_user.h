/*================================================================

 *
 * sdenro_user.h
 *
 * This header exposes the interface to the TIesr SD enrollment api
 * processing functions.
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *

 ==================================================================*/

#ifndef _SDENRO_USER_H
#define _SDENRO_USER_H

#include "tiesr_config.h"
#include "tiesrcommonmacros.h"
#include "gmhmm_type.h"
#include "sd_enro_type.h"

short enroll_one_name(short n_mfcc, ushort total_frames[], 
		      ushort nbr_enr_utr, ushort silence, ushort bws, 
		      char out_dir[],short nbr_dim, short *sco,
		      gmhmm_type *gv, sd_enro_type *enro);

void free_save_models(sd_enro_type *enro);

#endif /* _SDENRO_USER_H */
