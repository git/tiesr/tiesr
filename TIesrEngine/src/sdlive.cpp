/*==================================================================
 *
 * sdlive.cpp
 *
 * This source defines functionality for TIesr SD recording for both
 * enrollment and recognition.
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *

 ====================================================================*/


/*
** main API for SD live version
*/

#include "tiesr_config.h"
#include "tiesrcommonmacros.h"

#include "winlen.h"
#include "gmhmm_type.h"
#include "gmhmm_type_common_user.h"
#include "uttdet_user.h"
#include "mfcc_f_user.h"
#include "volume_user.h"

#include "sdhead.h"
#include "gmhmm_sd_api.h"



/*--------------------------------*/
/* GMHMM_SD_API */ 
void TIesrSDRecordOpen(gmhmm_type *gvv)
{
  gmhmm_type *gv = (gmhmm_type *) gvv;
  gv->n_mfcc = SD_N_MFCC;
  dim_p2_init(gv->n_mfcc, gv ); 
  gv->amplitude_max = 0; 
  gv->amplitude_min = 0; 
  gv->signal_cnt = 0;

  // We don't want to set default uttdet parameters here, since the API
  // user may already have changed them.
  // set_default_uttdet(gv);

  init_uttdet( gv);

  gv->speech_detected = 0;

  /* Since enroll recording is not using the search engine, need to
  set this so that utterance detection works ok */
  gv->sr_is_end_of_grammar = TRUE;

  gv->frm_cnt = 0;
  gv->mfcc_cnt = gv->mem_start_frame * gv->n_mfcc;
  gv->last_sig = 0;
  //  for (s = 0; s< WINDOW_LEN; s++) sig[s] = 0; /* clear sig buffer */
  //  *p_sig = sig + OVERLAP; /* buffer location for aread */
  //  if (calc_logN) log_N_flag = TRUE;
  //  else log_N_flag = NULL;
  //  gv->mem_feature = (ushort *) mem;
  //  gv->max_frame_nbr = max_frm;
  gv->pred_first_frame = TRUE;
}


/*--------------------------------*/
/* GMHMM_SD_API */ 
/* open with reset */
void TIesrSDRecordOpen_1(gmhmm_type *gvv) 
{
  gmhmm_type *gv = (gmhmm_type *) gvv;
  gv->mem_start_frame = 0;
  gv->nbr_loaded_utter = 0;
  TIesrSDRecordOpen(gvv);
}


/*--------------------------------*/
/* GMHMM_SD_API */ 
TIesrEngineStatusType TIesrSDRecordClose(gmhmm_type *gvv)
{
  gmhmm_type *gv = (gmhmm_type *) gvv;

#ifdef USE_AUDIO
  /*  extern short set_volume_flag(gmhmm_type *);*/
  gv->vol = set_volume_flag(gv);
#endif

  /* Reset the status of the search engine flag, since recording is complete. */
  gv->sr_is_end_of_grammar = FALSE;

  if (gv->nbr_loaded_utter == NBR_ENR_UTR) return eTIesrEngineEnrollLoadFail;
  else {
    /* gv->total_frames[gv->nbr_loaded_utter] = GetSDFrameCount(gv); */
    gv->total_frames[gv->nbr_loaded_utter] = GetFrameCount(gv);
    //    printf("frame = %d\n", total_frames[nbr_loaded_utter]);
    gv->mem_start_frame += gv->total_frames[gv->nbr_loaded_utter];
    gv->nbr_loaded_utter++;
  }
  
  if (gv->frm_cnt < MIN_RECORD_FRM) return eTIesrEngineShortUtterance;
  else return eTIesrEngineSuccess;
}


/*
** /1/ live record, for recognition, with noise estimate, no regression
**     noise estimate in MFCC domain
** /2/ this function is also used for live record of enrollment. The noise estimate
**     (in log_N) is not calculated in that case.
*/

/*--------------------------------*/
/* GMHMM_SD_API */ 
TIesrEngineStatusType  TIesrSDRecord(gmhmm_type *gvv, short sig_frm[], 
				     short calc_noise_estimate)
{
  gmhmm_type *gv = (gmhmm_type *) gvv;
  short  i, idx, log_mel_energy[ N_FILTER26 ];
  short curt_noise[ MAX_DIM_MFCC16 ];        /* current noise in mfcc  */
  long  tmp;
  unsigned short idx_noise;
  short noise_start_time;
  NormType var_norm;
  short power_spectrum[ WINDOW_LEN ];     

  /* do nothing but shifting input buffer */
  if (gv->pred_first_frame) {
    /* overlap */
    for (i = 0; i < OVERLAP; i++) 
      gv->sample_signal[ i ] = sig_frm[ i + (FRAME_LEN - (OVERLAP))];
    gv->pred_first_frame = FALSE;
    return  eTIesrEngineSuccess;
  }

  /* pack the input data into proper location */ 
  for (i = 0; i < FRAME_LEN; i++)  
    gv->sample_signal[ i + OVERLAP] = sig_frm[ i ];

  /* put into circular buffer, for regression computation */
  idx = circ_idx( gv->signal_cnt );
    
  mfcc_a_window(gv->sample_signal, gv->mfcc_buf[ idx ], log_mel_energy, gv->n_mfcc, 		  gv->n_filter, gv->mel_filter, gv->cosxfm,
		  power_spectrum, gv->muScaleP2, &var_norm, &(gv->last_sig), NULL );
  /* utterance detection */
    
  // compute_uttdet( power_spectrum, gv->signal_cnt,  mfcc_buf[ idx ] );
  compute_uttdet( power_spectrum, gv->signal_cnt, gv->frm_cnt, gv->mfcc_buf[ idx ], gv, &var_norm);

  if ( gv->speech_detected == 0 && gv->uttdet_state == IN_SPEECH ) {
	   gv->speech_detected = 1;
  }
  if (!gv->speech_detected) { /* silence, non-speech frame */
    if (calc_noise_estimate) {
      noise_start_time = gv->signal_cnt - ( MFCC_BUF_SZ - 1 ) + REG_SPAN;
	
      if (noise_start_time >=0) idx_noise = circ_idx( noise_start_time );
      else {
	idx_noise = 0;
	for ( i = 0; i < gv->n_mfcc; i++ )  gv->mfcc_noise[ i ] =  gv->mfcc_buf[ idx_noise ][ i ];
      }
      
      /* cannot use log_mel_energy, as it is not bufferized */
      for ( i = 0; i < gv->n_mfcc; i++ ) {
	tmp = 3 * (long) gv->mfcc_buf[idx_noise][ i ] + (long) gv->mfcc_noise[ i ] * 13;
	gv->mfcc_noise[ i ] = (short) ( tmp >> 4 );
	curt_noise[ i ] = gv->mfcc_noise[ i ]; /* cos_transform changes curt_noise[0], to be fixed later */
      }
      /* cepstrum to log mel power spectrum, Q 11 to Q 9 */
      cos_transform( curt_noise, gv->log_N, gv->muScaleP2, gv->n_mfcc, gv->n_filter,  gv->cosxfm);
    }  
  }
  else { /* speech detected: */
      
    if (( gv->mfcc_cnt + gv->n_mfcc) >=  gv->max_frame_nbr * gv->n_mfcc ) return eTIesrEngineFeatureMemoryOut;
    
    /* several frames has already passed detection, go back */
    idx = circ_idx( idx - ( MFCC_BUF_SZ - 1 ) + REG_SPAN );
    //      compute_regression( idx, 1, reg_mfcc, n_mfcc );
    for (i = 0; i < gv->n_mfcc; i++){
      gv->mem_feature[gv->mfcc_cnt + i] = gv->mfcc_buf[idx][i];
    }
      
    gv->frm_cnt++;
    gv->mfcc_cnt += gv->n_mfcc;

  }
  //    PRT_ERR(printf("[%.3d] frm = %3d %2d %2d\n", gv->signal_cnt, gv->frm_cnt, 
  //	   gv->speech_detected , gv->uttdet_state == NON_SPEECH); )
  /* overlap */
  for (i = 0; i <  OVERLAP; i++) {
    gv->sample_signal[ i ] =  gv->sample_signal[ i + FRAME_LEN ];
    if ( gv->sample_signal[i] > gv->amplitude_max) gv->amplitude_max =  gv->sample_signal[i];
    else if ( gv->sample_signal[i] < gv->amplitude_min) gv->amplitude_min =  gv->sample_signal[i];
  }
  gv->signal_cnt++;
  if ( gv->signal_cnt > TIMEOUT ) return eTIesrEngineTimeOut;
  return eTIesrEngineSuccess;
}
