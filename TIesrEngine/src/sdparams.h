/*==============================================================
 
 *
 * sdparams.h
 *
 * sdparams.h exposes the number of utterances
 * that must be collected to enroll a model for a TIesr SD enrollment name.
 * This must be exposed to the application designer, so they know how
 * many times to prompt the user.
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *

 =================================================================*/


#ifndef _SDPARAMS_H
#define _SDPARAMS_H

/* number of enrollment utterances */
#define NBR_ENR_UTR 2 


#endif /* _SDPARAMS_H */
