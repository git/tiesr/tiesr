/*================================================================
 
 *
 * sdreco.h
 *
 * This header defines constants used during TIesr SD recognition.
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *

 ====================================================================*/


#ifndef _SDRECO_H
#define _SDRECO_H

#include "tiesr_config.h"

#include "gmhmm_type.h"

#include "sdhead.h"


/*
 ** search parameters for name dialing: prune = -7: 455  &  30  & 124   & 73  & total 1889 words
 */

#define BEAM_Z  480
#define SYMB_Z   40
#define STATE_Z 140
#define TIME_Z   90

//#define BEAM_Z  455
//#define SYMB_Z   30
//#define STATE_Z 124
//#define TIME_Z   73

/* memory: 
 ** background cluster memory + grammar + hmm
 */

#ifdef BIT8MEAN 
#define TOTAL_MODEL_SIZE  2000 /* in short */
#else
#define TOTAL_MODEL_SIZE  2500 /* in short */
#endif


/*--------------------------------*/
/* Defines as function macros */

/* memory: 
 ** search space for name dialing
 */
#define TOTAL_SEARCH_SIZE   SEARCH_SIZE(BEAM_Z, SYMB_Z, STATE_Z, TIME_Z)


/* total memory: (background cluster memory + grammar + hmm ) + search space */

// #define MAX_MEM (TOTAL_MODEL_SIZE + TOTAL_SEARCH_SIZE)

/* Max number of syms during recognition */
#define MAX_RECO_SYMS  ( SD_MAX_NBR_SEGS + 2 )
#define MAX_RECO_GAUSS ( MAX_RECO_SYMS * MAX_NBR_EM_STATES )


#endif /* _SDRECO_H */
