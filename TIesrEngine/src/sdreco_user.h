/*----------------------------------------------------------------
  sdreco_user.h

  This header exposes the functionality of sdreco.cpp, which 
  performs recognition in TIesr SD.
  4/27/05 LN
  ----------------------------------------------------------------*/
#ifndef _SDRECO_USER_H
#define _SDRECO_USER_H

#include <stdio.h>

#include "tiesr_config.h"
#include "tiesrcommonmacros.h"
#include "status.h"

#include "gmhmm_type.h"
#include "sd_reco_type.h"


TIesrEngineStatusType load_1_net_and_gtm(char *dir, char *network_name, 
					 ushort memory_size, short clusters[], 
					 short nbr_class, gmhmm_type *gv, 
					 sd_reco_type *sdv);


TIesrEngineStatusType make_bgd_net_and_gtm(short clusters[], short nbr_class, 
					   ushort memory_size, gmhmm_type *gv,
					   sd_reco_type *sdv);


void pmc_all_hmms(gmhmm_type *gv);


void read_one_vector(FILE *pf_data, short vec[]);


short rec_process_a_file(gmhmm_type *gv);


#endif /* _SDRECO_USER_H */
