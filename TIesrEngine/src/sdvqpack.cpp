/*====================================================================
 
 *
 * sdvqpack.cpp
 *
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 *

 ======================================================================*/

#include "tiesr_config.h"

#include "sdhead.h"
#include "pack_user.h"
#include "sdvqpack_user.h"

#include <stdlib.h>

#ifdef VQMODEL

/*
** generated using WAVES names non-quantified models
*/
/* codebook (8 x 16 = 128 words) */
static const short codebook[  SD_N_MFCC ][ MIXN ] = { 
{ 13569, 15102, 15867, 16632, 17396, 17903, 18664, 19168, 19716, 20231, 21002, 21773, 22289, 22806, 23069, 23845,},
{     1,   770,  2051,  3589,  -761, -1526, -2033, -2794, -3073, -3842, -4612, -5382, -6665, -7948,-10000,-13335,},
{   256,  1023,  -258,  -515,  1787,  2553,  3830,  5617, -1535, -1790, -2557, -3324, -4090, -4856, -5877, -7409,},
{    -1,   766,  -771, -1541, -2567, -3850, -5903, -9237,  1281,  1794,  2564,  3334,  4104,  5131,  6414,  8724,},
{     1,  -766,  1028,  2566, -1271, -2036, -2800, -3562, -4097, -4866, -5636, -6407, -7434, -8717,-10256,-12309,},
{   513,  1282,  2307,  3844,  -250,  -504, -1269, -1520, -1793, -2306, -3075, -3844, -4614, -5640, -6923, -9233,},
{   512,  -257, -1282, -2563, -4101, -5895, -7946,-11277,  1281,  2306,  3331,  4612,  5894,  7431,  9482, 12557,},
{   256,  1025,  1794,  2819,  4100,  5637,  7431, 10506,  -513, -1282, -2051, -3076, -4101, -5638, -7688,-11275,},
}; 
/* end of codebook */ 

/*--------------------------------*/
/*
** unpack all the mixtures of the dimension, then find the codeword.
*/
void vq_decoding(short *xl, short *xh, short dim, unsigned short coded_vec[])
{
  short i, i_best_l, i_best_h ;
  short cword[MIXN*2]; /* can be remved later */
  short H_L_code, code_position = dim >>1; /* from 0 to 3 */
  const short *p_code = codebook[dim];

  if (dim & 1) H_L_code = coded_vec[code_position] & 0xff; /* odd dim  */ 
  else         H_L_code = coded_vec[code_position] >> 8;   /* even dim */
  i_best_l = H_L_code & 0x0f;
  i_best_h = H_L_code >> 4;

  for (i=0; i<MIXN; i++) {
    DECODE_STDY(p_code[i], cword[i], cword[i+MIXN]);
    cword[i] >>= 8;
    cword[i+MIXN] >>= 8;
  }
  *xl = cword[ i_best_l ];
  *xh = cword[ i_best_h + MIXN ];
}


/*--------------------------------*/
void vq_to_pack(unsigned short coded[], short packed[])
{
  short d,  v[ SDDIM ];
  
  for (d = 0; d < SD_N_MFCC ; d++) {
    vq_decoding(&(v[d]), &(v[d + SD_N_MFCC]), d, coded);
    packed[d] = (v[d] << 8) + (v[d + SD_N_MFCC] & 0xff);
  }
}


/*--------------------------------*/
void vq_coding(short xl, short xh, short dim, unsigned short coded_vec[])
{
  short i, i_best_l = 0, min_l = 256, diff;
  short    i_best_h = 0, min_h = 256;
  short cword[MIXN*2]; /* can be remved later */
  const short *p_code = codebook[dim];
  short H_L_code, code_position = dim >>1; /* from 0 to 3 */

  for (i=0; i<MIXN; i++) {
    DECODE_STDY(p_code[i], cword[i], cword[i+MIXN]);
    cword[i] >>= 8;
    cword[i+MIXN] >>= 8;

    diff = xl - cword[i];
    diff = abs(diff);
    if (min_l > diff) {
      min_l = diff;
      i_best_l = i;
    }

    diff = xh - cword[i+MIXN];
    diff = abs(diff);
    if (min_h > diff) {
      min_h = diff;
      i_best_h = i;
    }
  }
  // coded value:
  //  xl = cword[ i_best_l ];
  //  xh = cword[ i_best_h + MIXN ];
  H_L_code = i_best_l + (i_best_h<<4);

  if (dim & 1) 
    coded_vec[code_position] = (coded_vec[code_position]<<8) + H_L_code; /* odd dim */ 
  else 
    coded_vec[code_position] = H_L_code; /* even dim */
}


/*--------------------------------*/
/*
** return the VQ-ed and packed (into 4 words) mean vector
*/
void pack_to_vq(short tmp[],  unsigned short coded[])
{
  short d,  unpacked[SDDIM];

  for (d = 0; d < SD_N_MFCC ; d++) { 
/* unpack */
      DECODE_STDY(tmp[d], unpacked[d], unpacked[d + SD_N_MFCC]);
      unpacked[d] >>= 8;
      unpacked[d + SD_N_MFCC] >>= 8;
/* code by VQ */
      vq_coding(unpacked[d], unpacked[d + SD_N_MFCC], d, coded);
  }
}

#endif /* VQMODEL */
