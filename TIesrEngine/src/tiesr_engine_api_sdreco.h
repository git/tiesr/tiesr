/*================================================================

 *
 * tiesr_engine_api_sdreco.h
 *
 * This header exposes the interface of the TIesr SD recognition.
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *

=====================================================================*/

#ifndef _TIESR_ENGINE_API_SDRECO_H
#define _TIESR_ENGINE_API_SDRECO_H

/* 
** Data structure specifying TIESR SD recognition algorithms.
*/

#include "tiesr_config.h"
#include "status.h"


/* This is only included here, since the user must know how many
   samples per frame to supply to the recognizer. */
#include "winlen.h"


/* This is only included here, since the user must know how many
   utterances are needed to complete an enrollment. */
#include "sdparams.h"


/* Typedefs to specify what each API function pointer type is, and enable
 compiler checking. */

typedef struct sd_reco_type* sd_reco_type_p;

#ifndef SD_GMHMM_TYPE_P
#define SD_GMHMM_TYPE_P
typedef struct gmhmm_type* gmhmm_type_p;
typedef struct gmhmm_type const * const_gmhmm_type_p;
#endif

/*--------------------------------*/
/*
   The TIesrEngineSDRECOType structure defines an instance of the
   TIesr SD recognition API.  The user must instantiate and delete an
   instance by opening or closing the instance.
*/

/*
** scope limitation: API function visible only within variable
*/
typedef struct {

  /*
  ** initialization of the SDSR, to be called at beginning of recognition
  */
  /* TIesrSDRecOpen */
  TIesrEngineStatusType (*Open)( sd_reco_type_p, int, int );

  /*
  ** restore 
  */
  /* TIesrSDRecClose */
  void (*Close)( sd_reco_type_p );

  /*
  ** create background models
  */
  /* TIesrSDBgd */
  TIesrEngineStatusType (*LoadBackground)(unsigned short, char *, char *[], sd_reco_type_p );

  /*
  ** initialization of SD recognizer
  */
  /* TIesrSDRecInit */
  void (*Init)  ( sd_reco_type_p );

  /*
  ** set pruning coeff 
  */
  /* SetTIesrPrune */
  void (*SetPrune)( gmhmm_type_p, short);

  /* SetTIesrSDPrune */ 
  void (*GetPrune)( const_gmhmm_type_p, short*);

  /*
  ** confidence score
  */
  /* TIesrSDConf */
  short (*GetConfidence)(short, sd_reco_type_p );

  /*
  ** load test utterance from file, with no utterance detection.
  */
#ifndef USE_AUDIO
  /* TIesrSDRecLoad */
  unsigned short  (*Load)(char [], void *);
#endif

  /*
  ** scoring one models, given utterance
  */
  /* TIesrSDSco */ 
  TIesrEngineStatusType (*Score)(char *, char [], long *, sd_reco_type_p );


  /*
  ** get common (enro/reco)  part of SD
  */
  /* TIesrSDRecoData */
  gmhmm_type_p (*GetRecordData)( sd_reco_type_p );

  /*
  ** for live mode
  */
  /* TIesrSDRecordOpen */
  void (*RecordOpen)( gmhmm_type_p );

  /* TIesrSDRecord */
  TIesrEngineStatusType (*Record)( gmhmm_type_p,short sig[], short);

  /* TIesrSDRecordClose */
  TIesrEngineStatusType (*RecordClose)( gmhmm_type_p );

  short (*GetFrameCount)( const_gmhmm_type_p );

  short (*SpeechEnded)( gmhmm_type_p );

  short (*SpeechDetected) ( gmhmm_type_p );

  short (*GetVolumeStatus)( const_gmhmm_type_p ); 


#ifdef USE_AUDIO
  void  (*SetSADPrams)( gmhmm_type_p, short, short, short, short, short);
  void  (*SetVolumeRange)( gmhmm_type_p, unsigned short, unsigned short);

  void  (*GetSADPrams)( const_gmhmm_type_p, short  *, short *, short *, short *, short *);
  void  (*GetVolumeRange)( gmhmm_type_p, unsigned short *, unsigned short *);
#endif
} TIesrEngineSDRECOType;

//#define USE_DLL 

#ifdef USE_DLL
#ifdef  TIESR_ENGINE_SDRECO_API_EXPORTS
#define TIESR_ENGINE_SDRECO_API __declspec(dllexport)
#else
#define TIESR_ENGINE_SDRECO_API __declspec(dllimport)
#endif
#else /* not USE_DLL */
#define TIESR_ENGINE_SDRECO_API
#endif /* USE_DLL */
#ifdef __cplusplus
extern "C"
{
#endif

TIESR_ENGINE_SDRECO_API void TIesrEngineSDRECOOpen (TIesrEngineSDRECOType *);
TIESR_ENGINE_SDRECO_API void TIesrEngineSDRECOClose(TIesrEngineSDRECOType *);
#ifdef __cplusplus
}
#endif


#endif /* _TIESR_ENGINE_API_SDRECO_H */
