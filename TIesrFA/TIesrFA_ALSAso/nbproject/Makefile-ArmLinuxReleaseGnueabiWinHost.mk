.build-conf:
	@echo Tool collection not found.
	@echo Please specify existing tool collection in project properties
	@exit 1

# Clean Targets
.clean-conf:
	${RM} -r build/ArmLinuxReleaseGnueabiWinHost
	${RM} ../../Dist/ArmLinuxReleaseGnueabiWinHost/lib/libTIesrFA_ALSA.so.1

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
