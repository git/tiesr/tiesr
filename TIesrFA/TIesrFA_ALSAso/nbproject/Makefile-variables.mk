#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# LinuxDebugGnu configuration
CND_PLATFORM_LinuxDebugGnu=GNU-Linux-x86
CND_ARTIFACT_DIR_LinuxDebugGnu=../../Dist/LinuxDebugGnu/lib
CND_ARTIFACT_NAME_LinuxDebugGnu=libTIesrFA_ALSA.so.1
CND_ARTIFACT_PATH_LinuxDebugGnu=../../Dist/LinuxDebugGnu/lib/libTIesrFA_ALSA.so.1
CND_PACKAGE_DIR_LinuxDebugGnu=dist/LinuxDebugGnu/GNU-Linux-x86/package
CND_PACKAGE_NAME_LinuxDebugGnu=libTIesrFAALSAso.so.tar
CND_PACKAGE_PATH_LinuxDebugGnu=dist/LinuxDebugGnu/GNU-Linux-x86/package/libTIesrFAALSAso.so.tar
# LinuxReleaseGnu configuration
CND_PLATFORM_LinuxReleaseGnu=GNU-Linux-x86
CND_ARTIFACT_DIR_LinuxReleaseGnu=../../Dist/LinuxReleaseGnu/lib
CND_ARTIFACT_NAME_LinuxReleaseGnu=libTIesrFA_ALSA.so.1
CND_ARTIFACT_PATH_LinuxReleaseGnu=../../Dist/LinuxReleaseGnu/lib/libTIesrFA_ALSA.so.1
CND_PACKAGE_DIR_LinuxReleaseGnu=dist/LinuxReleaseGnu/GNU-Linux-x86/package
CND_PACKAGE_NAME_LinuxReleaseGnu=libTIesrFAALSAso.so.tar
CND_PACKAGE_PATH_LinuxReleaseGnu=dist/LinuxReleaseGnu/GNU-Linux-x86/package/libTIesrFAALSAso.so.tar
# ArmLinuxDebugGnueabi configuration
CND_PLATFORM_ArmLinuxDebugGnueabi=arm-none-linux-gnueabi-Linux-x86
CND_ARTIFACT_DIR_ArmLinuxDebugGnueabi=../../Dist/ArmLinuxDebugGnueabi/lib
CND_ARTIFACT_NAME_ArmLinuxDebugGnueabi=libTIesrFA_ALSA.so.1
CND_ARTIFACT_PATH_ArmLinuxDebugGnueabi=../../Dist/ArmLinuxDebugGnueabi/lib/libTIesrFA_ALSA.so.1
CND_PACKAGE_DIR_ArmLinuxDebugGnueabi=dist/ArmLinuxDebugGnueabi/arm-none-linux-gnueabi-Linux-x86/package
CND_PACKAGE_NAME_ArmLinuxDebugGnueabi=libTIesrFAALSAso.so.tar
CND_PACKAGE_PATH_ArmLinuxDebugGnueabi=dist/ArmLinuxDebugGnueabi/arm-none-linux-gnueabi-Linux-x86/package/libTIesrFAALSAso.so.tar
# ArmLinuxReleaseGnueabi configuration
CND_PLATFORM_ArmLinuxReleaseGnueabi=arm-none-linux-gnueabi-Linux-x86
CND_ARTIFACT_DIR_ArmLinuxReleaseGnueabi=../../Dist/ArmLinuxReleaseGnueabi/lib
CND_ARTIFACT_NAME_ArmLinuxReleaseGnueabi=libTIesrFA_ALSA.so.1
CND_ARTIFACT_PATH_ArmLinuxReleaseGnueabi=../../Dist/ArmLinuxReleaseGnueabi/lib/libTIesrFA_ALSA.so.1
CND_PACKAGE_DIR_ArmLinuxReleaseGnueabi=dist/ArmLinuxReleaseGnueabi/arm-none-linux-gnueabi-Linux-x86/package
CND_PACKAGE_NAME_ArmLinuxReleaseGnueabi=libTIesrFAALSAso.so.tar
CND_PACKAGE_PATH_ArmLinuxReleaseGnueabi=dist/ArmLinuxReleaseGnueabi/arm-none-linux-gnueabi-Linux-x86/package/libTIesrFAALSAso.so.tar
# ArmLinuxDebugGnueabiWinHost configuration
CND_PLATFORM_ArmLinuxDebugGnueabiWinHost=
CND_ARTIFACT_DIR_ArmLinuxDebugGnueabiWinHost=../../Dist/ArmLinuxDebugGnueabiWinHost/lib
CND_ARTIFACT_NAME_ArmLinuxDebugGnueabiWinHost=libTIesrFA_ALSA.so.1
CND_ARTIFACT_PATH_ArmLinuxDebugGnueabiWinHost=../../Dist/ArmLinuxDebugGnueabiWinHost/lib/libTIesrFA_ALSA.so.1
CND_PACKAGE_DIR_ArmLinuxDebugGnueabiWinHost=dist/ArmLinuxDebugGnueabiWinHost//package
CND_PACKAGE_NAME_ArmLinuxDebugGnueabiWinHost=libTIesrFAALSAso.so.tar
CND_PACKAGE_PATH_ArmLinuxDebugGnueabiWinHost=dist/ArmLinuxDebugGnueabiWinHost//package/libTIesrFAALSAso.so.tar
# ArmLinuxReleaseGnueabiWinHost configuration
CND_PLATFORM_ArmLinuxReleaseGnueabiWinHost=
CND_ARTIFACT_DIR_ArmLinuxReleaseGnueabiWinHost=../../Dist/ArmLinuxReleaseGnueabiWinHost/lib
CND_ARTIFACT_NAME_ArmLinuxReleaseGnueabiWinHost=libTIesrFA_ALSA.so.1
CND_ARTIFACT_PATH_ArmLinuxReleaseGnueabiWinHost=../../Dist/ArmLinuxReleaseGnueabiWinHost/lib/libTIesrFA_ALSA.so.1
CND_PACKAGE_DIR_ArmLinuxReleaseGnueabiWinHost=dist/ArmLinuxReleaseGnueabiWinHost//package
CND_PACKAGE_NAME_ArmLinuxReleaseGnueabiWinHost=libTIesrFAALSAso.so.tar
CND_PACKAGE_PATH_ArmLinuxReleaseGnueabiWinHost=dist/ArmLinuxReleaseGnueabiWinHost//package/libTIesrFAALSAso.so.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
