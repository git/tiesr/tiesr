#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# WindowsDebugMinGW configuration
CND_PLATFORM_WindowsDebugMinGW=Cygwin-Windows
CND_ARTIFACT_DIR_WindowsDebugMinGW=../../Dist/WindowsDebugMinGW
CND_ARTIFACT_NAME_WindowsDebugMinGW=testtiesrfa.exe
CND_ARTIFACT_PATH_WindowsDebugMinGW=../../Dist/WindowsDebugMinGW/testtiesrfa.exe
CND_PACKAGE_DIR_WindowsDebugMinGW=dist/WindowsDebugMinGW/Cygwin-Windows/package
CND_PACKAGE_NAME_WindowsDebugMinGW=testtiesrfa.tar
CND_PACKAGE_PATH_WindowsDebugMinGW=dist/WindowsDebugMinGW/Cygwin-Windows/package/testtiesrfa.tar
# WindowsReleaseMinGW configuration
CND_PLATFORM_WindowsReleaseMinGW=Cygwin-Windows
CND_ARTIFACT_DIR_WindowsReleaseMinGW=../../Dist/WindowsReleaseMinGW
CND_ARTIFACT_NAME_WindowsReleaseMinGW=testtiesrfa.exe
CND_ARTIFACT_PATH_WindowsReleaseMinGW=../../Dist/WindowsReleaseMinGW/testtiesrfa.exe
CND_PACKAGE_DIR_WindowsReleaseMinGW=dist/WindowsReleaseMinGW/Cygwin-Windows/package
CND_PACKAGE_NAME_WindowsReleaseMinGW=testtiesrfa.tar
CND_PACKAGE_PATH_WindowsReleaseMinGW=dist/WindowsReleaseMinGW/Cygwin-Windows/package/testtiesrfa.tar
# LinuxDebugGnu configuration
CND_PLATFORM_LinuxDebugGnu=GNU_current-Linux-x86
CND_ARTIFACT_DIR_LinuxDebugGnu=../../Dist/LinuxDebugGnu/bin
CND_ARTIFACT_NAME_LinuxDebugGnu=testtiesrfa
CND_ARTIFACT_PATH_LinuxDebugGnu=../../Dist/LinuxDebugGnu/bin/testtiesrfa
CND_PACKAGE_DIR_LinuxDebugGnu=dist/LinuxDebugGnu/GNU_current-Linux-x86/package
CND_PACKAGE_NAME_LinuxDebugGnu=testtiesrfa.tar
CND_PACKAGE_PATH_LinuxDebugGnu=dist/LinuxDebugGnu/GNU_current-Linux-x86/package/testtiesrfa.tar
# LinuxReleaseGnu configuration
CND_PLATFORM_LinuxReleaseGnu=GNU_current-Linux-x86
CND_ARTIFACT_DIR_LinuxReleaseGnu=../../Dist/LinuxReleaseGnu/bin
CND_ARTIFACT_NAME_LinuxReleaseGnu=testtiesrfa
CND_ARTIFACT_PATH_LinuxReleaseGnu=../../Dist/LinuxReleaseGnu/bin/testtiesrfa
CND_PACKAGE_DIR_LinuxReleaseGnu=dist/LinuxReleaseGnu/GNU_current-Linux-x86/package
CND_PACKAGE_NAME_LinuxReleaseGnu=testtiesrfa.tar
CND_PACKAGE_PATH_LinuxReleaseGnu=dist/LinuxReleaseGnu/GNU_current-Linux-x86/package/testtiesrfa.tar
# ArmLinuxDebugGnueabi configuration
CND_PLATFORM_ArmLinuxDebugGnueabi=arm-none-linux-gnueabi-Linux-x86
CND_ARTIFACT_DIR_ArmLinuxDebugGnueabi=../../Dist/ArmLinuxDebugGnueabi/bin
CND_ARTIFACT_NAME_ArmLinuxDebugGnueabi=testtiesrfa
CND_ARTIFACT_PATH_ArmLinuxDebugGnueabi=../../Dist/ArmLinuxDebugGnueabi/bin/testtiesrfa
CND_PACKAGE_DIR_ArmLinuxDebugGnueabi=dist/ArmLinuxDebugGnueabi/arm-none-linux-gnueabi-Linux-x86/package
CND_PACKAGE_NAME_ArmLinuxDebugGnueabi=testtiesrfa.tar
CND_PACKAGE_PATH_ArmLinuxDebugGnueabi=dist/ArmLinuxDebugGnueabi/arm-none-linux-gnueabi-Linux-x86/package/testtiesrfa.tar
# ArmLinuxReleaseGnueabi configuration
CND_PLATFORM_ArmLinuxReleaseGnueabi=arm-none-linux-gnueabi-Linux-x86
CND_ARTIFACT_DIR_ArmLinuxReleaseGnueabi=../../Dist/ArmLinuxReleaseGnueabi/bin
CND_ARTIFACT_NAME_ArmLinuxReleaseGnueabi=testtiesrfa
CND_ARTIFACT_PATH_ArmLinuxReleaseGnueabi=../../Dist/ArmLinuxReleaseGnueabi/bin/testtiesrfa
CND_PACKAGE_DIR_ArmLinuxReleaseGnueabi=dist/ArmLinuxReleaseGnueabi/arm-none-linux-gnueabi-Linux-x86/package
CND_PACKAGE_NAME_ArmLinuxReleaseGnueabi=testtiesrfa.tar
CND_PACKAGE_PATH_ArmLinuxReleaseGnueabi=dist/ArmLinuxReleaseGnueabi/arm-none-linux-gnueabi-Linux-x86/package/testtiesrfa.tar
# ArmLinuxDebugGnueabiWinHost configuration
CND_PLATFORM_ArmLinuxDebugGnueabiWinHost=
CND_ARTIFACT_DIR_ArmLinuxDebugGnueabiWinHost=../../Dist/ArmLinuxDebugGnueabiWinHost/bin
CND_ARTIFACT_NAME_ArmLinuxDebugGnueabiWinHost=testtiesrfa
CND_ARTIFACT_PATH_ArmLinuxDebugGnueabiWinHost=../../Dist/ArmLinuxDebugGnueabiWinHost/bin/testtiesrfa
CND_PACKAGE_DIR_ArmLinuxDebugGnueabiWinHost=dist/ArmLinuxDebugGnueabiWinHost//package
CND_PACKAGE_NAME_ArmLinuxDebugGnueabiWinHost=testtiesrfa.tar
CND_PACKAGE_PATH_ArmLinuxDebugGnueabiWinHost=dist/ArmLinuxDebugGnueabiWinHost//package/testtiesrfa.tar
# ArmLinuxReleaseGnueabiWinHost configuration
CND_PLATFORM_ArmLinuxReleaseGnueabiWinHost=
CND_ARTIFACT_DIR_ArmLinuxReleaseGnueabiWinHost=../../Dist/ArmLinuxReleaseGnueabiWinHost/bin
CND_ARTIFACT_NAME_ArmLinuxReleaseGnueabiWinHost=testtiesrfa
CND_ARTIFACT_PATH_ArmLinuxReleaseGnueabiWinHost=../../Dist/ArmLinuxReleaseGnueabiWinHost/bin/testtiesrfa
CND_PACKAGE_DIR_ArmLinuxReleaseGnueabiWinHost=dist/ArmLinuxReleaseGnueabiWinHost//package
CND_PACKAGE_NAME_ArmLinuxReleaseGnueabiWinHost=testtiesrfa.tar
CND_PACKAGE_PATH_ArmLinuxReleaseGnueabiWinHost=dist/ArmLinuxReleaseGnueabiWinHost//package/testtiesrfa.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
