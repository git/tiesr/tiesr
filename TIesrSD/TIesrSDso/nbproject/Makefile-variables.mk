#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# LinuxDebugGnu configuration
CND_PLATFORM_LinuxDebugGnu=GNU-Linux-x86
CND_ARTIFACT_DIR_LinuxDebugGnu=../../Dist/LinuxDebugGnu/lib
CND_ARTIFACT_NAME_LinuxDebugGnu=libTIesrSD.so.1
CND_ARTIFACT_PATH_LinuxDebugGnu=../../Dist/LinuxDebugGnu/lib/libTIesrSD.so.1
CND_PACKAGE_DIR_LinuxDebugGnu=dist/LinuxDebugGnu/GNU-Linux-x86/package
CND_PACKAGE_NAME_LinuxDebugGnu=libTIesrSDso.so.tar
CND_PACKAGE_PATH_LinuxDebugGnu=dist/LinuxDebugGnu/GNU-Linux-x86/package/libTIesrSDso.so.tar
# LinuxReleaseGnu configuration
CND_PLATFORM_LinuxReleaseGnu=GNU-Linux-x86
CND_ARTIFACT_DIR_LinuxReleaseGnu=../../Dist/LinuxReleaseGnu/lib
CND_ARTIFACT_NAME_LinuxReleaseGnu=libTIesrSD.so.1
CND_ARTIFACT_PATH_LinuxReleaseGnu=../../Dist/LinuxReleaseGnu/lib/libTIesrSD.so.1
CND_PACKAGE_DIR_LinuxReleaseGnu=dist/LinuxReleaseGnu/GNU-Linux-x86/package
CND_PACKAGE_NAME_LinuxReleaseGnu=libTIesrSDso.so.tar
CND_PACKAGE_PATH_LinuxReleaseGnu=dist/LinuxReleaseGnu/GNU-Linux-x86/package/libTIesrSDso.so.tar
# ArmLinuxDebugGnueabi configuration
CND_PLATFORM_ArmLinuxDebugGnueabi=arm-none-linux-gnueabi-Linux-x86
CND_ARTIFACT_DIR_ArmLinuxDebugGnueabi=../../Dist/ArmLinuxDebugGnueabi/lib
CND_ARTIFACT_NAME_ArmLinuxDebugGnueabi=libTIesrSD.so.1
CND_ARTIFACT_PATH_ArmLinuxDebugGnueabi=../../Dist/ArmLinuxDebugGnueabi/lib/libTIesrSD.so.1
CND_PACKAGE_DIR_ArmLinuxDebugGnueabi=dist/ArmLinuxDebugGnueabi/arm-none-linux-gnueabi-Linux-x86/package
CND_PACKAGE_NAME_ArmLinuxDebugGnueabi=libTIesrSDso.so.tar
CND_PACKAGE_PATH_ArmLinuxDebugGnueabi=dist/ArmLinuxDebugGnueabi/arm-none-linux-gnueabi-Linux-x86/package/libTIesrSDso.so.tar
# ArmLinuxReleaseGnueabi configuration
CND_PLATFORM_ArmLinuxReleaseGnueabi=arm-none-linux-gnueabi-Linux-x86
CND_ARTIFACT_DIR_ArmLinuxReleaseGnueabi=../../Dist/ArmLinuxReleaseGnueabi/lib
CND_ARTIFACT_NAME_ArmLinuxReleaseGnueabi=libTIesrSD.so.1
CND_ARTIFACT_PATH_ArmLinuxReleaseGnueabi=../../Dist/ArmLinuxReleaseGnueabi/lib/libTIesrSD.so.1
CND_PACKAGE_DIR_ArmLinuxReleaseGnueabi=dist/ArmLinuxReleaseGnueabi/arm-none-linux-gnueabi-Linux-x86/package
CND_PACKAGE_NAME_ArmLinuxReleaseGnueabi=libTIesrSDso.so.tar
CND_PACKAGE_PATH_ArmLinuxReleaseGnueabi=dist/ArmLinuxReleaseGnueabi/arm-none-linux-gnueabi-Linux-x86/package/libTIesrSDso.so.tar
# WindowsDebugMinGW configuration
CND_PLATFORM_WindowsDebugMinGW=Cygwin-Linux-x86
CND_ARTIFACT_DIR_WindowsDebugMinGW=../../Dist/WindowsDebugMinGW
CND_ARTIFACT_NAME_WindowsDebugMinGW=libTIesrSD.dll
CND_ARTIFACT_PATH_WindowsDebugMinGW=../../Dist/WindowsDebugMinGW/libTIesrSD.dll
CND_PACKAGE_DIR_WindowsDebugMinGW=dist/WindowsDebugMinGW/Cygwin-Linux-x86/package
CND_PACKAGE_NAME_WindowsDebugMinGW=libTIesrSDso.so.tar
CND_PACKAGE_PATH_WindowsDebugMinGW=dist/WindowsDebugMinGW/Cygwin-Linux-x86/package/libTIesrSDso.so.tar
# WindowsReleaseMinGW configuration
CND_PLATFORM_WindowsReleaseMinGW=Cygwin-Linux-x86
CND_ARTIFACT_DIR_WindowsReleaseMinGW=../../Dist/WindowsReleaseMinGW
CND_ARTIFACT_NAME_WindowsReleaseMinGW=libTIesrSD.dll
CND_ARTIFACT_PATH_WindowsReleaseMinGW=../../Dist/WindowsReleaseMinGW/libTIesrSD.dll
CND_PACKAGE_DIR_WindowsReleaseMinGW=dist/WindowsReleaseMinGW/Cygwin-Linux-x86/package
CND_PACKAGE_NAME_WindowsReleaseMinGW=libTIesrSDso.so.tar
CND_PACKAGE_PATH_WindowsReleaseMinGW=dist/WindowsReleaseMinGW/Cygwin-Linux-x86/package/libTIesrSDso.so.tar
