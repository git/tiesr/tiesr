/*=======================================================================
 *
 * TIesrSD.c
 *
 * This file implements the TIesrSD API.
 *
 * Copyright (C) 2001-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *

 This file contains the code necessary to implement the TIesrSD API
 as defined in TIesrSD_User.h, running the recognition as a
 separate thread.  TIesrSD utilizes the TIesrFA API for audio input
 and the TIesr Engine SD API for recognition.

 ======================================================================*/


/* Windows precompiled header must appear first. Can not be inside
 a precompiler #if..#endif directive.  This line must be commented
 out if not Windows OS */
/*
 #include "stdafx.h"
 */

#if defined(WIN32) || defined(WINCE)
//#include <commctrl.h>
#include <windows.h>
#endif

/* Standard C operations */
#include <stdlib.h>
#include <strings.h>

/* The TIesrSD API, including user and local hidden functionality */
#include "TIesrSD.h"


#if defined( WIN32 ) || defined( WINCE )
/* Define entry point for the TIesrSD API DLL */
BOOL APIENTRY DllMain( HANDLE hModule,
DWORD  ul_reason_for_call,
LPVOID lpReserved
)
{
   switch (ul_reason_for_call)
   {
      case DLL_PROCESS_ATTACH:
      case DLL_THREAD_ATTACH:
      case DLL_THREAD_DETACH:
      case DLL_PROCESS_DETACH:
         break;
   }
   return TRUE;
}
#endif



/*-----------------------------------------------------------------------
 TIesrSD_create

 This function creates and initializes an instance of the TIesrSD recognizer,
 which is encapsulated in the TIesrSD_Object.
 -----------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_create( TIesrSD_t* aPtrToTIesrSD )
{
   TIesrSD_t aTIesrSD;
   
   /* Allocate memory for the TIesrSD object */
   *aPtrToTIesrSD = (TIesrSD_Object_t*)malloc( sizeof(TIesrSD_Object_t) );
   if( *aPtrToTIesrSD == NULL )
   {
      return TIesrSDErrNoMemory;
   }
   else
   {
      aTIesrSD = *aPtrToTIesrSD;
   }
   
   #if defined(TIESRSD_DEBUG_LOG)
   aTIesrSD->threadfp = NULL;
   
   aTIesrSD->logfp = fopen(LOGFILE, "w" );
   if( aTIesrSD->logfp == NULL )
      return TIesrSDErrFail;
   
   fprintf( aTIesrSD->logfp, "SDCreate\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   
   /* heap allocated objects are initialized here to ensure no misuse. */
   aTIesrSD->grammardir = NULL;
   aTIesrSD->modeldir = NULL;
   aTIesrSD->modellist = NULL;
   aTIesrSD->nummodels = 0;
   aTIesrSD->srchmemory = NULL;
   aTIesrSD->framedata = NULL;
   aTIesrSD->devicename = NULL;
   
   /* Initially no callbacks defined */
   aTIesrSD->speakcb = NULL;
   aTIesrSD->donecb = NULL;
   aTIesrSD->cbdata = NULL;
   
   #if defined(WIN32) || defined(WINCE)
   /* Windows synchronization objects initialized to NULL for bookkeeping */
   aTIesrSD->threadid = NULL;
   aTIesrSD->recordmutex = NULL;
   aTIesrSD->startevent = NULL;
   #endif
   
   
   /* Recognizer created ok, now in closed state */
   aTIesrSD->state = TIesrSDStateClosed;
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDCreate success\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   return TIesrSDErrNone;
}



/*----------------------------------------------------------------
 TIesrSD_open

 This function opens the TIesrSD recognizer.  This causes parameters that
 are global for the entire recognition session to be initialized and
 stored in the object.  Arguments specify the TIesrSD instance, the audio
 device that will be used, a directory holding the base grammar for enrollment
 alignment, a directory that holds all enrolled models, the two callback
 functions and a pointer to return with the callback functions.
 ---------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_open( TIesrSD_t aTIesrSD,
const char* aAudioDevice,
const char* aGrammarDir,
const char* aModelDir,
TIesrSD_Callback_t speakCallback,
TIesrSD_Callback_t doneCallback,
void* const cbData )
{
   /* Recognizer must be in closed state */
   if( aTIesrSD->state != TIesrSDStateClosed )
   {
      return TIesrSDErrState;
   }
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDOpen\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   /* Allocate frame data memory */
   aTIesrSD->framedata = (short*)malloc( FRAME_LEN*sizeof(short) );
   if( aTIesrSD->framedata == NULL )
   {
      return TIesrSDErrNoMemory;
   }
   
   
   /* Save a copy of the audio device name */
   aTIesrSD->devicename = strdup( aAudioDevice );
   if( aTIesrSD->devicename == NULL )
   {
      TIesrSDL_freeopen( aTIesrSD );
      return TIesrSDErrNoMemory;
   }
   
   
   /* Save a copy of the grammar directory name */
   aTIesrSD->grammardir = strdup( aGrammarDir );
   if( aTIesrSD->grammardir == NULL )
   {
      TIesrSDL_freeopen( aTIesrSD );
      return TIesrSDErrNoMemory;
   }
   
   
   /* Save a copy of the model directory name */
   aTIesrSD->modeldir = strdup( aModelDir );
   if( aTIesrSD->modeldir == NULL )
   {
      TIesrSDL_freeopen( aTIesrSD );
      return TIesrSDErrNoMemory;
   }
   
   
   /* Set callback functions */
   aTIesrSD->speakcb = speakCallback;
   aTIesrSD->donecb = doneCallback;
   aTIesrSD->cbdata = cbData;
   
   
   /* Set status tracking variables */
   aTIesrSD->audiostatus = TIesrFAErrNone;
   aTIesrSD->engstatus = eTIesrEngineSuccess;
   
   
   /* No search memory allocated yet.  This is a subtask operation */
   aTIesrSD->srchmemory = NULL;
   
   
   /* Open the enrollment and recognition subtask engines.  This gets
    pointers to functions that each engine uses. */
   TIesrEngineSDENROOpen( &aTIesrSD->enroengine );
   TIesrEngineSDRECOOpen( &aTIesrSD->recoengine );
   
   
   /* Successfully opened the recognizer and audio channel */
   aTIesrSD->state = TIesrSDStateOpen;
   
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDOpen success\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   
   return TIesrFAErrNone;
}


/*----------------------------------------------------------------
 TIesrSDL_freeopen

 Local function to free objects allocated during TIesrSD_open.
 This is a convenience function that is used to manage variables
 allocated on the heap.
 -------------------------------------------------------------*/
TIesrSDL_Error_t TIesrSDL_freeopen( TIesrSD_t aTIesrSD )
{
   if(  aTIesrSD->devicename != NULL )
   {
      free( aTIesrSD->devicename);
      aTIesrSD->devicename = NULL;
   }
   
   if( aTIesrSD->srchmemory != NULL )
   {
      free( aTIesrSD->srchmemory );
      aTIesrSD->srchmemory = NULL;
   }
   
   if(  aTIesrSD->framedata != NULL )
   {
      free( aTIesrSD->framedata );
      aTIesrSD->framedata = NULL;
   }
   
   if(  aTIesrSD->grammardir != NULL )
   {
      free( aTIesrSD->grammardir );
      aTIesrSD->grammardir = NULL;
   }
   
   if(  aTIesrSD->modeldir != NULL )
   {
      free( aTIesrSD->modeldir );
      aTIesrSD->modeldir = NULL;
   }
   
   
   /* Set callbacks to not defined */
   aTIesrSD->speakcb = NULL;
   aTIesrSD->donecb = NULL;
   aTIesrSD->cbdata = NULL;
   
   
   return TIesrSDLErrNone;
}


/*----------------------------------------------------------------
 TIesrSD_enroll_open

 This function opens the enrollment subtask.  This opens the enrollment
 functions of the TIesr Engine API.

 ----------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_enroll_open( TIesrSD_t aTIesrSD,
const unsigned int aMemorySize,
const unsigned int aMaxFrames )
{
   /* Must be in SD open state to open the enrollment subtask */
   if( aTIesrSD->state != TIesrSDStateOpen )
   {
      return TIesrSDErrState;
   }
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDEnrollOpen\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   /* Allocate subtask memory */
   aTIesrSD->srchmemory = (short *)malloc( aMemorySize*sizeof(short) );
   if( aTIesrSD->srchmemory == NULL )
   {
      return TIesrSDErrNoMemory;
   }
   
   /* Set initial enrollment subtask substate variables */
   aTIesrSD->initialized = FALSE;
   aTIesrSD->complete = FALSE;
   aTIesrSD->uttcount = 0;
   
   
   /* Open the enrollment subtask */
   aTIesrSD->engstatus = (aTIesrSD->enroengine).Open( aTIesrSD->grammardir,
   (sd_enro_type_p)aTIesrSD->srchmemory,
   aMemorySize,
   aMaxFrames );
   if( aTIesrSD->engstatus != eTIesrEngineSuccess )
   {
      free( aTIesrSD->srchmemory);
      aTIesrSD->srchmemory = NULL;
      
      return TIesrSDErrReco;
   }
   
   
   /* Set default audio parameters */
   aTIesrSD->samplerate = SAMPLE_RATE;
   aTIesrSD->circularframes = CIRCULAR_FRAMES;
   aTIesrSD->audioframes = AUDIO_FRAMES;
   aTIesrSD->audiopriority = AUDIO_PRIORITY;
   aTIesrSD->audioreadrate =  AUDIO_READ_RATE;
   
   
   /* Initialize the audio channel */
   aTIesrSD->audiostatus = TIesrFA_init( &aTIesrSD->audiodevice,
   aTIesrSD->samplerate,
   TIesrFALinear,
   FRAME_LEN,
   aTIesrSD->circularframes,
   aTIesrSD->audioframes,
   aTIesrSD->audioreadrate,
   aTIesrSD->devicename,
   aTIesrSD->audiopriority );
   
   if( aTIesrSD->audiostatus != TIesrFAErrNone )
   {
      aTIesrSD->enroengine.Close( (sd_enro_type_p)aTIesrSD->srchmemory );
      free( aTIesrSD->srchmemory);
      aTIesrSD->srchmemory = NULL;
      
      return TIesrSDErrAudio;
   }
   
   aTIesrSD->state = TIesrSDStateEnroOpen;
   
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDEnrollOpen success\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   
   return TIesrSDErrNone;
}


/*----------------------------------------------------------------
 TIesrSD_enroll_init

 This function initializes the memory structures within the TIesr
 Engine to accept recording of the utterances that will be used for
 enrollment.  It also keeps track of enrollment initialization, and
 number of utterances enrolled correctly.  Upon calling this function,
 the enrollment session is marked as initialized, and the number of
 utterances recorded is set to zero.  Note that if at any time recording
 is stopped early by the application, then the initialization flag is
 set false.
 ----------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_enroll_init( TIesrSD_t aTIesrSD )
{
   if( aTIesrSD->state != TIesrSDStateEnroOpen )
   {
      return TIesrSDErrState;
   }
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDEnrollInit\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   /* Initialize the enrollment engine */
   (aTIesrSD->enroengine).Init( (sd_enro_type_p)aTIesrSD->srchmemory );
   
   /* Initialize subtask substates */
   aTIesrSD->initialized = TRUE;
   aTIesrSD->complete = FALSE;
   
   /* Set no utterances recorded for this enrollment */
   aTIesrSD->uttcount = 0;
   
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDEnrollInit success\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   return TIesrSDErrNone;
}


/*----------------------------------------------------------------
 TIesrSD_enroll_record

 This function starts recording an utterance in a separate thread.
 The recording only will start if the enrollment subtask is open,
 has been initialized, and not all utterances necessary for an
 enrollment have been collected.
 -----------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_enroll_record( TIesrSD_t aTIesrSD )
{
   TIesrSDL_Error_t result;
   
   /*
    Must be in enrollment subtask open state, and enrollment must be
    initialized prior to doing a recording.  Note that initialization acts
    as a substate of the TIesrSD enrollment subtask state.
    */
   if( aTIesrSD->state != TIesrSDStateEnroOpen ||
   ! aTIesrSD->initialized )
   {
      return TIesrSDErrState;
   }
   
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDEnrollRecord\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   
   if( aTIesrSD->uttcount >= TIESRSD_NUM_ENR_UTT )
   {
      return TIesrSDErrUtt;
   }
   
   
   /* Call helper function to start recording an utterance */
   result = TIesrSDL_start_record( aTIesrSD );
   
   if( result == TIesrSDLErrThread )
   {
      return TIesrSDErrThread;
   }
   else if( result != TIesrSDLErrNone )
   {
      return TIesrSDErrFail;
   }
   
   /* Recording started ok and is ongoing */
   aTIesrSD->state = TIesrSDStateEnroRecord;
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDEnrollRecord success\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   return TIesrSDErrNone;
}

/*----------------------------------------------------------------
 TIesrSD_enroll_stop

 This function terminates recording of an enrollment utterance. This
 function MUST be called ONCE for each successful call to
 TIesrSD_enroll_record. It may be called prior to the completion of
 recording in order to force the stopping of recording of an
 enrollment utterance.  However, since recording is asynchronous, the
 recording may complete normally prior to this function forcing the
 stopping of recording.  In any case, the user application is
 responsible for handling all calls of the callback functions
 correctly.  After this function is called and returns, it is
 guaranteed that the recording thread has terminated.
 ------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_enroll_stop( TIesrSD_t aTIesrSD )
{
   TIesrSDL_Error_t sdlError;
   
   
   /* Check state to ensure a recognizer thread is running */
   if( aTIesrSD->state != TIesrSDStateEnroRecord )
   {
      return TIesrSDErrState;
   }
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDEnrollStop\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   sdlError = TIesrSDL_stop_record( aTIesrSD );
   
   
   /* Recognizer has successfully stopped running */
   aTIesrSD->state = TIesrSDStateEnroOpen;
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDEnrollStop success\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   return TIesrSDErrNone;
}


/*----------------------------------------------------------------
 TIesrSD_enroll_model

 This function enrolls a model from the recorded utterances, and
 puts the resulting model in the file specified by the argument
 aFileName.  This file is located in the directory specified when
 TIesrSD was opened.
 ----------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_enroll_model( TIesrSD_t aTIesrSD, const char* aFileName )
{
   int nameSize;
   char* fullName;
   
   
   if( aTIesrSD->state != TIesrSDStateEnroOpen ||
   ! aTIesrSD->initialized )
   {
      return TIesrSDErrState;
   }
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDEnrollModel\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   if( aTIesrSD->uttcount != TIESRSD_NUM_ENR_UTT )
   {
      return TIesrSDErrUtt;
   }
   
   
   /* Set status tracking variables */
   aTIesrSD->engstatus = eTIesrEngineSuccess;
   
   
   /* Determine full name of file to store enrollment data */
   nameSize = strlen( aTIesrSD->modeldir );
   nameSize += strlen( aFileName ) + 2;
   fullName = (char*)malloc( nameSize*sizeof(char) );
   if( fullName == NULL )
   {
      return TIesrSDErrNoMemory;
   }
   
   strcpy( fullName, aTIesrSD->modeldir );
   strcat( fullName, FILESEPCHR );
   strcat( fullName, aFileName );
   
   aTIesrSD->engstatus = (aTIesrSD->enroengine).Enroll( fullName,
   (sd_enro_type_p)aTIesrSD->srchmemory );
   
   if( aTIesrSD->engstatus != eTIesrEngineSuccess )
   {
      return TIesrSDErrFail;
   }
   
   /* Enrollment completed successfully. Set subtask state */
   aTIesrSD->initialized = FALSE;
   aTIesrSD->complete = TRUE;
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDEnrollModel success\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   return TIesrSDErrNone;
}


/*----------------------------------------------------------------
 TIesrSD_enroll_score

 This function returns the enrollment score after performing model
 enrollment.  This score provides a means to confirm that the
 enrollment model matches the two enrollment utterances.  The score
 is not too sensitive to differences in enrollment utterances, since
 BW training smooths the model over the two utterances.  However, it
 can detect catasrophic failures.
 --------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_enroll_score( TIesrSD_t aTIesrSD, short* aScore )
{
   if( aTIesrSD->state != TIesrSDStateEnroOpen ||
   ! aTIesrSD->complete )
   {
      return TIesrSDErrState;
   }
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDEnrollScore\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   *aScore = (aTIesrSD->enroengine).GetScore( (sd_enro_type_p)aTIesrSD->srchmemory );
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDEnrollScore success\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   return TIesrSDErrNone;
}


/*----------------------------------------------------------------
 TIesrSD_enroll_close

 This function closes the enrollment subtask. If the user application was in
 the middle of creating an enrollment model, that model is not created, and
 all data is lost.  All resources allocated for enrollment are freed.
 TIesrSD to subsequently open the recognition subtask, or to close.
 ----------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_enroll_close( TIesrSD_t aTIesrSD )
{
   if( aTIesrSD->state != TIesrSDStateEnroOpen )
   {
      return TIesrSDErrState;
   }
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDEnrollClose\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   /* Close the enrollment engine instance */
   aTIesrSD->enroengine.Close( (sd_enro_type_p)aTIesrSD->srchmemory );
   
   /* Destroy the audio device instance */
   aTIesrSD->audiostatus = TIesrFA_destroy( &aTIesrSD->audiodevice );
   
   
   /* Reset all substate flags and variables */
   aTIesrSD->initialized = FALSE;
   aTIesrSD->complete = FALSE;
   aTIesrSD->uttcount = 0;
   
   /* Close all resources that were allocated by opening the
    enrollment subtask. */
   free( aTIesrSD->srchmemory );
   aTIesrSD->srchmemory = NULL;
   
   
   /* Closed enrollment subtask, now TIesrSD is open for other things. */
   aTIesrSD->state = TIesrSDStateOpen;
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDEnrollClose success\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   return TIesrSDErrNone;
}


/*----------------------------------------------------------------
 TIesrSD_reco_open

 This function opens the TIesrSD recognition subtask.  As part of the
 recognition subtask, it creates a list of all of the model files in
 the model directory. It allocates the memory for the recognition task.
 It initializes the audio channel object.
 ------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_reco_open( TIesrSD_t aTIesrSD,
const unsigned int aMemorySize,
const unsigned int aMaxFrames )
{
   TIesrSDL_Error_t sdlError;
   
   
   /* Must be in SD open state to open the recognition subtask */
   if( aTIesrSD->state != TIesrSDStateOpen )
   {
      return TIesrSDErrState;
   }
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDRecoOpen\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   /* Create a list of all of the model files in the model directory */
   sdlError = TIesrSDL_reco_list( aTIesrSD );
   if( sdlError != TIesrSDLErrNone )
   {
      return TIesrSDErrFail;
   }
   
   
   /* Allocate subtask memory */
   aTIesrSD->srchmemory = (short *)malloc( aMemorySize*sizeof(short) );
   if( aTIesrSD->srchmemory == NULL )
   {
      TIesrSDL_free_list( aTIesrSD );
      return TIesrSDErrNoMemory;
   }
   
   
   /* Set initial recognition subtask substate variables */
   aTIesrSD->initialized = FALSE;
   aTIesrSD->complete = FALSE;
   aTIesrSD->uttcount = 0;
   
   
   /* Open the recognition subtask */
   aTIesrSD->engstatus = (aTIesrSD->recoengine).Open( (sd_reco_type_p)aTIesrSD->srchmemory,
   aMemorySize,
   aMaxFrames );
   if( aTIesrSD->engstatus != eTIesrEngineSuccess )
   {
      TIesrSDL_free_list( aTIesrSD );
      free( aTIesrSD->srchmemory);
      aTIesrSD->srchmemory = NULL;
      
      return TIesrSDErrReco;
   }
   
   
   /* Create background model */
   aTIesrSD->engstatus =
   aTIesrSD->recoengine.LoadBackground( aTIesrSD->nummodels,
   aTIesrSD->modeldir,
   aTIesrSD->modellist,
   (sd_reco_type_p)aTIesrSD->srchmemory );
   
   if( aTIesrSD->engstatus != eTIesrEngineSuccess )
   {
      TIesrSDL_free_list( aTIesrSD );
      aTIesrSD->recoengine.Close( (sd_reco_type_p)aTIesrSD->srchmemory );
      free( aTIesrSD->srchmemory);
      aTIesrSD->srchmemory = NULL;
      
      return TIesrSDErrReco;
   }
   
   
   /* Set default audio parameters */
   aTIesrSD->samplerate = SAMPLE_RATE;
   aTIesrSD->circularframes = CIRCULAR_FRAMES;
   aTIesrSD->audioframes = AUDIO_FRAMES;
   aTIesrSD->audiopriority = AUDIO_PRIORITY;
   aTIesrSD->audioreadrate =  AUDIO_READ_RATE;
   
   
   /* Initialize the audio channel */
   aTIesrSD->audiostatus = TIesrFA_init( &aTIesrSD->audiodevice,
   aTIesrSD->samplerate,
   TIesrFALinear,
   FRAME_LEN,
   aTIesrSD->circularframes,
   aTIesrSD->audioframes,
   aTIesrSD->audioreadrate,
   aTIesrSD->devicename,
   aTIesrSD->audiopriority );
   
   if( aTIesrSD->audiostatus != TIesrFAErrNone )
   {
      TIesrSDL_free_list( aTIesrSD );
      aTIesrSD->recoengine.Close( (sd_reco_type_p)aTIesrSD->srchmemory );
      free( aTIesrSD->srchmemory);
      aTIesrSD->srchmemory = NULL;
      
      return TIesrSDErrAudio;
   }
   
   
   aTIesrSD->state = TIesrSDStateRecoOpen;
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDRecoOpen success\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   
   return TIesrSDErrNone;
}

/*----------------------------------------------------------------
 TIesrSD_reco_init

 Once the recognition subtask is opened, then multiple recognition of
 items can take place.  Each recognition consists of initialization,
 recording an utterance, and scoring the utterance with all models in
 the model directory.  At any time, the procedure can be restarted by
 initializing again.  The TIesrSD_reco_init function performs
 initialization.
 --------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_reco_init( TIesrSD_t aTIesrSD )
{
   if( aTIesrSD->state != TIesrSDStateRecoOpen )
   {
      return TIesrSDErrState;
   }
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDRecoInit\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   /* Initialize the recognition engine */
   (aTIesrSD->recoengine).Init( (sd_reco_type_p)aTIesrSD->srchmemory );
   
   /* Initialize subtask substates */
   aTIesrSD->initialized = TRUE;
   aTIesrSD->complete = FALSE;
   
   /* Set no utterances recorded for this recognition */
   aTIesrSD->uttcount = 0;
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDRecoInit success\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   
   return TIesrSDErrNone;
}

/*----------------------------------------------------------------
 TIesrSD_reco_record

 Record a recognition utterance.  Callback functions will be called within
 the recording thread to indicate when it is ok for the user to speak, and
 when the recording has completed.  See TIesrSD_open.
 ----------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_reco_record( TIesrSD_t aTIesrSD )
{
   TIesrSDL_Error_t result;
   
   /*
    Must be in recognition subtask open state, and recognition must be
    initialized prior to doing a recording.  Note that initialization acts
    as a substate of the TIesrSD recognition subtask state.
    */
   if( aTIesrSD->state != TIesrSDStateRecoOpen ||
   ! aTIesrSD->initialized )
   {
      return TIesrSDErrState;
   }
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDRecoRecord\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   /* If utterance already recorded, can not record another */
   if( aTIesrSD->uttcount > 0 )
   {
      return TIesrSDErrUtt;
   }
   
   
   /* Call helper function to start recording an utterance */
   result = TIesrSDL_start_record( aTIesrSD );
   
   if( result == TIesrSDLErrThread )
   {
      return TIesrSDErrThread;
   }
   else if( result != TIesrSDLErrNone )
   {
      return TIesrSDErrFail;
   }
   
   /* Recording started ok and is ongoing */
   aTIesrSD->state = TIesrSDStateRecoRecord;
   
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDRecoRecord success\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   return TIesrSDErrNone;
}


/*----------------------------------------------------------------
 TIesrSD_reco_stop

 The TIesrSD_reco_stop function must be called once for each successful
 call to TIesrSD_reco_record. It may be called prior to the completion of
 recording in order to force the stopping of recording the recognition
 utterance.  Since recording is asynchronous, the recording may complete
 normally prior to this function completing.  The user application is
 responsible for handling all callback functions correctly.  After this
 function is called and returns, it is guaranteed that the recording thread
 has terminated.  If the user application calls this function to stop
 recording prior to successful completion of a recording, then no further
 recognition can be done without first re-initializing, and starting the
 recording process over again.  See TIesrSD_reco_init above.
 ---------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_reco_stop( TIesrSD_t aTIesrSD )
{
   TIesrSDL_Error_t sdlError;
   
   /* Check state to ensure a recognizer thread is running */
   if( aTIesrSD->state != TIesrSDStateRecoRecord )
   {
      return TIesrSDErrState;
   }
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDRecoStop\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   sdlError = TIesrSDL_stop_record( aTIesrSD );
   
   /* Recognizer has stopped running */
   aTIesrSD->state = TIesrSDStateRecoOpen;
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDRecoStop success\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   return TIesrSDErrNone;
}


/*----------------------------------------------------------------
 Score the recorded utterance versus all of the models in the model directory
 specified in TIesrSD_Open.  Return the filename and score of the best
 scoring file.  This can only be called after the recognition task is opened,
 initialized and an utterance is recorded.  Note that this function returns
 the filename by copying into the parameter aBestFile, so the user must provide
 a character array big enough to hold the file name.  If there is a failure
 status returned, the returned file and score are undefined and
 must not be used.
 -----------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_reco_score( TIesrSD_t aTIesrSD,
char* aBestFile,
long* aBestScore )
{
   TIesrEngineStatusType engStatus;
   long score;
   long topScore = 0;
   int bestModel = 0;
   int model;
   int haveScore;
   
   /* Must be in proper state and substate */
   if( aTIesrSD->state != TIesrSDStateRecoOpen ||
   ! aTIesrSD->initialized )
   {
      return TIesrSDErrState;
   }
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDRecoScore\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   
   /* Must have recorded an utterance successfully */
   if( aTIesrSD->uttcount != 1 )
   {
      return TIesrSDErrUtt;
   }
   
   /* Score the collected utterance versus all models */
   haveScore = FALSE;
   for( model = 0; model < aTIesrSD->nummodels; model++ )
   {
      engStatus = aTIesrSD->recoengine.Score( aTIesrSD->modeldir,
      aTIesrSD->modellist[model],
      &score,
      (sd_reco_type_p)aTIesrSD->srchmemory );
      
      /* Recognition reported a valid score? */
      if( engStatus == eTIesrEngineSuccess )
      {
         if( !haveScore || score > topScore )
         {
            topScore = score;
            bestModel = model;
            haveScore = TRUE;
         }
      }
   }
   
   /* Output the best model and its score, if it exists */
   if( haveScore )
   {
      strcpy( aBestFile, aTIesrSD->modellist[bestModel] );
      *aBestScore = topScore;
   }
   
   /* Flag that a recognition is complete */
   aTIesrSD->complete = TRUE;
   aTIesrSD->initialized = FALSE;
   
   if( !haveScore )
      return TIesrSDErrFail;
   else
   {
      #ifdef TIESRSD_DEBUG_LOG
      fprintf( aTIesrSD->logfp, "SDRecoScore success\n" );
      fflush( aTIesrSD->logfp );
      #endif
      
      return TIesrSDErrNone;
   }
}

/*----------------------------------------------------------------
 TIesrSD_reco_confidence

 Get the confidence in the recognition result.  This compares the score
 of the best result to the background model score.
 ----------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_reco_confidence( TIesrSD_t aTIesrSD,
short* confScore )
{
   
   /* Must have done a successul recognition and scoring */
   if( aTIesrSD->state != TIesrSDStateRecoOpen ||
   ! aTIesrSD->complete )
   {
      return TIesrSDErrState;
   }
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDRecoConfidence\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   *confScore = aTIesrSD->recoengine.GetConfidence( WORDSPOT,
   (sd_reco_type_p)aTIesrSD->srchmemory );
   
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDRecoConfidence success\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   return TIesrSDErrNone;
}


/*----------------------------------------------------------------
 TIesrSD_reco_close

 Close the recognition subtask.  This will release all resources created when
 the subtask was opened.  This allows the TIesrSD instance to either open
 another recognition subtask, the enrollment subtask, or close the instance.
 ---------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_reco_close( TIesrSD_t aTIesrSD )
{
   TIesrSDL_Error_t sdlError;
   
   if( aTIesrSD->state != TIesrSDStateRecoOpen )
   {
      return TIesrSDErrState;
   }
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDRecoClose\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   /* Destroy the model list */
   sdlError = TIesrSDL_free_list( aTIesrSD );
   
   
   /* Close the enrollment engine instance */
   aTIesrSD->recoengine.Close( (sd_reco_type_p)aTIesrSD->srchmemory );
   
   /* Destroy the audio device instance */
   aTIesrSD->audiostatus = TIesrFA_destroy( &aTIesrSD->audiodevice );
   
   
   /* Reset all substate flags and variables */
   aTIesrSD->initialized = FALSE;
   aTIesrSD->complete = FALSE;
   aTIesrSD->uttcount = 0;
   
   /* Close all resources that were allocated by opening the
    enrollment subtask. */
   free( aTIesrSD->srchmemory );
   aTIesrSD->srchmemory = NULL;
   
   
   /* Closed enrollment subtask, now TIesrSD is open for other things. */
   aTIesrSD->state = TIesrSDStateOpen;
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDRecoClose success\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   return TIesrSDErrNone;
}


/*----------------------------------------------------------------
 TIesrSD_getparams

 Get the parameters that describe the settings that will be used with
 this instance of enrollment or recognition. This fills a
 TIesrSD_params_t structure with the present settings.
 ---------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_getparams( TIesrSD_t aTIesrSD,  TIesrSD_Params_t* aParams )
{
   gmhmm_type_p pGmmType;
   
   if( aTIesrSD->state != TIesrSDStateEnroOpen &&
   aTIesrSD->state != TIesrSDStateRecoOpen )
   {
      return TIesrSDErrState;
   }
   
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDGetParams\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   
   /* Audio device settings */
   aParams->sampleRate = aTIesrSD->samplerate;
   aParams->circularFrames = aTIesrSD->circularframes;
   aParams->audioFrames = aTIesrSD->audioframes;
   aParams->audioPriority = aTIesrSD->audiopriority;
   aParams->audioReadRate = aTIesrSD->audioreadrate;
   
   
   /* Settings used during enrollment */
   if( aTIesrSD->state == TIesrSDStateEnroOpen )
   {
      pGmmType = 
      (aTIesrSD->enroengine).GetRecordData( (sd_enro_type_p)aTIesrSD->srchmemory );
      
      /* pruneFactor ignored during enrollment */
      aParams->pruneFactor = 0;
      
      (aTIesrSD->enroengine).GetSADPrams( pGmmType,
      &aParams->sadDelta,
      &aParams->sadMinDb,
      &aParams->sadBeginFrames,
      &aParams->sadEndFrames,
      &aParams->sadNoiseFloor );
      
      (aTIesrSD->enroengine).GetVolumeRange( pGmmType,
      &aParams->lowVolume,
      &aParams->highVolume );
   }
   
   /* recognition settings */
   else
   {
      pGmmType =
      (aTIesrSD->recoengine).GetRecordData( (sd_reco_type_p)aTIesrSD->srchmemory );
      
      /* pruneFactor used during recognition */
      (aTIesrSD->recoengine).GetPrune( pGmmType,
      &aParams->pruneFactor );
      
      (aTIesrSD->recoengine).GetSADPrams( pGmmType,
      &aParams->sadDelta,
      &aParams->sadMinDb,
      &aParams->sadBeginFrames,
      &aParams->sadEndFrames,
      &aParams->sadNoiseFloor );
      
      (aTIesrSD->recoengine).GetVolumeRange( pGmmType,
      &aParams->lowVolume,
      &aParams->highVolume );
   }
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDGetParams success\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   return TIesrSDErrNone;
}


/*----------------------------------------------------------------
 TIesrSD_setparams

 Set the recognizer parameter settings that are to be used for enrollment
 or recognition from the parameter structure.
 ----------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_setparams( TIesrSD_t aTIesrSD,
const TIesrSD_Params_t* aParams )
{
   TIesrFA_Error_t faError;

   gmhmm_type_p pGmmType = NULL;
   
   
   if( aTIesrSD->state != TIesrSDStateEnroOpen &&
   aTIesrSD->state != TIesrSDStateRecoOpen )
   {
      return TIesrSDErrState;
   }
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDSetParams\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   
   if( aTIesrSD->state != TIesrSDStateEnroOpen &&
   aTIesrSD->state != TIesrSDStateRecoOpen )
   {
      return TIesrSDErrState;
   }
   

   /*
    Determine if the audio channel needs to be re-initialized because
    parameters last set have been changed.
    */
   if( aParams->sampleRate != aTIesrSD->samplerate ||
   aParams->circularFrames != aTIesrSD->circularframes ||
   aParams->audioFrames != aTIesrSD->audioframes ||
   aParams->audioPriority != aTIesrSD->audiopriority ||
   aParams->audioReadRate != aTIesrSD->audioreadrate )
   {
      faError = TIesrFA_destroy(  &aTIesrSD->audiodevice );
      if( faError != TIesrFAErrNone )
      {
         aTIesrSD->audiostatus = faError;
         return TIesrSDErrAudio;
      }
      
      aTIesrSD->samplerate = aParams->sampleRate;
      aTIesrSD->circularframes = aParams->circularFrames;
      aTIesrSD->audioframes = aParams->audioFrames;
      aTIesrSD->audiopriority = aParams->audioPriority;
      aTIesrSD->audioreadrate = aParams->audioReadRate;
      
      faError = TIesrFA_init( &aTIesrSD->audiodevice,
      aTIesrSD->samplerate,
      TIesrFALinear,
      FRAME_LEN,
      aTIesrSD->circularframes,
      aTIesrSD->audioframes,
      aTIesrSD->audioreadrate,
      aTIesrSD->devicename,
      aTIesrSD->audiopriority );
      
      if( faError != TIesrFAErrNone )
      {
         aTIesrSD->audiostatus = faError;
         return TIesrSDErrAudio;
      }
   }
   
   
   /* Change enrollment subtask parameters if in enrollment */
   if( aTIesrSD->state == TIesrSDStateEnroOpen )
   {
      pGmmType = 
      (aTIesrSD->enroengine).GetRecordData( (sd_enro_type_p)aTIesrSD->srchmemory );
      
      (aTIesrSD->enroengine).SetSADPrams( pGmmType,
      aParams->sadDelta,
      aParams->sadMinDb,
      aParams->sadBeginFrames,
      aParams->sadEndFrames,
      aParams->sadNoiseFloor );
      
      (aTIesrSD->enroengine).SetVolumeRange( pGmmType,
      aParams->lowVolume,
      aParams->highVolume );
   }
   
   /* In recognition subtask, so set recognition subtask parameters */
   else
   {
      pGmmType = 
      (aTIesrSD->recoengine).GetRecordData( (sd_reco_type_p)aTIesrSD->srchmemory );
      
      (aTIesrSD->recoengine).SetPrune( pGmmType,
      aParams->pruneFactor );
      
      (aTIesrSD->recoengine).SetSADPrams( pGmmType,
      aParams->sadDelta,
      aParams->sadMinDb,
      aParams->sadBeginFrames,
      aParams->sadEndFrames,
      aParams->sadNoiseFloor );
      
      (aTIesrSD->recoengine).SetVolumeRange( pGmmType,
      aParams->lowVolume,
      aParams->highVolume );
   }
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDSetParams success\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   return TIesrSDErrNone;
}


/*----------------------------------------------------------------
 TIesrSD_getframecount

 This function returns the number of frames recorded during the
 last successful utterance recording.
 -------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_getframecount( TIesrSD_t aTIesrSD,
short* aFrameCount )
{
   gmhmm_type_p pGmmType;
   
   if( aTIesrSD->state != TIesrSDStateEnroOpen &&
   aTIesrSD->state != TIesrSDStateRecoOpen )
   {
      return TIesrSDErrState;
   }
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDGetFrameCount\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   if( aTIesrSD->uttcount <= 0 )
   {
      return TIesrSDErrUtt;
   }
   
   if( aTIesrSD->state == TIesrSDStateEnroOpen )
   {
      pGmmType =
      (aTIesrSD->enroengine).GetRecordData( (sd_enro_type_p)aTIesrSD->srchmemory );
      
      *aFrameCount = (aTIesrSD->enroengine).GetFrameCount( pGmmType );
   }
   
   else
   {
      pGmmType =
      (aTIesrSD->recoengine).GetRecordData( (sd_reco_type_p)aTIesrSD->srchmemory );
      
      *aFrameCount = (aTIesrSD->recoengine).GetFrameCount( pGmmType );
   }
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDGetFrameCount success\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   return TIesrSDErrNone;
}

/*----------------------------------------------------------------
 TIesrSD_getvolume

 After recording an utterance, this function can be used to determine
 whether the speech recorded was spoken at a low or high volume, as
 determined by the threshold values set via TIesrSD_setparams.
 ----------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_getvolume( TIesrSD_t aTIesrSD, short* aVolumeLevel )
{
   gmhmm_type_p pGmmType;
   
   
   if( aTIesrSD->state != TIesrSDStateEnroOpen &&
   aTIesrSD->state != TIesrSDStateRecoOpen )
   {
      return TIesrSDErrState;
   }
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDGetVolume\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   if( aTIesrSD->uttcount <= 0 )
   {
      return TIesrSDErrUtt;
   }
   
   if( aTIesrSD->state == TIesrSDStateEnroOpen )
   {
      pGmmType =
      (aTIesrSD->enroengine).GetRecordData( (sd_enro_type_p)aTIesrSD->srchmemory );
      
      *aVolumeLevel = (aTIesrSD->enroengine).GetVolumeStatus( pGmmType );
   }
   
   else
   {
      pGmmType =
      ( aTIesrSD->recoengine).GetRecordData( (sd_reco_type_p)aTIesrSD->srchmemory );
      
      *aVolumeLevel = ( aTIesrSD->recoengine).GetVolumeStatus( pGmmType );
   }
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDGetVolume success\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   return TIesrSDErrNone;
}


/*----------------------------------------------------------------
 TIesrSD_status

 Get detailed status information for the result of the last operation.  This
 status consists of two unsigned integers. One specifies detailed engine
 status, and the other detailed status of audio collection.  Normally, this
 function is not called, but may be useful during debugging of an application
 to determine the cause of failures.  The values are not protected by any
 synchronization object and are set as errors occur.
 -------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_status( TIesrSD_t aTIesrSD,
int* aEngineStatus,
int* aAudioStatus )
{
   *aEngineStatus = aTIesrSD->engstatus;
   *aAudioStatus = aTIesrSD->audiostatus;
   return TIesrSDErrNone;
}


/*----------------------------------------------------------------
 TIesrSD_close

 Close down the TIesrSD recognizer.  This will deallocate any resources that
 were created when the recognizer was opened.  The recognizer can then be
 opened again, for example, with a new model directory for a different
 speaker dependent task.
 --------------------------------------------------------------------*/
TIesrSD_Error_t TIesrSD_close( TIesrSD_t aTIesrSD )
{
   
   if( aTIesrSD->state != TIesrSDStateOpen )
   {
      return TIesrSDErrState;
   }
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDClose\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   
   /* Free resources allocated during open */
   TIesrSDL_freeopen( aTIesrSD );
   
   aTIesrSD->engstatus = eTIesrEngineSuccess;
   aTIesrSD->audiostatus = TIesrFAErrNone;
   
   /* Close the SD enrollment and recognition engines */
   TIesrEngineSDENROClose( &aTIesrSD->enroengine );
   TIesrEngineSDRECOClose( &aTIesrSD->recoengine );
   
   /* State is now closed */
   aTIesrSD->state = TIesrSDStateClosed;
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDClose success\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   return TIesrSDErrNone;
}


/*----------------------------------------------------------------
 TIesrSD_destroy

 Destruct the instance of the TIesrSD recognizer.  This completely removes the
 TIesrSD object that was created with TIesrSD_create, and frees all resources
 that were allocated internally.  This must be called as the last part of
 using a recognizer instance to ensure that all allocated resources have
 been deallocated and no leakage occurs.
 ----------------------------------------------------------------------*/
TIESRSD_API
TIesrSD_Error_t TIesrSD_destroy( TIesrSD_t aTIesrSD )
{
   
   if( aTIesrSD->state != TIesrSDStateClosed )
   {
      return TIesrSDErrState;
   }
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->logfp, "SDDestroy\n" );
   fflush( aTIesrSD->logfp );
   #endif
   
   
   #ifdef TIESRSI_DEBUG_LOG
   if( aTIesrSD->logfp )
      fclose( aTIesrSD->logfp );
   
   if( aTIesrSD->threadfp )
      fclose( aTIesrSD->threadfp );
   #endif
   
   /* deallocate the TIesrSD instance */
   free( aTIesrSD );
   
   return TIesrSDErrNone;
}


/*----------------------------------------------------------------
 TIesrSDL_start_record

 This local helper function starts a recording.
 --------------------------------*/
TIesrSDL_Error_t TIesrSDL_start_record( TIesrSD_t aTIesrSD )
{
   TIesrSDL_Error_t sdlError;
   int result;

#if defined(LINUX)
   int threadresult;
   pthread_attr_t attr;
   struct sched_param sparam;
   TIesrSDL_Error_t threadError;
#endif
   void *error;
   int semstatus;
   
   
   /* Initialize the sync objects used with the recognizer */
   sdlError = TIesrSDL_initsync( aTIesrSD );
   if( sdlError != TIesrSDLErrNone )
   {
      return TIesrSDLErrFail;
   }
   
   
   /* Reset variables for the recording */
   aTIesrSD->startok = FALSE;
   aTIesrSD->stoprecord = FALSE;
   
 #if defined (LINUX)
   /* Set thread attributes; requested priority is the only changed attribute */
   result = pthread_attr_init(&attr);
   result = pthread_attr_getschedparam( &attr, &sparam );
   sparam.sched_priority = aTIesrSD->audiopriority;
   result = pthread_attr_setschedparam( &attr, &sparam );
   
   
   /* Start the recording thread */
   threadresult = pthread_create( &(aTIesrSD->threadid), &attr,
   TIesrSDL_thread, (void *) aTIesrSD );
   
   result = pthread_attr_destroy(&attr);
   
   /* Check if the thread was created ok */
   if( threadresult != 0 )
   {
      TIesrSDL_resetsync( aTIesrSD );
      return TIesrSDLErrThread;
   }

 #elif defined (WIN32) || defined (WINCE)
    aTIesrSD->threadid = CreateThread( NULL, 0,
           (LPTHREAD_START_ROUTINE)TIesrSDL_thread, aTIesrSD,
      CREATE_SUSPENDED, NULL );
   if( aTIesrSD->threadid == NULL )
   {
      TIesrSDL_resetsync( aTIesrSD );

      return TIesrSDLErrThread;
   }

   SetThreadPriority( aTIesrSD->threadid, aTIesrSD->audiopriority );

   ResumeThread( aTIesrSD->threadid );

 #endif


   /* The thread was created ok.  Wait for it to signal that it has started. */
#if defined (LINUX)
   do
   {
     semstatus = sem_wait( &(aTIesrSD->startsemaphore) );
   }
   while( semstatus && errno == EINTR );

#elif defined (WIN32) || defined (WINCE)
   WaitForSingleObject( aTIesrSD->startevent, INFINITE );
#endif
   
   /* If the recognition thread did not start ok, then wait for the thread to
    terminate, and then notify the application that it failed */
   if( ! aTIesrSD->startok )
   {
#if defined (LINUX)
      pthread_join( aTIesrSD->threadid, &error );
      threadError = (TIesrSD_Error_t)error;      

#elif defined (WIN32) || defined (WINCE)
      WaitForSingleObject( aTIesrSD->threadid, INFINITE );
      CloseHandle( aTIesrSD->threadid );
      aTIesrSD->threadid = NULL;
#endif
    
      TIesrSDL_resetsync( aTIesrSD );
      
      return TIesrSDLErrThread;
   }
   
   return TIesrSDLErrNone;
}


/*----------------------------------------------------------------
 TIesrSDL_stop_record

 This local helper function stops a recording, and ensures that
 the recording thread has terminated.
 --------------------------------*/
TIesrSDL_Error_t TIesrSDL_stop_record( TIesrSD_t aTIesrSD )
{

#if defined (LINUX)
   TIesrSDL_Error_t threadError;
#endif
   
   void *error;

   
   /* Set flag to force the recording thread to terminate, if that has
    not happened already due to normal end of utterance or failure.  */
   aTIesrSD->stoprecord = TRUE;
   
   
   /* Wait for the recording thread to stop */
#if defined (LINUX)
   pthread_join( aTIesrSD->threadid, &error );
   threadError = (TIesrSDL_Error_t)(error);

#elif defined (WIN32) || defined (WINCE)
   WaitForSingleObject( aTIesrSD->threadid, INFINITE );
   CloseHandle( aTIesrSD->threadid );
   aTIesrSD->threadid = NULL;
#endif

   /* Reset sync objects since they are no longer needed for the thread */
   TIesrSDL_resetsync( aTIesrSD );
   
   return TIesrSDLErrNone;
}


/*----------------------------------------------------------------
 TIesrSDL_initsync

 Initialize synchronization objects for the TIesrSD instance.
 --------------------------------*/
TIesrSDL_Error_t TIesrSDL_initsync( TIesrSD_t aTIesrSD )
{
   
   #if defined(LINUX)
   
   int error;
   
   error = pthread_mutex_init( &(aTIesrSD->recordmutex), NULL );
   if( error )
   {
      return TIesrSDLErrFail;
   }
   
   error = sem_init( &(aTIesrSD->startsemaphore), 0, 0 );
   if( error )
   {
      error = pthread_mutex_destroy( &(aTIesrSD->recordmutex) );
      
      return TIesrSDLErrFail;
   }
   
   
   #elif defined(WIN32) || defined(WINCE)
   
   aTIesrSD->recordmutex = CreateMutex( NULL, FALSE, NULL );
   if( aTIesrSD->recordmutex == NULL )
      return TIesrSDLErrFail;
   
   aTIesrSD->startevent = CreateEvent( NULL, FALSE, FALSE, NULL );
   if( aTIesrSD->startevent == NULL )
   {
      CloseHandle( aTIesrSD->recordmutex );
      aTIesrSD->recordmutex = NULL;
      return TIesrSDLErrFail;
   }
   
   #endif
   
   return TIesrSDLErrNone;
}


/*----------------------------------------------------------------
 TIesrSDL_resetsync

 Reset the synchronization objects for the TIesrSD instance.
 --------------------------------*/
TIesrSDL_Error_t TIesrSDL_resetsync( TIesrSD_t aTIesrSD )
{
   TIesrSDL_Error_t sdlError = TIesrSDLErrNone;
   
   #if defined(LINUX)
   
   int error;
   
   error = pthread_mutex_destroy( &(aTIesrSD->recordmutex) );
   if( error )
   {
      sdlError = TIesrSDLErrFail;
   }
   
   error = sem_destroy( &(aTIesrSD->startsemaphore) );
   if( error != 0 )
   {
      sdlError = TIesrSDLErrFail;
   }
   
   
   #elif defined(WIN32) || defined(WINCE)
   
   if( ! CloseHandle( aTIesrSD->recordmutex ) )
   {
      sdlError = TIesrSDLErrFail;
   }
   
   if( ! CloseHandle( aTIesrSD->startevent ) )
   {
      sdlError = TIesrSDLErrFail;
   }
   
   #endif
   
   return sdlError;
}


/*----------------------------------------------------------------
 TIesrSDL_thread

 This is the TIesrSD record thread function that is started by
 a call to TIesrSD_start_record.
 --------------------------------------------------------------*/
#if defined (LINUX)
void* TIesrSDL_thread( void* aArg )
#elif defined (WIN32) || defined (WINCE)
DWORD TIesrSDL_thread( LPVOID aArg )
#endif
{
   TIesrFA_Error_t faError;
   TIesrFA_Error_t faStopError;
   TIesrFA_Error_t faCloseError;
   
   TIesrEngineStatusType engStatus = eTIesrEngineSuccess;
   
   int stopRequested;
   
   unsigned int framesQueued;
   
   TIesrSD_Error_t sdError = TIesrSDErrNone;
   
   /* The thread argument is the TIesrSD instance */
   TIesrSD_t aTIesrSD = (TIesrSD_t)aArg;
   
   #ifdef TIESRSD_DEBUG_LOG
   int frame = 0;
   fprintf( aTIesrSD->threadfp, "SDThread\n" );
   fflush( aTIesrSD->threadfp );
   #endif
   
   
   /* Determine if this is enrollment or recognition record */
   int enroll = ( aTIesrSD->state == TIesrSDStateEnroOpen );
   
   /* The TIesrSD record data memory location */
   gmhmm_type_p pGmmType = enroll ?
   aTIesrSD->enroengine.GetRecordData( (sd_enro_type_p)aTIesrSD->srchmemory )
   : aTIesrSD->recoengine.GetRecordData( (sd_reco_type_p)aTIesrSD->srchmemory );
   
   
   /* Set thread state flags. Recording has not ended, and
    the start callback has not been called. */
   int recordEnded = FALSE;
   int startCalled = FALSE;
   
   /* Initialize frames received counter */
   int frameCount = 0;
   
   
   /* Initially set flag indicating recording did not start ok */
   aTIesrSD->startok = FALSE;
   
   
   /* Open an audio channel which connects to the audio device.  On failure
    inform the start function via the start semaphore and exit the thread. */
   faError = TIesrFA_open( &aTIesrSD->audiodevice );
   if( faError != TIesrFAErrNone )
   {
      aTIesrSD->audiostatus = faError;
      
      #if defined(LINUX)
      sem_post( &aTIesrSD->startsemaphore );
      return (void*)TIesrSDLErrAudio;
      
      #elif defined(WIN32) || defined(WINCE)
      SetEvent( aTIesrSD->startevent );
      return TIesrSDLErrAudio;
      
      #endif
   }
   
   
   /* Open the search engine, which prepares it for recognition. */
   enroll ? aTIesrSD->enroengine.RecordOpen( pGmmType )
   : aTIesrSD->recoengine.RecordOpen( pGmmType );
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->threadfp, "SDThread starting FA\n" );
   fflush( aTIesrSD->threadfp );
   #endif
    
   /* Start collection of audio samples into the circular frame buffer. On
    failure, close the audio channel, inform the start function via the
    start semaphore and exit the thread. */
   faError = TIesrFA_start( &aTIesrSD->audiodevice );
   if( faError != TIesrFAErrNone )
   {
      TIesrFA_close( &aTIesrSD->audiodevice );
      aTIesrSD->audiostatus = faError;
      
      aTIesrSD->engstatus = enroll ?
      aTIesrSD->enroengine.RecordClose( pGmmType )
      : aTIesrSD->recoengine.RecordClose( pGmmType );
      
      #if defined(LINUX)
      sem_post( &aTIesrSD->startsemaphore );
      return (void*)TIesrSDLErrAudio;
      
      #elif defined(WIN32) || defined(WINCE)
      SetEvent( aTIesrSD->startevent );
      return TIesrSDLErrAudio;
      
      #endif
   }
   
   
   /* Audio collection has started successfully, and the recognizer engine is
    opened succesfully.  Notify the start function that recognition started
    successfully. */
   aTIesrSD->startok = TRUE;
   
   #if defined(LINUX)
   sem_post( &aTIesrSD->startsemaphore );
   
   #elif defined (WIN32) || defined (WINCE)
   SetEvent( aTIesrSD->startevent );
   
   #endif
   
   #ifdef TIESRSI_DEBUG_LOG
   fprintf( aTIesrSD->threadfp, "SDThread starting loop\n" );
   fflush( aTIesrSD->threadfp );
   #endif
   
   /* Get the first frame of data, blocking until it is received */
   faError = TIesrFA_getframe( &aTIesrSD->audiodevice,
   (unsigned char*)aTIesrSD->framedata, BLOCKING, &framesQueued );
   
   
   /* Enter loop processing frames of data until recognition complete,
    a request to terminate recognition, or failure */
   while( faError == TIesrFAErrNone && ! aTIesrSD->stoprecord )
   {

      #if defined(TIESRSD_DEBUG_LOG)
      /* If debug logging, capture number of frames queued */
      if( aTIesrSD->threadfp )
      {
         fprintf( aTIesrSD->threadfp, "Queued: %d\n", framesQueued );
         fflush( aTIesrSD->threadfp );
      }
      #endif     
      
      
      #ifdef TIESRSI_DEBUG_LOG
      fprintf( aTIesrSD->threadfp, "SDThread calling engine %d\n", frame );
      fflush( aTIesrSD->threadfp );
      #endif
      
      engStatus = enroll ?
      aTIesrSD->enroengine.Record( pGmmType,  aTIesrSD->framedata, FALSE )
      : aTIesrSD->recoengine.Record( pGmmType,  aTIesrSD->framedata, TRUE );
      
      #ifdef TIESRSD_DEBUG_LOG
      fprintf( aTIesrSD->threadfp, "SDThread engine done %d %d\n", frame, engStatus );
      fflush( aTIesrSD->threadfp );
      #endif
      
     /* Processed another frame of data */
      frameCount++;
      
      
      /* Catch recording error and exit loop immediately */
      if( engStatus != eTIesrEngineSuccess )
      {
         break;
      }
      
      
      /* Catch end of utterance and exit loop immediately */
      recordEnded = enroll ?
      (aTIesrSD->enroengine).SpeechEnded( pGmmType )
      : (aTIesrSD->recoengine).SpeechEnded( pGmmType );
      if( recordEnded )
      {
         break;
      }
      
      
      /* Check if speak callback should be called. */
      if( !startCalled && aTIesrSD->speakcb != NULL )
      {
         /* Find if speech has started */
         int speechDetected = enroll ?
         (aTIesrSD->enroengine).SpeechDetected( pGmmType )
         : (aTIesrSD->recoengine).SpeechDetected( pGmmType );
         
         if( speechDetected || frameCount >= TIESRSD_START_FRAMES )
         {
            startCalled = TRUE;
            
            if( frameCount >= TIESRSD_START_FRAMES )
            {
               (aTIesrSD->speakcb)(aTIesrSD->cbdata, TIesrSDErrNone );
            }
            else
            {
               (aTIesrSD->speakcb)(aTIesrSD->cbdata, TIesrSDErrSpeechEarly );
            }
         }
      }
      
      
      /* Get next frame of data */
      faError = TIesrFA_getframe( &aTIesrSD->audiodevice,
      (unsigned char*)aTIesrSD->framedata, BLOCKING, &framesQueued );
   }
   
   #ifdef TIESRSI_DEBUG_LOG
   fprintf( aTIesrSD->threadfp, "SDThread exit loop\n" );
   fflush( aTIesrSD->threadfp );
   #endif   
   
   /* The loop has been exited. Stop data collection and disconnect from the
    channel.  Determine audio status at end of recognition */
   faStopError = TIesrFA_stop( &aTIesrSD->audiodevice );
   faCloseError = TIesrFA_close( &aTIesrSD->audiodevice );
   if( faError != TIesrFAErrNone )
      aTIesrSD->audiostatus = faError;
   else if( faStopError != TIesrFAErrNone )
      aTIesrSD->audiostatus = faStopError;
   else if( faCloseError != TIesrFAErrNone )
      aTIesrSD->audiostatus = faCloseError;
   else
      aTIesrSD->audiostatus = TIesrFAErrNone;
   
   
   /* Any failure to end recording successfully, or a user request to
    stop recording, results in a requirement to reinitialize data
    collection */
   stopRequested =  aTIesrSD->stoprecord;
   if( ! recordEnded || engStatus != eTIesrEngineSuccess || stopRequested )
   {
      aTIesrSD->initialized = FALSE;
   }
   else
   {
      aTIesrSD->uttcount++;
   }
   
   
   /* Close the recording */
   aTIesrSD->engstatus = enroll ?
   aTIesrSD->enroengine.RecordClose( pGmmType )
   : aTIesrSD->recoengine.RecordClose( pGmmType );
   
   
   /* Notify the application of recognition thread completion */
   if( aTIesrSD->donecb != NULL )
   {
      /* If processing in loop failed, then record error there
       takes precedence over record closing error */
      if( engStatus != eTIesrEngineSuccess )
         aTIesrSD->engstatus = engStatus;
      
      /* Determine if any audio or recording errors to report to app */
      if( aTIesrSD->audiostatus != TIesrFAErrNone &&
      aTIesrSD->engstatus != eTIesrEngineSuccess )
         sdError = TIesrSDErrBoth;
      else if( aTIesrSD->audiostatus != TIesrFAErrNone )
         sdError = TIesrSDErrAudio;
      else if( aTIesrSD->engstatus != eTIesrEngineSuccess )
         sdError = TIesrSDErrReco;
      else
         sdError = TIesrSDErrNone;
      
      /* Call the done callback function */
      (aTIesrSD->donecb)( aTIesrSD->cbdata, sdError );
   }
   
   #ifdef TIESRSD_DEBUG_LOG
   fprintf( aTIesrSD->threadfp, "SDThread ended\n" );
   fflush( aTIesrSD->threadfp );
   #endif   
    
   
   /* Terminate the thread after a recognition process. */
   #if defined(LINUX)
   if( sdError == TIesrSDErrNone )
      return (void*)TIesrSDLErrNone;
   else
      return (void*)TIesrSDLErrFail;
   
   #elif defined (WIN32) || defined (WINCE)
   if( sdError == TIesrSDErrNone )
      return TIesrSDLErrNone;
   else
      return TIesrSDLErrFail;
   #endif
}


/*----------------------------------------------------------------
 TIesrSDL_reco_list

 Create a list of all of the filenames in the model directory.  This
 is needed in order to perform recognition.
 --------------------------------*/
TIesrSDL_Error_t TIesrSDL_reco_list( TIesrSD_t aTIesrSD )
{
   DIR* mdlDIR;
   struct dirent* dirEntry;
   
   struct stat fileEntry;
   
   char fullFileName[PATH_MAX+1];
   int addSlash;
   int dirLength;
   char* fileName;
   int fileLength;
   
   int result;
   int model;
   
   int numModels = 0;
   char* modelDir = aTIesrSD->modeldir;
   
   /* Determine the model directory size and format */
   dirLength = strlen( modelDir );
   addSlash = ( modelDir[dirLength-1] != '/' );
   strcpy(fullFileName, modelDir);
   if( addSlash )
   {
      strcat( fullFileName, "/" );
      dirLength++;
   }
   
   
   
   /* Open the directory for reading files */
   mdlDIR = opendir( modelDir );
   if( mdlDIR == NULL )
   {
      return TIesrSDLErrFail;
   }
   
   /* Iterate through all entries in the directory */
   dirEntry = readdir( mdlDIR );
   while( dirEntry != NULL )
   {
      /* Full name of the entry */
      strcpy(fullFileName, modelDir);
      strcat(fullFileName+dirLength, dirEntry->d_name );
      
      /* If it is a regular file name, increment counter */
      result = stat( fullFileName, &fileEntry );
      if( result != 0 )
      {
         closedir( mdlDIR );
         return TIesrSDLErrFail;
      }
      else if( S_ISREG( fileEntry.st_mode ) )
      {
         numModels++;
      }
      
      /* Next directory entry */
      dirEntry = readdir( mdlDIR );
   }
   
   /* Abort if no model files */
   if( numModels <= 0 )
   {
      closedir( mdlDIR );
      return TIesrSDLErrNoModels;
   }
   
   
   /* Create array to hold filenames */
   aTIesrSD->modellist = (char**)malloc( numModels*sizeof(char*) );
   if( aTIesrSD->modellist == NULL )
   {
      closedir( mdlDIR );
      return TIesrSDLErrNoMemory;
   }
   
   for( model=0; model < numModels; model++ )
      aTIesrSD->modellist[model] = NULL;
   
   
   /* Put all model names into the list */
   rewinddir( mdlDIR );
   model = 0;
   dirEntry = readdir( mdlDIR );
   while( dirEntry != NULL && model < numModels )
   {
      fileName = dirEntry->d_name;
      fileLength = strlen( fileName );
      
      /* Full name of the entry */
      strcpy(fullFileName, modelDir);
      strcat(fullFileName+dirLength, fileName );
      
      /* Get an entry for the present file */
      result = stat( fullFileName, &fileEntry );
      if( result != 0 )
      {
         closedir( mdlDIR );
         TIesrSDL_free_list( aTIesrSD );
         return TIesrSDLErrFail;
      }
      
      /* Check if the entry is a regular file, and add to list if so. */
      if( S_ISREG( fileEntry.st_mode ) )
      {
         
         /*aTIesrSD->modellist[model] = (char*)malloc( (fileLength+1)*sizeof(char) ); */
         aTIesrSD->modellist[model] = strdup( fileName );
         if( aTIesrSD->modellist[model] == NULL )
         {
            closedir( mdlDIR );
            TIesrSDL_free_list( aTIesrSD );
            return TIesrSDLErrNoMemory;
         }
         
         /* Keep track of total number of models loaded */
         model++;
         aTIesrSD->nummodels = model;
      }
      
      /* Next directory entry to process */
      dirEntry = readdir( mdlDIR );
   }
   
   
   closedir( mdlDIR );
   
   return TIesrSDLErrNone;
}


/*----------------------------------------------------------------
 TIesrSDL_free_list

 This helper function frees all of the models in the model list,
 and the model list itself.
 --------------------------------*/
TIesrSDL_Error_t TIesrSDL_free_list( TIesrSD_t aTIesrSD )
{
   if( aTIesrSD->modellist != NULL )
   {
      int model;
      for( model=0; model < aTIesrSD->nummodels; model++ )
      {
         free( aTIesrSD->modellist[model] );
      }
      
      aTIesrSD->nummodels = 0;
      
      free( aTIesrSD->modellist );
      aTIesrSD->modellist = NULL;
   }
   
   return TIesrSDLErrNone;
}
