/*==============================================================

 *
 * TestTIesrSD.c
 *
 * This file implements a test of the TIesrSD API.
 *
 * Copyright (C) 2006-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *

 This program implements an interactive test of the TIesrSD API that
 demonstrates use of the TIesrSD API functionality. Single
 character commands are entered on the console to enroll or recognize a name.
 The demo is multi-threaded, so that an enrollment or recognition can be
 aborted.  The program will look for the enrollment models and network iun
 in the directory "grammar", and will place speaker-dependent template models
 in the "models" directory.
 
 The console commands recognized are:

 e = enroll a new speaker-dependent name
 r = recognize an enrolled name
 a = abort a recognition or enrollment
 q = quit the test program

 The command line is:

 TestTIesrSD EnroMemSize RecoMemSize MaxFrames AudioDevice

 EnroMemSize - Enrollment memory to allocate in shorts (try 30000)
 RecoMemSize - Recognition memory to allocate in shorts (try 30000)
 MaxFrames - Maximum frames to record
 AudioDevice - TIesrFA audio device name, or a file name

 ================================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <semaphore.h>
#include <string.h>
#include <errno.h>

#include "TestTIesrSD.h"

/* Global parameters for the demo */

/* App State information */
AppState_t appState;

/* Enrollment State */
EnroState_t enroState;

/* RecognitionState */
RecoState_t recoState;

/* General counter for number utterances recorded */
int numRecorded;

/* Enrollment name */
char enrollName[256];

/* Parameters for TIesrSD */
int enroMemorySize;
int recoMemorySize;
int maxFrames;


/*----------------------------------------------------------------*/

int main( int argc, char** argv )
{
   /* The keyboard thread */
   pthread_t kbdThread;
   int thdResult;
   int thdStatus;

   /* The process que */
   ProcessQue_t processQue;
   
   /* The recognizer */
   TIesrSD_t tiesrSD;
   TIesrSD_Error_t sdError;
   char* audioDevice;


   /* Initialization */
   if( argc != 5 )
   {
      return 1;
   }

   enroMemorySize = atoi( argv[1] );
   recoMemorySize = atoi( argv[2] );
   maxFrames = atoi( argv[3] );
   audioDevice = argv[4];

   /* Initialize the que to hold pending commands */
   InitializeQue( &processQue );

   /* Create a TIesrSD recognizer instance */
   sdError = TIesrSD_create( &tiesrSD );
   if( sdError != TIesrSDErrNone )
   {
      return sdError;
   }

   /* Open the recognizer with default parameters */
   sdError = TIesrSD_open( tiesrSD, 
			   audioDevice, 
			   GRAMMARDIR, 
			   MODELDIR,
			   SpeakCallback,
			   DoneCallback,
			   (void*)&processQue );
   if( sdError != TIesrSDErrNone )
   {
      return sdError;
   }

   appState = APPOPEN;
   enroState = ENROCLOSED;
   recoState = RECOCLOSED;
   numRecorded = 0;


   /* Start keyboard capture thread for the keyboard commands */
   thdResult = pthread_create( &kbdThread, NULL, KbdThreadFcn, &processQue );
   if( thdResult != 0 )
   {
      TIesrSD_close( tiesrSD );
      TIesrSD_destroy( tiesrSD );
      return 999;
   }


   /* Run the process Que.  This will run until the user presses 'q' */
   RunQueLoop( &processQue, tiesrSD );


   /* Que has been exited. Close recognizer */
   sdError = TIesrSD_close( tiesrSD );
   if( sdError != TIesrSDErrNone )
   {
      printf( "Failed to close recognizer\n" );
   }

   sdError = TIesrSD_destroy( tiesrSD );
   if( sdError != TIesrSDErrNone )
   {
      printf( "Failed to destroy recognizer\n" );
   }

   pthread_join( kbdThread, (void**)&thdStatus );

   FlushQue( &processQue );

   return 0;
}


/*----------------------------------------------------------------*/
void RunQueLoop( ProcessQue_t* aProcessQue, TIesrSD_t tiesrSD )
{
   EventCode_t eventCode;
   int eventStatus;
   int semstatus;

   int error;

   int exitLoop = FALSE;


   while( !exitLoop )
   {
      /* Wait for an event in the que */
      do
      {
        semstatus = sem_wait( &aProcessQue->queSemaphore );
      }
      while( semstatus && errno == EINTR );
      
      GetEventFromQue( aProcessQue, &eventCode, &eventStatus );

      switch( eventCode )
      {
	 case ENROLL:
	 {
	    if( appState == APPENROLLING )
	    {
	       /* Start enrolling all over again */
	       error = StopEnrolling( tiesrSD );
	       error = StartEnrolling( tiesrSD );
	    }
	    else if( appState == APPRECOGNIZING )
	    {
	       /* Stop recognizing and start enrolling */
	       error = StopRecognizing( tiesrSD );
	       error = StartEnrolling( tiesrSD );
	    }
	    else if( appState == APPOPEN )
	    {
	       /* start enrolling */
	       error = StartEnrolling( tiesrSD );
	    }
	    break;
	 }

	 case RECOGNIZE:
	 {
	    if( appState == APPRECOGNIZING )
	    {
	       /* Start recognizing all over again */
	       error = StopRecognizing( tiesrSD );
	       error = StartRecognizing( tiesrSD );
	    }
	    else if( appState == APPENROLLING )
	    {
	       /* Stop enrolling, start recognizing */
	       error = StopEnrolling( tiesrSD );
	       error = StartRecognizing( tiesrSD );
	    }
	    else if( appState == APPOPEN )
	    {
	       error = StartRecognizing( tiesrSD );
	    }
	    break;
	 }

	 case ABORT:
	 {
	    if( appState == APPRECOGNIZING )
	    {
	       error = StopRecognizing( tiesrSD );
	    }
	    else if( appState == APPENROLLING )
	    {
	       error = StopEnrolling( tiesrSD );
	    }
	    break;
	 }

	 case QUIT:
	 {
	    if( appState == APPENROLLING )
	    {
	       error = StopEnrolling( tiesrSD );
	    }
	    else if( appState == APPRECOGNIZING )
	    {
	       error = StopRecognizing( tiesrSD );
	    }
	    exitLoop = TRUE;
	    break;
	 }

	 case RECORDSPEAK:
	 {
	    if( appState == APPRECOGNIZING )
	    {
	       error = PromptRecognize( tiesrSD, eventStatus );
	    }
	    else if( appState == APPENROLLING )
	    {
	       error = PromptEnroll( tiesrSD, eventStatus );
	    }
	    break;
	 }


	 case RECORDDONE:
	 {
	    if( appState == APPRECOGNIZING )
	    {
	       error = RecoRecordDone( tiesrSD, eventStatus );
	    }
	    else if( appState == APPENROLLING )
	    {
	       error = EnrollRecordDone( tiesrSD, eventStatus );
	    }
	    break;
	 }
	 
	 case NAME:
	 {
	    if( appState == APPENROLLING )
	    {
	       error = HaveEnrollFilename( tiesrSD );
	    }
	    break;
	 }

	 case NOEVENT:
	 {
	    if( appState == APPRECOGNIZING )
	    {
	       error = StopRecognizing( tiesrSD );
	    }
	    else if( appState == APPENROLLING )
	    {
	       error = StopEnrolling( tiesrSD );
	    }

	    printf( "No event error!\n" );
	    exitLoop = TRUE;
	    break;
	 }

	 default:
	 {
	    if( appState == APPRECOGNIZING )
	    {
	       error = StopRecognizing( tiesrSD );
	    }
	    else if( appState == APPENROLLING )
	    {
	       error = StopEnrolling( tiesrSD );
	    }

	    printf( "Invalid event!\n" );
	    exitLoop = TRUE;
	    break;
	 }
      }
   }
}


/*----------------------------------------------------------------*/
ErrorCode_t StartEnrolling( TIesrSD_t tiesrSD )
{
   TIesrSD_Error_t sdError;
   TIesrSD_Params_t sdParams;
   
   /* The app should be in the APPOPEN state.  Both subtasks should be closed */
   if( appState != APPOPEN || recoState != RECOCLOSED ||
       enroState != ENROCLOSED )
   {
      return ErrorState;
   }


   /* Initialize global parameters for enrollment */
   numRecorded = 0;

   /* Open TIesrSD enrollment subtask */
   sdError = TIesrSD_enroll_open( tiesrSD, enroMemorySize, maxFrames );
   if( sdError != TIesrSDErrNone )
   {
      return ErrorFail;
   }

   /* Now have opened the enrollment subtask */
   enroState = ENROOPEN;
   appState = APPENROLLING;


   /* Set TIesrSD Parameters */
   sdError = TIesrSD_getparams( tiesrSD, &sdParams );
   if( sdError != TIesrSDErrNone )
   {
      sdError = TIesrSD_enroll_close( tiesrSD );
      if( sdError == TIesrSDErrNone )
      {
	 enroState = ENROCLOSED;
	 appState = APPOPEN;
      }
      return ErrorFail;
   }

   sdParams.sampleRate = 8000;
   sdParams.circularFrames = 800;
   sdParams.audioFrames = 10;
   sdParams.audioPriority = 20;
   sdParams.audioReadRate = 10000;

   sdError = TIesrSD_setparams( tiesrSD, &sdParams );
   if( sdError != TIesrSDErrNone )
   {
      sdError = TIesrSD_enroll_close( tiesrSD );
      if( sdError == TIesrSDErrNone )
      {
	 enroState = ENROCLOSED;
	 appState = APPOPEN;
      }
      return ErrorFail;
   }


   sdError = TIesrSD_enroll_init( tiesrSD );
   if( sdError != TIesrSDErrNone )
   {
      sdError = TIesrSD_enroll_close( tiesrSD );
      if( sdError == TIesrSDErrNone )
      {
	 enroState = ENROCLOSED;
	 appState = APPOPEN;
      }
      return ErrorFail;
   }


   /* We now want to get the model name to enroll */
   printf( "Enter name to enroll: \n" );

   return ErrorNone;
}


/*----------------------------------------------------------------*/
ErrorCode_t HaveEnrollFilename( TIesrSD_t tiesrSD )
{
   TIesrSD_Error_t sdError;


   if( appState != APPENROLLING || enroState != ENROOPEN )
   {
      return ErrorState;
   }

   /* Start recording first utterance */
   sdError = TIesrSD_enroll_record( tiesrSD );
   if( sdError != TIesrSDErrNone )
   {
      return ErrorFail;
   }

   enroState = ENRORECORDING1;

   return ErrorNone;
}

/*----------------------------------------------------------------*/
ErrorCode_t StopEnrolling( TIesrSD_t tiesrSD )
{
   TIesrSD_Error_t sdError;

   if( appState != APPENROLLING )
   {
      return ErrorState;
   }

   /* If recording is ongoing, stop it */
   if( enroState == ENRORECORDING1 || enroState == ENRORECORDING2 )
   {
      sdError = TIesrSD_enroll_stop( tiesrSD );
      if( sdError != TIesrSDErrNone )
      {
	 return ErrorFail;
      }

      enroState = ENROOPEN;
   }

   /* Close open enrollment subtask */
   if( enroState == ENROOPEN )
   {
      sdError = TIesrSD_enroll_close( tiesrSD );
      if( sdError != TIesrSDErrNone )
      {
	 return ErrorFail;
      }
      enroState = ENROCLOSED;
      appState = APPOPEN;
   }


   printf( "Stopped enrolling\n" );
   return ErrorNone;
}


/*----------------------------------------------------------------*/
ErrorCode_t PromptEnroll( TIesrSD_t tiesrSD, TIesrSD_Error_t status )
{
   ErrorCode_t error;

   if( appState != APPENROLLING || 
       (enroState != ENRORECORDING1 && enroState != ENRORECORDING2 ) )
   {
      return ErrorState;
   }

   /* User spoke before timeout */
   if( status != TIesrSDErrNone )
   {
      printf( "Spoke too soon\n" );
      error = StopEnrolling( tiesrSD );
      return error;
   }

   if( enroState == ENRORECORDING1 )
      printf( "Say: %s\n", enrollName );
   else
      printf( "Please repeat: %s\n", enrollName );

   return ErrorNone;
}


/*----------------------------------------------------------------*/
ErrorCode_t EnrollRecordDone( TIesrSD_t tiesrSD, TIesrSD_Error_t status )
{
   ErrorCode_t error;

   TIesrSD_Error_t sdError;

   int engStatus, audioStatus;
   
   if( appState != APPENROLLING || 
       ( enroState != ENRORECORDING1 && enroState != ENRORECORDING2 ) )
   {
      return ErrorState;
   }

   /* Some failure to record utterance */
   if( status != TIesrSDErrNone )
   {
      sdError = TIesrSD_status( tiesrSD, &engStatus, &audioStatus );
      error = StopEnrolling( tiesrSD );
      return ErrorFail;
   }

   
   /* Stop the recording phase */
   sdError = TIesrSD_enroll_stop( tiesrSD );
   if( sdError != TIesrSDErrNone )
   {
      return ErrorFail;
   }

   /* First utterance recording done */
   if( enroState == ENRORECORDING1 )
   {
      numRecorded++;
      
      /* Start the second recording phase */
      sdError = TIesrSD_enroll_record( tiesrSD );
      if( sdError != TIesrSDErrNone )
      {
	 return ErrorFail;
      }
      enroState = ENRORECORDING2;

      return ErrorNone;
   }

   /* Second utterance recording done */
   else if( enroState == ENRORECORDING2 )
   {
      /* Done with second recording, make the model */
      numRecorded++;

      /* Sanity check */
      if( numRecorded == 2 )
      {
	 printf( "Enrolling model %s, please wait\n", enrollName );

	 sdError = TIesrSD_enroll_model( tiesrSD, enrollName );
	 if( sdError != TIesrSDErrNone )
	 {
	    printf( "Failed to create model, %d\n", sdError );
	 }
	 else
	 {
	    short score;
	    sdError = TIesrSD_enroll_score( tiesrSD, &score );
	    
	    printf( "Enrolled: %s; Score %d\n", enrollName, score );
	 }
      }
   }
   
   /* Done with this enrollment */
   numRecorded = 0;
   enroState = ENROOPEN;
   
   /* Close the enrollment subtask */
   error = StopEnrolling( tiesrSD );
   
   return ErrorNone;
}

/*----------------------------------------------------------------*/
ErrorCode_t StartRecognizing( TIesrSD_t tiesrSD )
{
   TIesrSD_Error_t sdError;
   TIesrSD_Params_t sdParams;
   
   /* The app should be in the APPOPEN state, so should be able to 
      start recognizing */
   if( appState != APPOPEN || recoState != RECOCLOSED ||
       enroState != ENROCLOSED )
   {
      return ErrorState;
   }
   
   /* Initialize global parameters for recognizing */
   numRecorded = 0;

   /* Open TIesrSD recognition subtask */
   sdError = TIesrSD_reco_open( tiesrSD, recoMemorySize, maxFrames );
   if( sdError != TIesrSDErrNone )
   {
      return ErrorFail;
   }
   recoState = RECOOPEN;
   appState = APPRECOGNIZING;

   /* Set TIesrSD Parameters */
   sdError = TIesrSD_getparams( tiesrSD, &sdParams );
   if( sdError != TIesrSDErrNone )
   {
      sdError = TIesrSD_reco_close( tiesrSD );
      if( sdError == TIesrSDErrNone )
      {
	 recoState = RECOCLOSED;
	 appState = APPOPEN;
      }
      return ErrorFail;
   }

   sdParams.pruneFactor = -8;

   sdParams.sampleRate = 8000;
   sdParams.circularFrames = 800;
   sdParams.audioFrames = 10;
   sdParams.audioPriority = 20;
   sdParams.audioReadRate = 10000;

   sdError = TIesrSD_setparams( tiesrSD, &sdParams );
   if( sdError != TIesrSDErrNone )
   {
      sdError = TIesrSD_enroll_close( tiesrSD );
      if( sdError == TIesrSDErrNone )
      {
	 recoState = RECOCLOSED;
	 appState = APPOPEN;
      }
      return ErrorFail;
   }


   sdError = TIesrSD_reco_init( tiesrSD );
   if( sdError != TIesrSDErrNone )
   {
      sdError = TIesrSD_reco_close( tiesrSD );
      if( sdError == TIesrSDErrNone )
      {
	 recoState = RECOCLOSED;
	 appState = APPOPEN;
      }
      return ErrorFail;
   }


   /* We now want to start recording of the recognition utterance */
   sdError = TIesrSD_reco_record( tiesrSD );
   if( sdError != TIesrSDErrNone )
   {
      sdError = TIesrSD_reco_close( tiesrSD );
      if( sdError == TIesrSDErrNone )
      {
	 recoState = RECOCLOSED;
	 appState = APPOPEN;
      }
      return ErrorFail;
   }      
   recoState = RECORECORDING;


   return ErrorNone;
}


/*----------------------------------------------------------------*/
ErrorCode_t StopRecognizing( TIesrSD_t tiesrSD )
{
   TIesrSD_Error_t sdError;

   if( appState != APPRECOGNIZING )
   {
      return ErrorState;
   }

   /* If recording is ongoing, stop it */
   if( recoState == RECORECORDING )
   {
      sdError = TIesrSD_reco_stop( tiesrSD );
      if( sdError != TIesrSDErrNone )
      {
	 return ErrorFail;
      }

      recoState = RECOOPEN;
   }

   /* Close open enrollment subtask */
   if( recoState == RECOOPEN )
   {
      sdError = TIesrSD_reco_close( tiesrSD );
      if( sdError != TIesrSDErrNone )
      {
	 return ErrorFail;
      }
      recoState = RECOCLOSED;
      appState = APPOPEN;
   }

   printf( "Stopped recognizing\n" );
   return ErrorNone;
}


/*----------------------------------------------------------------*/
ErrorCode_t PromptRecognize( TIesrSD_t tiesrSD, TIesrSD_Error_t status )
{
   ErrorCode_t error;

   if( appState != APPRECOGNIZING || recoState != RECORECORDING )
   {
      return ErrorState;
   }

   /* User spoke before timeout */
   if( status != TIesrSDErrNone )
   {
      printf( "Spoke too soon\n" );
      error = StopRecognizing( tiesrSD );
      return error;
   }
   else
   {
      printf( "Please say an enrolled name\n" );
   }

   return ErrorNone;
}


/*----------------------------------------------------------------*/
ErrorCode_t RecoRecordDone( TIesrSD_t tiesrSD, TIesrSD_Error_t status )
{
   ErrorCode_t error;

   TIesrSD_Error_t sdError;

   int engStatus, audioStatus;

   
   if( appState != APPRECOGNIZING || recoState != RECORECORDING )
   {
      return ErrorState;
   }

   /* Some failure to record utterance */
   if( status != TIesrSDErrNone )
   {
      sdError = TIesrSD_status( tiesrSD, &engStatus, &audioStatus );
      printf( "Recording fail %d  %d  %d\n", status, engStatus, audioStatus );
      error = StopRecognizing( tiesrSD );
      return ErrorFail;
   }

   
   /* Stop the recording phase */
   sdError = TIesrSD_reco_stop( tiesrSD );
   if( sdError != TIesrSDErrNone )
   {
      return ErrorFail;
   }
   recoState = RECOOPEN;


   /* Done with recording, do the recognition */
   numRecorded++;

   /* Sanity check */
   if( numRecorded == 1 )
   {
      char bestFile[PATH_MAX + 1];
      long bestScore;

      printf( "Recognizing, please wait\n" );

      sdError = TIesrSD_reco_score( tiesrSD, bestFile, &bestScore );
      if( sdError != TIesrSDErrNone )
      {
	 printf( "Failed to recognize\n" );
      }
      else
      {
	 short confScore;
	 sdError = TIesrSD_reco_confidence( tiesrSD, &confScore );
	    
	    printf( "Recognized: %s; Score %ld; Confidence %d\n",
		    bestFile, bestScore, confScore );
      }
   }
   
   /* Done with this recognition */
   numRecorded = 0;
   
   /* Close the recognition subtask */
   error = StopRecognizing( tiesrSD );
   
   return error;
}



/*----------------------------------------------------------------*/
void* KbdThreadFcn( void* aArg )
{
   char line[256];
   char cmd;
   int lineLength;

   ProcessQue_t* ptrProcessQue =  (ProcessQue_t*)aArg;

   do
   {
      fgets(line, 256, stdin );
      cmd = line[0];
      lineLength = strlen( line );
      if( line[lineLength-1] == '\n' )
      {
         line[--lineLength] = '\0';
      }

      if( lineLength > 1 )
      {
	 strcpy( enrollName, line );
	 PutEventInQue( ptrProcessQue, NAME, 0 );
      }

      else
      {
	 switch( cmd )
	 {
	    case 'q': case 'Q':
	       PutEventInQue( ptrProcessQue, QUIT, 0 );
	       break;

	    case 'e': case 'E':
	       PutEventInQue( ptrProcessQue, ENROLL, 0 );
	       break;

	    case 'r': case 'R':
	       PutEventInQue( ptrProcessQue, RECOGNIZE, 0 );
	       break;

	    case 'a': case 'A':
	       PutEventInQue( ptrProcessQue, ABORT, 0 );
	       break;

	       /* Ignore all other single characters */
	    default:
	       break;
	 }
      }


   } while( lineLength > 1 || ( cmd != 'q' && cmd != 'Q' ) );

   return 0;
}


/*----------------------------------------------------------------*/
void InitializeQue( ProcessQue_t* aProcessQue )
{
   aProcessQue->eventsInQue = 0;
   aProcessQue->nextEvent = NULL;
   aProcessQue->lastEvent = NULL;

   pthread_mutex_init( &aProcessQue->queMutex, NULL );
   sem_init( &aProcessQue->queSemaphore, 0 , 0 );
}   


/*----------------------------------------------------------------*/
void PutEventInQue( ProcessQue_t* aProcessQue, EventCode_t aEventCode, int aEventStatus )
{
   Event_t* newEvent;

   /* Make a new event to put in que */
   newEvent = (Event_t*)malloc( sizeof(Event_t) );
   if( newEvent == NULL )
   {
      return;
   }
   newEvent->eCode = aEventCode;
   newEvent->eStatus = aEventStatus;
   newEvent->next = NULL;

   /* Get lock on the process que */
   pthread_mutex_lock( &aProcessQue->queMutex );
      
   /* put the event at the end of the que */
   if( aProcessQue->nextEvent == NULL )
   {
      aProcessQue->nextEvent = newEvent;
   }
   else
   {
      aProcessQue->lastEvent->next = newEvent;
   }
   aProcessQue->lastEvent = newEvent;

   /* Increment number of events in the que */
   aProcessQue->eventsInQue++;
   
   /* Signal another event available in the que */
   sem_post( &aProcessQue->queSemaphore );

   /* Unlock process que */
   pthread_mutex_unlock( &aProcessQue->queMutex );
}


/*----------------------------------------------------------------*/
void GetEventFromQue( ProcessQue_t* aProcessQue, EventCode_t* aEventCode, int* aEventStatus )
{
   Event_t* currEvent;

   /* Get lock on the process que */
   pthread_mutex_lock( &aProcessQue->queMutex );
      
   /*Check for empty event, should not happen */
   if( aProcessQue->nextEvent == NULL )
   {
      *aEventCode = NOEVENT;
      *aEventStatus = 0;
      pthread_mutex_unlock( &aProcessQue->queMutex );
      return;
   }

   /* Get the event code */
   currEvent = aProcessQue->nextEvent;
   *aEventCode = currEvent->eCode;
   *aEventStatus = currEvent->eStatus;

   /* Remove current event from que */
   aProcessQue->nextEvent = currEvent->next;
   if( aProcessQue->nextEvent == NULL )
   {
      aProcessQue->lastEvent = NULL;
   }

   /* Track count of active events */
   aProcessQue->eventsInQue--;

   /* Destroy the current event */
   free(currEvent);

   /* Unlock process que */
   pthread_mutex_unlock( &aProcessQue->queMutex );
}


/*----------------------------------------------------------------*/
void FlushQue( ProcessQue_t* aProcessQue )
{
   Event_t* currEvent;

   while( aProcessQue->nextEvent != NULL )
   {
      currEvent = aProcessQue->nextEvent;
      aProcessQue->nextEvent = currEvent->next;
      free( currEvent );
   }
   aProcessQue->lastEvent = NULL;
   aProcessQue->eventsInQue = 0;

   pthread_mutex_destroy( &aProcessQue->queMutex );

   sem_destroy( &aProcessQue->queSemaphore );
}


/*----------------------------------------------------------------*/
void SpeakCallback( void* aArg, TIesrSD_Error_t aError )
{
   ProcessQue_t* processQue = (ProcessQue_t*)aArg;

   PutEventInQue( processQue, RECORDSPEAK, (int)aError );
}


/*----------------------------------------------------------------*/
void DoneCallback( void* aArg, TIesrSD_Error_t aError )
{
   ProcessQue_t* processQue = (ProcessQue_t*)aArg;

   PutEventInQue( processQue, RECORDDONE, (int)aError );
}
