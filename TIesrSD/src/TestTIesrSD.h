/*=================================================================

 *
 * TestTIesrSD.h
 *
 * Include file for testing TIesrSD API.
 *
 * Copyright (C) 2006-2011 Texas Instruments Incorporated - http://www.ti.com/
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation version 2.1 of the License.
 *
 * This program is distributed "as is" WITHOUT ANY WARRANTY of any kind,
 * whether express or implied; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *

 ====================================================================*/

#define TRUE 1
#define FALSE 0

#include <limits.h>
#include "TIesrSD_User.h"


/* Constants for the test */
#define GRAMMARDIR "./grammar/"
#define MODELDIR "./models/"


/* Application state */
typedef enum AppState
{
   APPOPEN,
   APPENROLLING,
   APPRECOGNIZING,
} AppState_t;


/* Enroll state */
typedef enum EnroState
{
   ENROCLOSED,
   ENROOPEN,
   ENRORECORDING1,
   ENRORECORDING2,
}EnroState_t;

/* Recognize state */
typedef enum RecoState
{
   RECOCLOSED,
   RECOOPEN,
   RECORECORDING,
}RecoState_t;



/* The enum that defines allowable Events */
typedef enum EventCode
{  
   ENROLL,
   RECOGNIZE,
   ABORT,
   QUIT,
   RECORDSPEAK,
   RECORDDONE,
   NAME,
   NOEVENT
} EventCode_t;


typedef enum ErrorCode
{
   ErrorNone,
   ErrorState,
   ErrorFail
} ErrorCode_t;


/* A structure defining an event linked list entry */

typedef struct Event
{
      EventCode_t eCode;
      int eStatus;
      struct Event* next;
} Event_t;


/* 
   The event Process Que for receiving and dispatching recognition events.
   The events are processed from the linked list in a FIFO manner.
*/
typedef struct ProcessQue
{
      /* counter of events in que */
      int eventsInQue;

      /* The linked list of events */
      Event_t* nextEvent;
      Event_t* lastEvent;
  
      pthread_mutex_t queMutex;
      sem_t queSemaphore;

} ProcessQue_t;


void InitializeQue( ProcessQue_t* aProcessQue );
void PutEventInQue( ProcessQue_t* aProcessQue, EventCode_t aEventNum, int aEventStatus );
void GetEventFromQue( ProcessQue_t* aProcessQue, EventCode_t *aEventNum, int*aEventStatus );
void RunQueLoop( ProcessQue_t* aProcessQue, TIesrSD_t tiesrSD );
void FlushQue( ProcessQue_t* aProcessQue );

void* KbdThreadFcn( void* aArg );

void SpeakCallback( void* aArg, TIesrSD_Error_t aError );
void DoneCallback( void* aArg, TIesrSD_Error_t aError );

ErrorCode_t StartEnrolling( TIesrSD_t tiesrSD );
ErrorCode_t HaveEnrollFilename( TIesrSD_t tiesrSD );
ErrorCode_t StopEnrolling( TIesrSD_t tiesrSD );
ErrorCode_t PromptEnroll( TIesrSD_t tiesrSD, TIesrSD_Error_t status );
ErrorCode_t EnrollRecordDone( TIesrSD_t tiesrSD, TIesrSD_Error_t status );
ErrorCode_t StartRecognizing( TIesrSD_t tiesrSD );
ErrorCode_t StopRecognizing( TIesrSD_t tiesrSD );
ErrorCode_t PromptRecognize( TIesrSD_t tiesrSD, TIesrSD_Error_t status );
ErrorCode_t RecoRecordDone( TIesrSD_t tiesrSD, TIesrSD_Error_t status );

