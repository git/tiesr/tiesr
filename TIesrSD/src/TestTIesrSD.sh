#!/bin/sh

# This shell script shows how to run the TestTIesrSD program.  Once
# started, you can input single character commands, followed by
# <return> to do speaker-dependent recognition.  The single character
# commands are:

# e = enroll a new name
# r = recognize a name
# a = abort an enrollment or recognition
# q = quit the program

# All names enrolled are placed in the  models/ directory.
# To add a new name, select the "e" command.  It will ask for
# a name.  Enter the name, then it will ask for you to say the
# name twice, and will enroll the name.

# You need to set the $AUDIO_DEVICE environment variable
# to the audio device to be used for data collection, or
# for debugging it can be set to a file name.

cd ../../Data/SD
../../Dist/LinuxDebugGnu/bin/testtiesrsd 30000 30000 200 $AUDIO_DEVICE
